package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MixNMatch {

	public static void main(String[] args) throws FileNotFoundException {
		String base = args[0];
		String pattern = args[1];
		
		File p = new File(pattern);
		Scanner scannerP = new Scanner(p);
		ArrayList<String> patt = new ArrayList<String>();
		while (scannerP.hasNextLine()) {
			String pat = scannerP.nextLine();
			patt.add(pat);
		}
		File b = new File(base);
		Scanner scannerB = new Scanner(b);
		while (scannerB.hasNextLine()) {
			String bas = scannerB.nextLine();
			for (int a = 0; a < patt.size(); a++) {
				if (bas.toLowerCase().contains(patt.get(a).toLowerCase())) {
					System.out.println(bas);
				}
			}
		}
		scannerP.close();
		scannerB.close();
	}

}
