package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Various methods to parse text files.
 * @author pwisesa
 *
 */

public class TextFileParser {
	
	public static ArrayList<Double> getDoublesAmongString (String line, String delim) {
		ArrayList<Double> numbers = new ArrayList<Double>();
		String[] inLine = line.split(delim);
		String number = null;
		for (int count = 0; count < inLine.length; count++) {
			try {
				double temp = Double.parseDouble(inLine[count]);
			} catch (NumberFormatException e) {
				continue;
			}
			number = inLine[count];
			numbers.add(Double.parseDouble(number));
		}
		return numbers;
	}
	public static ArrayList<Integer> getIntegersAmongString (String line, String delim) {
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		String[] inLine = line.split(delim);
		String number = null;
		for (int count = 0; count < inLine.length; count++) {
			String readLine = inLine[count].replaceAll("\\s+",""); //parseInt apparently does not work with spaces
			try {
				double temp = Integer.parseInt(readLine);
			} catch (NumberFormatException e) {
				continue;
			}
			number = readLine;
			numbers.add(Integer.parseInt(number));
		}
		return numbers;
	}
	public static Double parseDoubleValue(String value) {
	    try {
	      return Double.parseDouble(value.trim());
	    } catch (NumberFormatException e) {
	      return null;
	    }
	  }
	public static Integer parseIntegerValue(String value) {
	    try {
	      return Integer.parseInt(value.trim());
	    } catch (NumberFormatException e) {
	      return null;
	    }
	  }

	
	public static String grepFirstMatch (String targetString, String filePath) throws FileNotFoundException {
		File file = new File (filePath);
		Scanner scanner = new Scanner(file); //there was a final here
		String lineFound = null;
		while (scanner.hasNextLine()) {
		   String lineFromFile = scanner.nextLine(); // and here, seemed dangerous so I removed them
		   if(lineFromFile.contains(targetString)) { 
		       // a match!
		       //System.out.println("Found " +targetString+ " in file " +file.getName());
		       lineFound = lineFromFile;
		       break;
		   }
		}
		scanner.close();
		return lineFound;
	}
	
	public static int getFirstMatchLineNum (String targetString, String filePath) throws FileNotFoundException {
		File file = new File (filePath);
		Scanner scanner = new Scanner(file); //there was a final here
		String lineFound = null;
		int lineNum = 0;
		while (scanner.hasNextLine()) {
		   String lineFromFile = scanner.nextLine(); // and here, seemed dangerous so I removed them
		   lineNum++;
		   if(lineFromFile.contains(targetString)) { 
		       // a match!
		       //System.out.println("Found " +targetString+ " in file " +file.getName());
		       lineFound = lineFromFile;
		       break;
		   }
		}
		scanner.close();
		return lineNum;
	}
	
	public static ArrayList<String> grepAllMatches (String targetString, String filePath) throws FileNotFoundException {
		File file = new File (filePath);
		Scanner scanner = new Scanner(file);
		ArrayList<String> linesFound = new ArrayList<String>();
		while (scanner.hasNextLine()) {
		   String lineFromFile = scanner.nextLine();
		   if(lineFromFile.contains(targetString)) { 
		       // a match!
		       //System.out.println("Found " +targetString+ " in file " +file.getName());
		       linesFound.add(lineFromFile);
		   }
		}
		scanner.close();
		return linesFound;
	}
	
	
	public static String getLineNumX (int lineNum, String filePath) throws FileNotFoundException {
		File file = new File (filePath);
		final Scanner scanner = new Scanner(file);
		int lineCount = 0;
		String lineFound = null;
		while (scanner.hasNextLine()) {
		   final String lineFromFile = scanner.nextLine();
		   lineCount++;
		   if(lineCount == lineNum) { 
		       // we're done!
		       //System.out.println("Found " +targetString+ " in file " +file.getName());
		       lineFound = lineFromFile;
		       break;
		   }
		}
		scanner.close();
		return lineFound;
	}
	
	public static String[] splitIgnoringSingleQuotations(String line) {
		String otherThanQuote = " [^\"] ";
        String quotedString = String.format(" \' %s* \' ", otherThanQuote);
        String regex = String.format("(?x) "+ // enable comments, ignore white spaces
                ",                         "+ // match a comma
                "(?=                       "+ // start positive look ahead
                "  (?:                     "+ //   start non-capturing group 1
                "    %s*                   "+ //     match 'otherThanQuote' zero or more times
                "    %s                    "+ //     match 'quotedString'
                "  )*                      "+ //   end group 1 and repeat it zero or more times
                "  %s*                     "+ //   match 'otherThanQuote'
                "  $                       "+ // match the end of the string
                ")                         ", // stop positive look ahead
                otherThanQuote, quotedString, otherThanQuote);

        String[] tokens = line.split(regex, -1);
        return tokens;
	}
}


