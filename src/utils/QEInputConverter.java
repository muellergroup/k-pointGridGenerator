package utils;

import matsci.io.structure.CIF;
import matsci.io.structure.QEStructure;
import matsci.io.vasp.POSCAR;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperStructure;

public class QEInputConverter {
	
	public static void main (String[] args) {
		CartesianBasis.setPrecision(1E-6);
	    CartesianBasis.setAngleToleranceDegrees(1E-5);
		if (args[0].equals("cifToQE")) {
			CIF cif = new CIF(args[1], 1.0/720);
			Structure structure = new Structure(cif);
			QEStructure qe = new QEStructure(structure);
			qe.writeFile(args[2]);
		}
		if (args[0].equals("cifToPOS")) {
			CIF cif = new CIF(args[1], 1.0/720);
			Structure structure = new Structure(cif);
			POSCAR pos = new POSCAR(structure);
			pos.writeFile(args[2]);
		}
		
		if (args[0].equals("POSToQE")) {
			POSCAR posc = new POSCAR(args[1]);
			QEStructure qe = new QEStructure(posc);
			qe.writeFile(args[2]);
		}
		
		if (args[0].equals("supercell")) {
			POSCAR posc = new POSCAR(args[1]);
			int multiplier = Integer.parseInt(args[2]);
			Structure structure = new Structure(posc);
			SpaceGroup space = structure.getDefiningSpaceGroup().getSymmorphicGroup(true);
			BravaisLattice conventionalLattice = space.getConventionalLattice();
			BravaisLattice lat = structure.getDefiningLattice();
			int[][] latticeVectors = lat.getSuperToDirect(conventionalLattice);
			int[][] tempLatVec = new int[latticeVectors.length][];
			for (int a = 0; a < latticeVectors.length; a++) {
				tempLatVec[a] = new int[latticeVectors[a].length];
				System.arraycopy(latticeVectors[a], 0, tempLatVec[a], 0, latticeVectors[a].length);
				for (int b = 0; b < latticeVectors[a].length; b++) {
					int temp = latticeVectors[a][b] * multiplier;
					tempLatVec[a][b] = temp;
				}
			}
			SuperStructure supStru = new SuperStructure(structure, tempLatVec);
			POSCAR toPrint = new POSCAR (supStru);
			toPrint.writeFile(args[3]);
		}
		
		
	}

}
