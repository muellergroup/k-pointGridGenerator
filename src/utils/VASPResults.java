package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

//import mains.CheckConvergence;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.location.Vector;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.superstructure.SuperLattice;


/**
 * This class stores parameters from a VASP calculation <br>
 * The path is assumed to be ~/calculation/, with no spacing or symbols on the structure name <br>
 * Standard out is assumed to be named as stdout <br>
 * And OUTCAR is ... OUTCAR. <br>
 * 
 * 
 * Sep 12, 2014
 * @author pwisesa
 *
 */

public class VASPResults {

		private int m_TotKPoints;
		private int m_DistinctKPoints;
		private int m_NumAtoms;
		private int m_CoreNum;
		private int m_ArithNum;
		private int m_LastElectronicStep;
		private int m_ISpin;
		private int m_ISmear;
		private int m_NumSites;
		private int m_NumPrimSites;
		private double m_Nupdown;
		private double m_Time;
		private double m_CPUTime;
		private double m_Energy;
		private double m_EnergyPerAtom;
		private double m_EneDiffWSmallest;
		private double m_LowestSpacingEneInColl;
		private double m_KSpacing;
		private double m_RecordedMinDist;
		private double m_RecordedKSpacing;
		private double m_TimePerDistKPts;
		private double m_TimePerPoints;
		private double m_TimePerPointsTimesDistKPts;
		private double m_DirectBandGap;
		private double m_IndirectBandGap;
		private double m_Volume;
		private double m_PrimVolume;
		private double m_PrimMinPeriodicDistance;
		private double m_ThirdShortestKVectorLength;
		private double m_ThirdShortestRVectorLength;
		private double m_PrimitiveReciprocalMPD;
		private double m_PrimitiveRealMPD;
		private String m_ICSD_ID;
		private String m_Composition;
		private String m_Path;
		private String m_Algo;
		private boolean m_TetProblem;
		private boolean m_SmearProblem;
		private boolean m_AreCalculationsConvergedClean;
		private boolean m_AreCalculationsConvergedDirty;
		private boolean m_IsLastElectronicallyConverged;
		private boolean m_PointOfCleanConvergence;
		private boolean m_PointOfDirtyConvergence;
		private boolean m_AnyErrors;
		private boolean m_FatalError; //This is for errors in the calculation that prevented data reading
		//private boolean m_FlaggedForWarning;
		
		//These flag additions are based on Material Project's VASP Handlers that I append
		private boolean m_Err_Tet;
		private boolean m_InvRotMat;
		private boolean m_Brmix;
		private boolean m_SubSpaceMatrix;
		private boolean m_Tetirr;
		private boolean m_IncorrectShift;
		private boolean m_RealOptlay;
		private boolean m_Rspher;
		private boolean m_Dentnet;
		private boolean m_TooFewBands;
		private boolean m_TripleProduct;
		private boolean m_RotMatrix;
		private boolean m_Brions;
		private boolean m_Pricel;
		private boolean m_Zpotrf;
		private boolean m_Amin;
		private boolean m_Zbrent;
		private boolean m_Aliasing;
		private boolean m_AliasingIncar;
		private boolean m_Pssyevx;
		private boolean m_Eddrmm;
		private boolean m_Ibzkpt; //the reciprocal lattice is inconsistent with the k-lattice
		private boolean m_Nkpts;
		private boolean m_Sgrcon;
		private boolean m_Ibzkpt_Fatal;
		private boolean m_Ibzkpt_Shift;
		private boolean m_Ibzkpt_MeshType;
		
		public VASPResults (String calcPath) throws IOException {
			//if (calcPath.contains("0.0628318531") && calcPath.contains("167903")) {
			//	System.currentTimeMillis();
			//}
			m_FatalError = false;
			try {
				m_Energy = readVASPEnergy(calcPath + "/stdout", true);
			} catch (RuntimeException e) {
				m_Energy = Double.NaN;
				m_FatalError = true;
				System.out.println("VASP calculation in " + calcPath + " has not converged!");
			}
			
			//This is just used when the tetrahedron method had to be changed to smearing because VASP has a bug there
			File file1 = new File(calcPath + "/tetProb");
			if (file1.exists() && !file1.isDirectory()) {
				m_TetProblem = true;
			} else {
				m_TetProblem = false;
			}
			//likewise for smearing only problem
			File file2 = new File(calcPath + "/smearProb");
			if (file2.exists() && !file2.isDirectory()) {
				m_SmearProblem = true;
			} else {
				m_SmearProblem = false;
			}
			
			m_EneDiffWSmallest = Double.NaN;
			
			m_AreCalculationsConvergedClean = false;
			
			m_PointOfCleanConvergence = false;
			
			m_AreCalculationsConvergedDirty = false;
			
			m_PointOfDirtyConvergence = false;
			
			m_LowestSpacingEneInColl = Double.NaN;
			
			m_Path = calcPath;
			
			String delim = "[/,\\[\\]\\s=;]";
			String OUTCARPath = m_Path + "/OUTCAR";
			
			try {
				double[] directIndirect = getBandGaps(OUTCARPath);
				m_DirectBandGap = directIndirect[0];
				m_IndirectBandGap = directIndirect[1];
			} catch (RuntimeException e) {
				System.out.println("Cannot find the number of distinct k-points in the OUTCAR, calculation could not map the grid");
				m_DirectBandGap = Double.NaN;
				m_IndirectBandGap = Double.NaN;
				m_FatalError = true;
			}
			
			try {
				String smearLine = grepFirstMatch("ISMEAR", OUTCARPath);
				m_ISmear = getIntegersAmongString(smearLine, delim).get(0);
			} catch (RuntimeException e) {
				System.out.println("Cannot find the value for ISMEAR in the OUTCAR.");
				m_ISmear = -255;
			}
			
			try {
				String spinLine = grepFirstMatch("ISPIN", OUTCARPath);
				m_ISpin = getIntegersAmongString(spinLine, delim).get(0);
			} catch (RuntimeException e) {
				System.out.println("Cannot find the value for ISPIN in the OUTCAR.");
				m_ISpin = -1;
			}
			
			try {
				String updownLine = grepFirstMatch("NUPDOWN", OUTCARPath);
				m_Nupdown = getDoublesAmongString(updownLine, delim).get(0);
			} catch (RuntimeException e) {
				System.out.println("Cannot find the value for NUPDOWN in the OUTCAR.");
				m_Nupdown = Double.NaN;
			}
			
			String[] pathLine = m_Path.split(delim);
			m_ICSD_ID = pathLine[pathLine.length - 1];
			/*try {
				m_KSpacing = Double.parseDouble(pathLine[pathLine.length - 2]);
			} catch (RuntimeException e) {
				m_KSpacing = Double.NaN;
			}*/
			String POSCARPath = m_Path + "/POSCAR";
			String line = getLineNumX(7, POSCARPath);
			String[] values = line.split(delim);
			List<String> temp = new ArrayList<String>();
			for (int c = 0; c < values.length; c++) {
				if (!values[c].equals("")) {
					temp.add(values[c]);
				}
			}
			try { //in case the POSCAR is not VASP5 format
				Integer.parseInt(temp.get(0));
			} catch (NumberFormatException e) {
				line = getLineNumX(6, POSCARPath);
				values = line.split(delim);
				temp = new ArrayList<String>();
				for (int c = 0; c < values.length; c++) {
					if (!values[c].equals("")) {
						temp.add(values[c]);
					}
				}
			}
			int numAtoms = 0;
			for (int count = 0; count < temp.size(); count++) {
				Integer current = Integer.parseInt(temp.get(count));
				numAtoms = numAtoms + current;
			}
			
			POSCAR poscar = new POSCAR(POSCARPath);
			Structure struct = new Structure(poscar);
			m_PrimitiveReciprocalMPD = struct.getDefiningLattice().getInverseLattice().getMinPeriodicDistance();
			m_PrimitiveRealMPD = struct.getDefiningLattice().getMinPeriodicDistance();
			SpaceGroup space = struct.getDefiningSpaceGroup().getSymmorphicGroup(true);
			int numDim = struct.getDefiningLattice().numPeriodicVectors();
			m_ArithNum = space.getArithmeticCrystalClassNumber(numDim);
			
			m_Volume = struct.getDefiningVolume();
			m_NumSites = struct.getDefiningSites().length;
			
			Structure prim = struct.findPrimStructure();//.getCompactStructure();
			BravaisLattice realPrimLat = prim.getDefiningLattice();
			m_PrimVolume = prim.getDefiningVolume();
			m_NumPrimSites = prim.getDefiningSites().length;
			m_PrimMinPeriodicDistance = realPrimLat.getMinPeriodicDistance();
			
			
			/*Species[] specs = struct.getDistinctSpecies();
			m_Composition = new Element[specs.length];
			for (int a = 0; a < specs.length; a++) {
				m_Composition[a] = specs[a].getElement();
			}*/
			m_Composition = struct.getCompositionString();
			
			m_NumAtoms = numAtoms;
			
			m_EnergyPerAtom = m_Energy / m_NumAtoms;
			
			String INCARPath = m_Path + "/INCAR";
			String kSpacing = grepFirstMatch("KSPACING", INCARPath);
			String eqDel = "[=]";
			if (kSpacing == null) {
				try {
					m_KSpacing = Double.parseDouble(pathLine[pathLine.length - 2]);
				} catch (RuntimeException e) {
					m_KSpacing = Double.NaN;
				}
			} else {
				double reciprocalDistance = getDoublesAmongString(kSpacing, eqDel).get(0);
				m_KSpacing = reciprocalDistance;
			}
			
			scanAlgo();
			
			m_AnyErrors = scanErrors();
			
			String NELM = grepFirstMatch("NELM", INCARPath);
			Integer maxElecStep = 60;
			if (NELM != null) {
				maxElecStep = getDoublesAmongString(NELM, eqDel).get(0).intValue();
			}
			ArrayList<String> elecSteps = new ArrayList<>();
			
			if(m_Algo.equals("Normal")) {
				elecSteps = grepAllMatches("DAV:", m_Path + "/stdout");
			}
			if (m_Algo.equals("Fast") || m_Algo.equals("Very_Fast")) {
				elecSteps = grepAllMatches("RMM:", m_Path + "/stdout");
			}
			
			String howManyIonic = grepFirstMatch("2 F", m_Path + "/stdout");
			if (howManyIonic != null) {
				System.out.println("There are more than one ionic step in this calculation!!!");
			}
			ArrayList<Double> stepNum = new ArrayList<Double>();
			//try not to assume we only run one ionic step
			for (int z= 0; z < elecSteps.size(); z++) {
				Double currentStep = getDoublesAmongString(elecSteps.get(z), delim).get(0);
				stepNum.add(currentStep);
			}
			//there are more than one ionic step
			Collections.sort(stepNum);
			try {
				m_LastElectronicStep = stepNum.get(stepNum.size()-1).intValue();
			} catch (IndexOutOfBoundsException z) {
				m_LastElectronicStep = -1;
				System.out.println("No electronic steps recorded, calculation failed due to errors!");
				m_FatalError = true;
			}
			if (m_LastElectronicStep >= maxElecStep) {
				m_IsLastElectronicallyConverged = false;
			} else {
				m_IsLastElectronicallyConverged = true;
			}
				
			String[] files = listFilesInFolder(m_Path);
			boolean KPOINTFile = false;
			
			for (int a = 0; a < files.length; a++) {
				if (files[a].equals("KPOINTS")) {
					KPOINTFile = true;
					break;
				}
			}
			m_RecordedMinDist = Double.NaN;
			m_RecordedKSpacing = Double.NaN;
			String specDelim = "[/,\\[\\]\\s+_]";
			if (KPOINTFile) {
				String totKpts = getLineNumX(2, m_Path + "/KPOINTS");
				m_DistinctKPoints = Integer.parseInt(totKpts);
				m_TotKPoints = getTotalKPoints(m_DistinctKPoints, m_Path);
				String firstLine = getLineNumX(1, calcPath + "/KPOINTS");
				String[] splitLine = firstLine.split(specDelim);
				
				if (firstLine.contains("Effective MinDistance:")) { // this is the new format
					for (int co = 0; co < splitLine.length; co++) {
						if (splitLine[co].equals("MinDistance:")) {
							m_RecordedMinDist = Double.parseDouble(splitLine[co + 1]);
						} else if (splitLine[co].equals("space:")) {
							m_RecordedKSpacing = 2 * Math.PI * Double.parseDouble(splitLine[co + 1]);
						} else if (splitLine[co].equals("k-vector:")) {
							m_ThirdShortestKVectorLength = Double.parseDouble(splitLine[co + 1]);
						} else if (splitLine[co].equals("r-vector:")) {
							m_ThirdShortestRVectorLength = Double.parseDouble(splitLine[co + 1]);
						}
					}
				} else if(firstLine.contains("Actual minimum periodic distance")) {
					for (int co = 0; co < splitLine.length; co++) {
						if (splitLine[co].equals("periodic")) {
							m_RecordedMinDist = Double.parseDouble(splitLine[co + 3]);
						}
					}
				} else { // this is the old
					for (int co = 0; co < splitLine.length; co++) {
						if (splitLine[co].equals("minDist:")) {
							m_RecordedMinDist = Double.parseDouble(splitLine[co + 1]);
						} else if (splitLine[co].equals("kSpa:")) {
							m_RecordedKSpacing = 2 * Math.PI * Double.parseDouble(splitLine[co + 1]);
						}
					}
				}
				/*ArrayList<Double> headerValues = getDoublesAmongString(firstLine, specDelim); //This piece of code is so rigid, it's bad
				if (headerValues.size() < 2) { //this is... something I did not create
					m_RecordedMinDist = Double.NaN;
					m_RecordedKSpacing = Double.NaN;
				} else if (headerValues.size() == 3) { //this is a different KPOINTS version that I created
					m_RecordedMinDist = headerValues.get(1);
					m_RecordedKSpacing = headerValues.get(2);
				} else { //this is the current one, the first 3 doubles refer to the diagonal values
					m_RecordedMinDist = headerValues.get(3);
					m_RecordedKSpacing = headerValues.get(4);
				}*/
				
				if (m_DistinctKPoints == 0) {
					try {
						String distinctKPtsLine = grepFirstMatch("irreducible k-points:", OUTCARPath);
						Double numDistinct = getDoublesAmongString (distinctKPtsLine, delim).get(0);
						m_DistinctKPoints = numDistinct.intValue();
					} catch (RuntimeException e) {
						System.out.println("Cannot find the number of distinct k-points in the OUTCAR, calculation could not map the grid");
						m_DistinctKPoints = -1;
						m_FatalError = true;
					}
					//String a = getLineNumX(4, m_Path + "/KPOINTS");
					ArrayList<Integer> meshValues = getIntegersAmongString(getLineNumX(4, m_Path + "/KPOINTS") , specDelim);
					Integer t = -1;
					for (int valCount = 0; valCount < meshValues.size(); valCount++) {
						t = t * meshValues.get(valCount);
					}
					m_TotKPoints = -1 * t.intValue();
				}
			} else {
				BravaisLattice lat = struct.getDefiningLattice();
				Vector[] cellVector = struct.getCellVectors();
				int[][] superToDirect = new int[cellVector.length][cellVector.length];
				try {
					String numTotKPtsLine = grepFirstMatch("generate k-points for:", OUTCARPath);
					ArrayList<Double> kPts = getDoublesAmongString (numTotKPtsLine, delim);
					Double totKpts = kPts.get(0) * kPts.get(1) * kPts.get(2);
					m_TotKPoints = totKpts.intValue();
					for (int a = 0; a < cellVector.length; a++) {
						superToDirect[a][a] = kPts.get(a).intValue();
					}
				} catch (RuntimeException e) {
					System.out.println("Cannot find the total number of k-points in the OUTCAR, calculation has an error");
					m_TotKPoints = -1;
					m_FatalError = true;
				}
				SuperLattice sLat = new SuperLattice(lat, superToDirect);
				m_RecordedMinDist = sLat.getMinPeriodicDistance();
				m_RecordedKSpacing = 2 * Math.PI * sLat.getInverseLattice().getMinPeriodicDistance();
				m_ThirdShortestRVectorLength = sLat.getLongestCompactVectorLength();
				m_ThirdShortestKVectorLength = sLat.getInverseLattice().getLongestCompactVectorLength();
				try {
					String distinctKPtsLine = grepFirstMatch("irreducible k-points:", OUTCARPath);
					Double numDistinct = getDoublesAmongString (distinctKPtsLine, delim).get(0);
					m_DistinctKPoints = numDistinct.intValue();
				} catch (RuntimeException e) {
					System.out.println("Cannot find the number of distinct k-points in the OUTCAR, calculation could not map the grid");
					m_DistinctKPoints = -1;
					m_FatalError = true;
				}
			}
			try {
				String realTimeLine = grepFirstMatch("Total CPU time used", OUTCARPath);
				m_CPUTime = getDoublesAmongString(realTimeLine, delim).get(0);
			} catch (RuntimeException e) {
				m_CPUTime = Double.NaN;
				System.out.println("Cannot get CPUTime calculation either failed due to errors or has not finished!");
				m_FatalError = true;
			}
			try {
				String numCPU = grepFirstMatch("running on", OUTCARPath);
				m_CoreNum = getDoublesAmongString (numCPU, delim).get(0).intValue();
			} catch (RuntimeException e) {
				System.out.println("Cannot get total number of cores, calculation prematurely failed");
				m_CoreNum = -1;
				m_FatalError = true;
			}
			m_Time = m_CoreNum * m_CPUTime;
			m_TimePerDistKPts = m_Time / m_DistinctKPoints;
			m_TimePerPoints = m_Time/m_TotKPoints;
			m_TimePerPointsTimesDistKPts = m_TimePerPoints * m_DistinctKPoints;
			
			/*String VASPWarning = grepFirstMatch("VERY BAD NEWS", calcPath + "/stdout");
			//String BZINTS = grepFirstMatch("BZINTS", calcPath + "/stdout"); //BZINTS error is caused by #k-points < 4 in the tetrahedron method
			if (VASPWarning == null) {
				m_FlaggedForWarning = false;
			} else {
				m_FlaggedForWarning = true;
			}*/
		}
		
		public double getRecordedMinDistance() {
			return m_RecordedMinDist;
		}
		
		public double getRecordedKSpacing() {
			return m_RecordedKSpacing;
		}
		
		public double getEnergy() {
			return m_Energy;
		}
		
		public void setEnergyDifference(double error) {
			m_EneDiffWSmallest = error;
		}
		
		public double getEnergyDifference() {
			return m_EneDiffWSmallest;
		}
		
		public void setLowestSpacingEne (double lowestSpaEne) {
			m_LowestSpacingEneInColl = lowestSpaEne;
		}
		
		public double getLowestSpacingEne() {
			return m_LowestSpacingEneInColl;
		}
		
		public double getTotalTime() {
			return m_Time;
		}
		
		public double getCPUTime() {
			return m_CPUTime;
		}
		
		public double getKSpacing() {
			return m_KSpacing;
		}
		
		public double getEnergyPerAtom() {
			return m_EnergyPerAtom;
		}
		
		public double getTimePerDistinctKPoints() {
			return m_TimePerDistKPts;
		}
		
		public double getTimePerPoints() {
			return m_TimePerPoints;
		}
		
		public double getTimePerPointsTimesDistKPts() {
			return m_TimePerPointsTimesDistKPts;
		}
		
		public double getDirectBandGap() {
			return m_DirectBandGap;
		}
		
		public double getIndirectBandGap() {
			return m_IndirectBandGap;
		}
		
		public double getISpin() {
			return m_ISpin;
		}
		
		public double getISmear() {
			return m_ISmear;
		}
		
		public double getNupdown() {
			return m_Nupdown;
		}
		
		public double definingVolume() {
			return m_Volume;
		}
		
		public double primVolume() {
			return m_PrimVolume;
		}
		
		public double primMinPeriodicDistance() {
			return m_PrimMinPeriodicDistance;
		}
		
		public double getLongestCompactKVector() {
			return m_ThirdShortestKVectorLength;
		}
		
		public double getLongestCompactRVector() {
			return m_ThirdShortestRVectorLength;
		}
		public double getPrimitiveReciprocalMinPeriodicDistance() {
			return m_PrimitiveReciprocalMPD;
		}
		public double getPrimitiveRealMinPeriodicDistance() {
			return m_PrimitiveRealMPD;
		}
		public int getNumAtoms() {
			return m_NumAtoms;
		}
		
		public int getTotalKPoints() {
			return m_TotKPoints;
		}
		
		public int getNumDistinctKPoints() {
			return m_DistinctKPoints;
		}
		
		public int getNumCores() {
			return m_CoreNum;
		}
		
		public int arithNum() {
			return m_ArithNum;
		}
		
		public int lastElectStepNum() {
			return m_LastElectronicStep;
		}
		
		public int numDefiningSites() {
			return m_NumSites;
		}
		
		public int numPrimDefiningSites() {
			return m_NumPrimSites;
		}
		
		public String getStructureID() {
			return m_ICSD_ID;
		}
		
		public String getComposition() {
			return m_Composition;
		}
		
		public String getPath() {
			return m_Path;
		}
		
		public String getAlgo() {
			return m_Algo;
		}
		
		public boolean hasTetrahedronProblem() {
			return m_TetProblem;
		}
		
		public boolean hasSmearingProblem() {
			return m_SmearProblem;
		}
		
		public void setCleanCalculationsConvergence(boolean converged) {
			m_AreCalculationsConvergedClean = converged;
		}
		
		public boolean areCalculationsConvergedClean() {
			return m_AreCalculationsConvergedClean;
		}
		
		public void setDirtyCalculationsConvergence(boolean converged) {
			m_AreCalculationsConvergedDirty = converged;
		}
		
		public boolean areCalculationsConvergedDirty() {
			return m_AreCalculationsConvergedDirty;
		}
		
		public boolean isElectronicallyConverged() {
			return m_IsLastElectronicallyConverged;
		}
		
		public void isPointofCleanConvergence(boolean pointOfConvergence) {
			m_PointOfCleanConvergence = pointOfConvergence;
		}
		
		public boolean pointOfCleanConvergence() {
			return m_PointOfCleanConvergence;
		}
		
		public void isPointOfDirtyConvergence(boolean pointOfConvergence) {
			m_PointOfDirtyConvergence = pointOfConvergence;
		}
		
		public boolean pointOfDirtyConvergence() {
			return m_PointOfDirtyConvergence;
		}
		public boolean errorsExist() {
			return m_AnyErrors;
		}
		public boolean readingError() {
			return m_FatalError;
		}
		
		/*public boolean isFlaggedForWarning() {
			return m_FlaggedForWarning; 
		}*/
		
		public void scanAlgo() throws FileNotFoundException {
			String INCARPath = m_Path + "/INCAR";
			File file = new File(INCARPath);
			Scanner scanner = new Scanner(file);
			m_Algo = "Normal";
			while (scanner.hasNextLine()) {
				String currentLine = scanner.nextLine();
				if(currentLine.contains("#")) {
					continue;
				} else {
					if (currentLine.contains("ALGO")) { //I don't care about older VASP versions (IALGO=8)
						if(currentLine.contains("38")) {
							break;
						}
						if (currentLine.contains("48") || 
								currentLine.substring(currentLine.lastIndexOf("=") + 1).substring(0, 1).equals("V") || 
								currentLine.substring(currentLine.lastIndexOf("=") + 1).substring(0, 1).equals("v")) {
							m_Algo = "Very_Fast";
							break;
						}
						//String a = currentLine.substring(currentLine.lastIndexOf("=") + 1).substring(0, 1);
						if (currentLine.substring(currentLine.lastIndexOf("=") + 1).substring(0, 1).equals("F") || 
								currentLine.substring(currentLine.lastIndexOf("=") + 1).substring(0, 1).equals("f")) {
							m_Algo = "Fast";
							break;
						}
					}
				}
			}
			scanner.close();
		}
		
		public boolean scanErrors() throws FileNotFoundException {
			boolean anyErrors = false;
			String stdoutPath = m_Path + "/stdout";
			File file = new File (stdoutPath);
			Scanner scanner = new Scanner(file);
			while(scanner.hasNextLine()) {
				String currentLine = scanner.nextLine();
				if (currentLine.contains("Tetrahedron method fails for NKPT<4") ||
						currentLine.contains("Fatal error detecting k-mesh") ||
						currentLine.contains("Fatal error: unable to match k-point") ||
						currentLine.contains("Routine TETIRR needs special values")) {
					m_Err_Tet = true;
					anyErrors = true;
				}
				if (currentLine.contains("inverse of rotation matrix was not found") || 
						currentLine.contains("(increase SYMPREC)")) {
					m_InvRotMat = true;
					anyErrors = true;
				}
				if (currentLine.contains("BRMIX: very serious problems")) {
					m_Brmix = true;
					anyErrors = true;
				}
				if (currentLine.contains("WARNING: Sub-Space-Matrix is not hermitian in DAV")) {
					m_SubSpaceMatrix = true;
					anyErrors = true;
				}
				if (currentLine.contains("Routine TETIRR needs special values")) {
					m_Tetirr = true;
					anyErrors = true;
				}
				if (currentLine.contains("Could not get correct shifts")) {
					m_IncorrectShift = true;
					anyErrors = true;
				}
				if (currentLine.contains("REAL_OPTLAY: internal error") ||
						currentLine.contains("REAL_OPT: internal ERROR")) {
					m_RealOptlay = true;
					anyErrors = true;
				}
				if (currentLine.contains("ERROR RSPHER")) {
					m_Rspher = true;
					anyErrors = true;
				}
				if (currentLine.contains("DENTET")) {
					m_Dentnet = true;
					anyErrors = true;
				}
				if (currentLine.contains("TOO FEW BANDS")) {
					m_TooFewBands = true;
					anyErrors = true;
				}
				if (currentLine.contains("ERROR: the triple product of the basis vectors")) {
					m_TripleProduct = true;
					anyErrors = true;
				}
				if (currentLine.contains("Found some non-integer element in rotation matrix")) {
					m_RotMatrix = true;
					anyErrors = true;
				} 
				if (currentLine.contains("BRIONS problems: POTIM should be increased")) {
					m_Brions = true;
					anyErrors = true;
				}
				if (currentLine.contains("internal error in subroutine PRICEL")) {
					m_Pricel = true;
					anyErrors = true;
				}
				if (currentLine.contains("LAPACK: Routine ZPOTRF failed")) {
					m_Zpotrf = true;
					anyErrors = true;
				}
				if (currentLine.contains("One of the lattice vectors is very long (>50 A), but AMIN")) {
					m_Amin = true;
					anyErrors = true;
				}
				if (currentLine.contains("ZBRENT: fatal internal in")) {
					m_Zbrent = true;
					anyErrors = true;
				}
				if (currentLine.contains("WARNING: small aliasing (wrap around) errors must be expected")) {
					m_Aliasing = true;
					//anyErrors = true;
				}
				if (currentLine.contains("Your FFT grids (NGX,NGY,NGZ) are not sufficient")) {
					m_AliasingIncar = true;
					//anyErrors q= true;
				}
				if (currentLine.contains("ERROR in subspace rotation PSSYEVX")) {
					m_Pssyevx = true;
					anyErrors = true;
				}
				if (currentLine.contains("WARNING in EDDRMM: call to ZHEGV failed")) {
					m_Eddrmm = true;
					anyErrors = true;
				}
				if (currentLine.contains("Reciprocal lattice and k-lattice belong to different class of lattices. Often results are still useful...")) {
					m_Ibzkpt = true;
					anyErrors = true;
				}
				if (currentLine.contains("Tetrahedron method fails")) {
					m_Nkpts = true;
					anyErrors = true;
				}
				if (currentLine.contains("Found some non-integer element in rotation matrix")) {
					m_Sgrcon = true;
					anyErrors = true;
				}
				if (currentLine.contains("Fatal error: unable to match k-point")) {
					m_Ibzkpt_Fatal = true;
					anyErrors = true;
				}
				if (currentLine.contains("Could not get correct shifts")) {
					m_Ibzkpt_Shift = true;
					anyErrors = true;
				}
				if (currentLine.contains("Fatal error detecting k-mesh type")) {
					m_Ibzkpt_MeshType = true;
					anyErrors = true;
				}
			}
			scanner.close();
			return anyErrors;
		}
		
		public boolean tetErr() {
			return m_Err_Tet;
		}
		public boolean invRotMatErr() {
			return m_InvRotMat;
		}
		public boolean brmixErr() {
			return m_Brmix;
		}
		public boolean subSpaceMatrixErr() {
			return m_SubSpaceMatrix;
		}
		public boolean tetirrErr() {
			return m_Tetirr;
		}
		public boolean incorrectShiftErr() {
			return m_IncorrectShift;
		}
		public boolean realOptlayErr() {
			return m_RealOptlay;
		}
		public boolean rspherErr() {
			return m_Rspher;
		}
		public boolean dentnetErr() {
			return m_Dentnet;
		}
		public boolean tooFewBandsErr() {
			return m_TooFewBands;
		}
		public boolean tripleProdErr() {
			return m_TripleProduct;
		}
		public boolean rotMatErr() {
			return m_RotMatrix;
		}
		public boolean brionsErr() {
			return m_Brions;
		}
		public boolean pricelErr() {
			return m_Pricel;
		}
		public boolean zpotrfErr() {
			return m_Zpotrf;
		}
		public boolean aminErr() {
			return m_Amin;
		}
		public boolean zbrentErr() {
			return m_Zbrent;
		}
		public boolean aliasingErr() {
			return m_Aliasing;
		}
		public boolean aliasingIncarErr() {
			return m_AliasingIncar;
		}
		public boolean pssyevxErr() {
			return m_Pssyevx;
		}
		public boolean eddrmmErr() {
			return m_Eddrmm;
		}
		public boolean ibzkptErr() {
			return m_Ibzkpt;
		}
		public boolean nkptsErr() {
			return m_Nkpts;
		}
		public boolean sgrconErr() {
			return m_Sgrcon;
		}
		public boolean ibzkptFatalErr() {
			return m_Ibzkpt_Fatal;
		}
		public boolean ibzkptShiftErr() {
			return m_Ibzkpt_Shift;
		}
		public boolean ibzkpt_MeshType() {
			return m_Ibzkpt_MeshType;
		}
		
		protected double[] getBandGaps(String OUTCARPath) throws FileNotFoundException {
			ArrayList<Double[]> tips = new ArrayList<>();
			double directGap = Double.POSITIVE_INFINITY;
			double pastVal = Double.NaN;
			double pastOcc = Double.NaN;
			double currVal = Double.NaN;
			double currOcc = Double.NaN;
			boolean read = false;
			//boolean start = false;
			String delim = "[/,\\[\\]\\s]";
			File file = new File(OUTCARPath);
			Scanner scanner = new Scanner(file);
			while(scanner.hasNextLine()) {
				String currentLine = scanner.nextLine();
				if (currentLine.contains(" k-point")) { //safety check in case no bandgap was found
					read = false;
					continue;
				}
				if (currentLine.contains("  band No.  band energies     occupation")) {
					read = true;
					pastVal = Double.NaN; //reset checker
					currVal = Double.NaN;
					continue;
				}
				if (read) {
					pastVal = currVal; //safe history
					pastOcc = currOcc;
					ArrayList<Double> row = getDoublesAmongString(currentLine, delim);
					currVal = row.get(1); //update currentValue
					currOcc = row.get(2);
					if (Double.isNaN(pastVal)) { //first step, so move on
						continue;
					}
					if (currOcc < pastOcc) { //we found a bandgap
						if (currOcc > 0) { //partial occupancy, it's a metal
							scanner.close();
							return new double[] {0,0};
						}
						if (currOcc < 0) { //some negative number, treat it as 0
							currOcc = 0;
						}
						double val = currVal - pastVal;
						directGap = Math.min(directGap, val);
						Double[] valCon = new Double[] {pastVal, currVal}; //0 is valence tip, 1 is conduction tip
						tips.add(valCon);
						read = false;
					}
				}
				if(currentLine.contains("soft charge-density along")) { //done, no need to continue reading
					break;
				}
			}
			scanner.close();
			double valenceVal = Double.NEGATIVE_INFINITY; //valence
			double conductionVal = Double.POSITIVE_INFINITY; //conduction
			for (int a = 0; a < tips.size(); a++) {
				conductionVal = Math.min(conductionVal, tips.get(a)[1]);
				valenceVal = Math.max(valenceVal, tips.get(a)[0]);
			}
			double indirectGap = conductionVal - valenceVal;
			if (indirectGap < 0) {
				indirectGap = 0;
			}
			double[] bandGaps = new double[] {directGap, indirectGap}; //0 is direct, 1 is indirect
			return bandGaps;
		}
		
		protected int getTotalKPoints (int distKPoints, String calcPath) throws FileNotFoundException {
			int totKPoints = 0;
			String kPointFile = calcPath + "/KPOINTS";
			String delim = "[/,\\[\\]\\s+!]";
			for (int a = 4; a < (distKPoints+4); a++) {
				String currentLine = getLineNumX(a, kPointFile);
				String[] splitted = currentLine.split(delim);
				Double currentNum = Double.parseDouble(splitted[3]);
				totKPoints = totKPoints + currentNum.intValue();
			}
			return totKPoints;
		}
		
		protected ArrayList<Double> getDoublesAmongString (String line, String delim) {
			ArrayList<Double> numbers = new ArrayList<Double>();
			String[] inLine = line.split(delim);
			String number = null;
			for (int count = 0; count < inLine.length; count++) {
				try {
					double temp = Double.parseDouble(inLine[count]);
				} catch (NumberFormatException e) {
					continue;
				}
				number = inLine[count];
				numbers.add(Double.parseDouble(number));
			}
			return numbers;
		}
		
		protected ArrayList<Integer> getIntegersAmongString (String line, String delim) {
			ArrayList<Integer> numbers = new ArrayList<>();
			String[] inLine = line.split(delim);
			String number = null;
			for (int count = 0; count < inLine.length; count++) {
				try {
					int temp = Integer.parseInt(inLine[count]);
				} catch (NumberFormatException e) {
					continue;
				}
				number = inLine[count];
				numbers.add(Integer.parseInt(number));
			}
			return numbers;
		}
		
		protected String grepFirstMatch (String targetString, String filePath) throws FileNotFoundException {
			File file = new File (filePath);
			Scanner scanner = new Scanner(file); //there was a final here
			String lineFound = null;
			while (scanner.hasNextLine()) {
			   String lineFromFile = scanner.nextLine(); // and here, seemed dangerous so I removed them
			   if(lineFromFile.contains(targetString)) { 
			       // a match!
			       //System.out.println("Found " +targetString+ " in file " +file.getName());
			       lineFound = lineFromFile;
			       break;
			   }
			}
			scanner.close();
			return lineFound;
		}
		
		protected ArrayList<String> grepAllMatches (String targetString, String filePath) throws FileNotFoundException {
			File file = new File (filePath);
			Scanner scanner = new Scanner(file);
			ArrayList<String> linesFound = new ArrayList<String>();
			while (scanner.hasNextLine()) {
			   String lineFromFile = scanner.nextLine();
			   if(lineFromFile.contains(targetString)) { 
			       // a match!
			       //System.out.println("Found " +targetString+ " in file " +file.getName());
			       linesFound.add(lineFromFile);
			   }
			}
			scanner.close();
			return linesFound;
		}
		
		
		protected String getLineNumX (int lineNum, String filePath) throws FileNotFoundException {
			File file = new File (filePath);
			final Scanner scanner = new Scanner(file);
			int lineCount = 0;
			String lineFound = null;
			while (scanner.hasNextLine()) {
			   final String lineFromFile = scanner.nextLine();
			   lineCount++;
			   if(lineCount == lineNum) { 
			       // we're done!
			       //System.out.println("Found " +targetString+ " in file " +file.getName());
			       lineFound = lineFromFile;
			       break;
			   }
			}
			scanner.close();
			return lineFound;
		}
		
		protected static double readVASPEnergy(String vaspOutPath, boolean allowNonConverged) throws IOException {
			    
			    BufferedReader reader = new BufferedReader(new FileReader(vaspOutPath));

			    String line = reader.readLine();
			    boolean converged = false;
			    double lastEnergy = Double.NaN;
			    while (line != null) {
			      
			      converged |= line.contains("reached required accuracy");
			      if (line.contains("E0=")) {
			        StringTokenizer st = new StringTokenizer(line, "=");
			        st.nextToken();
			        st.nextToken();
			        String energyLineEnd = st.nextToken();
			        StringTokenizer st2 = new StringTokenizer(energyLineEnd);
			        String energyString = st2.nextToken();
			        lastEnergy = Double.parseDouble(energyString);
			      }
			      line = reader.readLine();
			    }
			    reader.close();
			    if (!converged) {
			      if (!allowNonConverged) {
			        throw new RuntimeException("VASP calculation has not converged.");
			      } else {
			        System.out.println("VASP calculation did not reach required accuracy.");
			      }
			    }
			    return lastEnergy;
			  }
		private static String[] listFilesInFolder(String dir) {
			File dirName = new File(dir);
			String[] fileFilter = new String[dirName.listFiles().length];
			int counter = 0;
		    for (final File fileEntry : dirName.listFiles()) {
		        if (fileEntry.isFile()) {
		        	fileFilter[counter] = fileEntry.getName();
		        	counter++;
		        } 
		    }
		    
		    //now remove all the nulls (i.e. the names of all the directories.)
			List<String> list = new ArrayList<String>();
			for (String s : fileFilter) {
				if(s != null && s.length() > 0) {
					list.add(s);
				}
			}
			fileFilter = list.toArray(new String[list.size()]);
			
			//System.out.println("dirList: " +Arrays.toString(fileFilter));
		    return fileFilter;
		}
		
	}


