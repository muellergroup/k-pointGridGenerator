/*
 * Created on Feb 23, 2019
 *
 */
package generators.filters;

import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.structure.superstructure.SuperLatticeGenerator;
import matsci.structure.superstructure.filters.ISuperLatticeFilter;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayIndexer;
import matsci.util.arrays.ArrayUtils;

public class LowSymmetryFilter  implements ISuperLatticeFilter  {
  
  private int[][][] m_PointOperators;
  private double[][] m_CartesianVectors;
  
  private double m_MinAllowedDistance;
  private int m_ScaleFactor = 1;
  
  private int m_BestKnownDistinctKPoints;
  private double m_BestKnownDistance; // This is only for the lattices with bestKnownDistinctKPoints
  private int[][] m_BestKnownSuperToDirect;
  private double[] m_BestKnownShift;
  
  private double[][] m_Shifts = new double[][] {
    {0.5, 0.5, 0.5},
    {0.5, 0.5, 0},
    {0.5, 0, 0.5},
    {0, 0.5, 0.5},
    {0, 0, 0.5},
    {0, 0.5, 0},
    {0.5, 0, 0},
  };
  
  private SpaceGroup m_SpaceGroup;
  
  private boolean m_IncludeGamma;
  
  private double[] m_LastGoodShift;
  
  // These are so that we don't need to constantly recreate them.
  private int[] m_SuperDimensions;
  private int[][][] m_KPointOperators;
  
  public LowSymmetryFilter(SpaceGroup spaceGroup, double minAllowedDistance, int bestKnownDistinctKPoints, double bestKnownDistance, boolean includeGamma, int scaleFactor) {

    m_SpaceGroup = spaceGroup;
    BravaisLattice lattice = spaceGroup.getLattice();
    m_PointOperators = spaceGroup.getLatticePointOperators(true, true, false);
    m_SuperDimensions = new int[lattice.numPeriodicVectors()];
    m_KPointOperators = new int[m_PointOperators.length][][];
    m_ScaleFactor = scaleFactor;
    
    m_MinAllowedDistance = minAllowedDistance;
    m_BestKnownDistinctKPoints = bestKnownDistinctKPoints;
    m_BestKnownDistance = bestKnownDistance;
    m_IncludeGamma = includeGamma;
    if (includeGamma) {
      m_Shifts = new double[][] {{0, 0, 0}};
    }
    
    m_CartesianVectors = new double[lattice.numPeriodicVectors()][];
    for (int vecNum = 0; vecNum < m_CartesianVectors.length; vecNum++) {
      m_CartesianVectors[vecNum] = lattice.getPeriodicVector(vecNum).getCartesianDirection();
    }
    
  }

  @Override
  public int screenLatticeIndex(int latticeIndex, int[][] superToDirect) {

    superToDirect = MSMath.arrayMultiply(superToDirect, m_ScaleFactor);
    
    // Add 1 to the operation length for the missing identity operation
    int gammaFactor = m_IncludeGamma ? 1 : 0;
    int minPossibleDistinctKPoints = (int) Math.ceil((MSMath.determinant(superToDirect) - gammaFactor) / (1.0 + m_PointOperators.length)) + gammaFactor;
    boolean bestKPointsFound = (minPossibleDistinctKPoints == m_BestKnownDistinctKPoints);

    double minDistance = this.getMinDistance(superToDirect, m_MinAllowedDistance);
    
    if (minDistance < m_MinAllowedDistance - CartesianBasis.getPrecision()) {
      return latticeIndex + 1;
    }
    
    if (bestKPointsFound && (minDistance < m_MinAllowedDistance + CartesianBasis.getPrecision())) {
      return latticeIndex + 1;
    }
    
    int distinctKPoints = this.numDistinctKPoints(superToDirect, minPossibleDistinctKPoints);
    if (distinctKPoints > m_BestKnownDistinctKPoints) {
      return latticeIndex + 1;
    }
    
    if (distinctKPoints < m_BestKnownDistinctKPoints) {
      m_BestKnownDistinctKPoints = distinctKPoints;
      m_BestKnownDistance = minDistance;
      m_BestKnownSuperToDirect = ArrayUtils.copyArray(superToDirect); 
      m_BestKnownShift = m_LastGoodShift;
      return latticeIndex;
    }
      
    if (minDistance > m_BestKnownDistance) {
      m_BestKnownDistance = minDistance;
      m_BestKnownSuperToDirect = ArrayUtils.copyArray(superToDirect);
      if (bestKPointsFound) { // We're just searching for best kpoints now
        m_MinAllowedDistance = minDistance;
      }
      m_BestKnownShift = m_LastGoodShift;
      return latticeIndex;
    }
    
    // Matches everything, so don't use it.
    return latticeIndex + 1;
  }
  
  public BravaisLattice getLattice() {
    return m_SpaceGroup.getLattice();
  }
  
  public double[] getBestKnownShift() {
    return ArrayUtils.copyArray(m_BestKnownShift);
  }
  
  private int numDistinctKPoints(int[][] superToDirect, int minPossibleDistinctKPoints) {
    int returnValue = Integer.MAX_VALUE;
    
    double[][] superVectors = MSMath.matrixMultiply(superToDirect, m_CartesianVectors);
    double[][] transposeSuper = MSMath.transpose(superVectors);
    double[][] kPointVectors = MSMath.simpleInverse(transposeSuper);
    double[][] compactVectors = this.getCompactVectors(kPointVectors);
    double[][] shiftToCompact = MSMath.matrixMultiply(compactVectors, transposeSuper);
    
    for (int shiftNum = 0; shiftNum < m_Shifts.length; shiftNum++) {
      double[] shift = MSMath.vectorTimesMatrix(m_Shifts[shiftNum], shiftToCompact);
      int numDistinctKPoints = numDistinctKPoints(superToDirect, shift);
      if (numDistinctKPoints >= returnValue) {continue;}
      
      m_LastGoodShift = shift;
      returnValue = numDistinctKPoints;
      if (numDistinctKPoints == minPossibleDistinctKPoints) {
        return numDistinctKPoints;
      }
    }
    return returnValue;
  }
  
  private int numDistinctKPoints(int[][] superToDirect, double[] compactShift) {
    
    // Apply the operation in k-space
    double[][] recipSTD = MSMath.simpleLowerTriangularInverse(superToDirect);
    for (int opNum = 0; opNum < m_KPointOperators.length; opNum++) {
      int[][] opInverse = MSMath.matrixMultiply(superToDirect, m_PointOperators[opNum]);
      m_KPointOperators[opNum] = MSMath.round(MSMath.matrixMultiply(opInverse, recipSTD));
    }
    
    int[][] kPointCanonical = SuperLatticeGenerator.toCanonical(MSMath.transpose(superToDirect));

    for (int dimNum = 0; dimNum < m_SuperDimensions.length; dimNum++) {
      m_SuperDimensions[dimNum] = kPointCanonical[dimNum][dimNum];
    }
    
    ArrayIndexer primCellIterator = new ArrayIndexer(m_SuperDimensions);
    
    int currIndex = 0;
    int numDistinctKPoints = 0;
    int[] currKPoint = primCellIterator.getInitialState();
    double[] shiftedPoint = new double[currKPoint.length];
    int[] mappedPointIntArray = new int[currKPoint.length];
    int mappedIndex = -1;
    
    do {
      currIndex = primCellIterator.joinValues(currKPoint);
      
      for (int opNum = 0; opNum < m_KPointOperators.length; opNum++) {
        for (int dimNum = 0; dimNum < currKPoint.length; dimNum++) {
          shiftedPoint[dimNum] = currKPoint[dimNum] + compactShift[dimNum];
        }
        
        // Putting the vector last is faster than taking the transpose of the op.
        double[] mappedPoint = MSMath.matrixTimesVector(m_KPointOperators[opNum], shiftedPoint); 
        for (int dimNum = 0; dimNum < currKPoint.length; dimNum++) {
          double newCoord = mappedPoint[dimNum] - compactShift[dimNum];
          if (Math.abs(newCoord - Math.round(newCoord)) > 1E-2) { // Not symmetry preserving
            //System.out.println("Bad shift! " + newCoord);
            return Integer.MAX_VALUE;
          }
          mappedPointIntArray[dimNum] = (int) Math.round(newCoord);
        }
        this.getInnerPrimCell(mappedPointIntArray, primCellIterator, kPointCanonical);
        mappedIndex = primCellIterator.joinValues(mappedPointIntArray);
        if (mappedIndex < currIndex) {
          break;
        }
      }
      
      if (mappedIndex >= currIndex) {
        numDistinctKPoints++;
      }
      
    } while (primCellIterator.increment(currKPoint));
    
    return numDistinctKPoints;
    
  }
  
  // TODO could make this faster by eliminating the object calls, but this seems to work OK.
  private double[][] getCompactVectors(double[][] vectors) {
    
    double precision = 1E-7;
    boolean minimizing = true;
    while (minimizing) {
      minimizing = false;
      for (int vecNum = 0; vecNum < vectors.length; vecNum++) {
        double[] vector = vectors[vecNum];
        double magSq = MSMath.magnitudeSquared(vector);
        for (int increment = 1; increment < vectors.length; increment++) {
          int vecNum2 = (vecNum + increment) % vectors.length;
          double[] vector2 = vectors[vecNum2];
          double innerProduct = MSMath.dotProduct(vector, vector2) / magSq;
          innerProduct = MSMath.roundWithPrecision(innerProduct, precision);
          double shift = Math.round(innerProduct);
          minimizing |= (shift != 0);
          for (int dimNum = 0; dimNum < vector2.length; dimNum++) {
            vector2[dimNum] -= vector[dimNum] * shift;
          }
        }
      }
    }
    
    // An extra step that is necessary in three dimensions.
    if (vectors.length == 3) {
      int eps01 = (int) Math.signum(MSMath.dotProduct(vectors[0], vectors[1]));
      int eps02 = (int) Math.signum(MSMath.dotProduct(vectors[0], vectors[2]));
      int eps12 = (int) Math.signum(MSMath.dotProduct(vectors[1], vectors[2]));
  
      if (eps01 * eps02 * eps12 == -1) {
        
        double[] placeHolder = new double[] {
          MSMath.dotProduct(vectors[0], vectors[0]),
          MSMath.dotProduct(vectors[1], vectors[1]),
          MSMath.dotProduct(vectors[2], vectors[2]),
        };
        
        int maxIndex = ArrayUtils.maxElementIndex(placeHolder);
        double maxMagSq = placeHolder[maxIndex];
        
        // Reuse the same array
        double newMagSq = 0;
        for (int dimNum = 0; dimNum < vectors[0].length; dimNum++) {
          placeHolder[dimNum] = vectors[0][dimNum] - vectors[1][dimNum] * eps01 - vectors[2][dimNum] * eps02;
          newMagSq += placeHolder[dimNum] * placeHolder[dimNum];
        }
        
        if (newMagSq < maxMagSq) {
          vectors[maxIndex] = placeHolder;
        }
      }
    }
    
    return vectors;
    
    // The below code is simpler but probably a little slower.
    // The new code reduced dependencies on the matsci library.
    /*BravaisLattice lattice = new BravaisLattice(kPointVectors, CartesianBasis.getInstance());
    //Vector[] compactVectors =  lattice.getConventionalLattice().getPeriodicVectors();
    Vector[] compactVectors =  lattice.getCompactLattice().getPeriodicVectors();
    double[][] returnArray = new double[kPointVectors.length][];
    for (int vecNum = 0; vecNum < returnArray.length; vecNum++) {
      returnArray[vecNum] = compactVectors[vecNum].getCartesianDirection();
    }
    return returnArray;*/
    
  }
  
  private void getInnerPrimCell(int[] primCellLocation, ArrayIndexer primCellIterator, int[][] superToDirect) {
    
    for (int coordNum = primCellLocation.length - 1; coordNum >= 0; coordNum--) {
      int innerCoord = primCellLocation[coordNum];
      for (int row = coordNum + 1; row < superToDirect.length; row++) {
        
        int rowDimensionSize = primCellIterator.getDimensionSize(row);
        int shiftedCoord = primCellLocation[row];
        if (shiftedCoord < 0) {  // Account for how Java handles negatives in integer division
          innerCoord -= ((shiftedCoord - rowDimensionSize + 1) / rowDimensionSize) * superToDirect[row][coordNum];
        } else {
          innerCoord -= (shiftedCoord / rowDimensionSize) * superToDirect[row][coordNum];
        }
      }
      primCellLocation[coordNum] = innerCoord;
    }
    
    // Now take the modulo of the coordinates
    for (int coordNum = 0; coordNum < primCellLocation.length; coordNum++) {
      int innerCoord = primCellLocation[coordNum] % primCellIterator.getDimensionSize(coordNum);
      primCellLocation[coordNum] = innerCoord < 0 ? innerCoord + primCellIterator.getDimensionSize(coordNum) : innerCoord;
    }
    
  }
  
  private double getMinDistance(int[][] superToDirect, double minAllowedDistance) {
    
    double[][] superVectors = MSMath.matrixMultiply(superToDirect, m_CartesianVectors);
    double minMagSq = Double.POSITIVE_INFINITY;
    
    double precision = 1E-7;
    boolean minimizing = true;
    while (minimizing) {
      minimizing = false;
      for (int vecNum = 0; vecNum < superVectors.length; vecNum++) {
        double[] vector = superVectors[vecNum];
        double magSq = MSMath.magnitudeSquared(vector);
        for (int increment = 1; increment < superVectors.length; increment++) {
          int vecNum2 = (vecNum + increment) % superVectors.length;
          double[] vector2 = superVectors[vecNum2];
          double innerProduct = MSMath.dotProduct(vector, vector2) / magSq;
          innerProduct = MSMath.roundWithPrecision(innerProduct, precision);
          double shift = Math.round(innerProduct);
          minimizing |= (shift != 0);
          for (int dimNum = 0; dimNum < vector2.length; dimNum++) {
            vector2[dimNum] -= vector[dimNum] * shift;
          }
          minMagSq = Math.min(MSMath.magnitudeSquared(vector2), minMagSq);
        }
      }
    }
    
    // An extra step that is necessary in three dimensions.
    if (superToDirect.length == 3) {
      int eps01 = (int) Math.signum(MSMath.dotProduct(superVectors[0], superVectors[1]));
      int eps02 = (int) Math.signum(MSMath.dotProduct(superVectors[0], superVectors[2]));
      int eps12 = (int) Math.signum(MSMath.dotProduct(superVectors[1], superVectors[2]));
  

      if (eps01 * eps02 * eps12 == -1) {
        for (int dimNum = 0; dimNum < superVectors[0].length; dimNum++) {
          superVectors[0][dimNum] -= (superVectors[1][dimNum] * eps01 + superVectors[2][dimNum] * eps02);
        }
        minMagSq = Math.min(MSMath.magnitudeSquared(superVectors[0]), minMagSq);
      }
    }
    
    return Math.sqrt(minMagSq);
  }
  
  public double getBestKnownDistance() {
    return m_BestKnownDistance;
  }
  
  public int getBestKnownDistinctKPoints() {
    return m_BestKnownDistinctKPoints;
  }
  
  public int[][] getBestKnownSuperToDirect() {
    return ArrayUtils.copyArray(m_BestKnownSuperToDirect);
  }
  
  public boolean foundLattice() {
    return (m_BestKnownSuperToDirect != null) && (m_BestKnownShift != null);
  }
  
}
