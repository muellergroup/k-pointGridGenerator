import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.List;

import generators.GapStretcher;
import generators.KPointLatticeCollection;
import generators.KPointLibrary;
//import generators.LowSymmetryGenerator;
import generators.DynamicLatticeGenerator;
import generators.KPointLibrary.SYMMETRY_TYPE;
import kptServer.INCAR;
import kptServer.ServerInput;
import matsci.Species;
import matsci.io.vasp.KPOINTS;
import matsci.io.vasp.POSCAR;
import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.structure.reciprocal.KPointLattice;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.symmetry.LatticeSymmetryFinder;
import matsci.structure.symmetry.StructureSymmetryFinder;
import matsci.structure.symmetry.SymmetryException;
import qe.QENamelists;
import utils.TextFileParser;

/**
 * The core class for k-point generation. Each valid case is wrapped in one
 * calculator. But the minDistance Collection is loaded only once when the
 * application is called. Such atomization of cases is designed for future
 * multi-threading and parallel implementation.
 */
public class KPointCalculator {
    
    // Class variables
    /**
     * If precision is high, some symmetry operations might be missing and lead to 
     * indetermination of space group. When this happen, we loosen the precision, and try
     * to determine the symmetry again. The number of such cycles is 4.    
     */
    public static int m_NumLoosenPrecision = 4;
    
    /**
     * Version number in output files for a logging purpose.
     */
    public static String m_Version = "2020.11.25";

    private static String collectionDirectory = System.getProperty("LATTICE_COLLECTIONS") 
            + System.getProperty("file.separator") + "minDistanceCollections";
    
    
    /**
     * Initialize the program. 
     * Set some static fields for global usage: precision and flags for symmetry determination.
     * Precision is set as 3 times the value VASP uses.
     */
    public static void init() {
        CartesianBasis.setPrecision(3E-6); 
        CartesianBasis.setAngleToleranceDegrees(1E-3);
        StructureSymmetryFinder.allowArtificialGroupCompletion(false);
        LatticeSymmetryFinder.allowArtificialGroupCompletion(false);
    }
    
    //Instance variables
    private KPointLibrary m_Library;
    private File workingDir;
    private ServerInput inputParams;
    private PrintWriter logger;
    
    /**
     * Constructor.
     * @param workingDir The path to a valid case for k-point grid generation.
     */
    public KPointCalculator(File workingDir) {
        this.workingDir = workingDir;
        this.inputParams = new ServerInput();
        this.m_Library = new KPointLibrary();
        try {
            this.logger = new PrintWriter(new BufferedWriter(
                    new FileWriter(this.workingDir.getAbsolutePath() + "/KPOINTS.log")));
            logger.println("Precision : " + CartesianBasis.getPrecision());
            logger.println("AngleTolerance: " 
                    + CartesianBasis.getAngleToleranceDegrees());
        } catch (IOException e) {
            this.reportWarning("Failed to create a log for this process: " + this.workingDir.getAbsolutePath());
        }
    }
    
    /**
     * The work-horse for k-point grid generation.
     * 
     * @return 0 if k-points grid is successfully generated.
     *         1 if errors occur. A debug message and the stack trace are printed to KPOINTS.log.
     */
    public int process() {
        long startTime = System.currentTimeMillis();
        
        //--------- Parsing the input files: INCAR, PRECALC, POSCAR, input file for QE. ----------//
        this.inputParams.setFilePath(this.workingDir.getAbsolutePath());
        List<File> fileItems = Arrays.asList(this.workingDir.listFiles());
        boolean hasPRECALC = false;
        boolean poscarOK = false;
        for (File fi: fileItems) {
            String fileName = fi.getName();
            if (fileName.equals("INCAR")) {
                long sINC = System.currentTimeMillis();
                INCAR incar = handleINCAR(fi);
                if (incar != null) {
                	inputParams.includeINCAR(true);
                	inputParams.setINCAR(incar);
                }
                long fINC = System.currentTimeMillis();
                inputParams.setReadINCARTime(fINC-sINC);
            } else if (fileName.equals("PRECALC")) {
                long sPRE = System.currentTimeMillis();
                hasPRECALC = handleInputFile(fi);
                long fPRE = System.currentTimeMillis();
                inputParams.setReadPRECALCTime(fPRE - sPRE);
            } else if (fileName.equals("POSCAR")) {
                long sStr = System.currentTimeMillis();
                poscarOK = handlePOSCAR(fi);
                long fStr = System.currentTimeMillis();
                inputParams.setStructureTime(fStr - sStr);
            } else { 
                //not something of known file name, assume a file from a software package with
                //flexible naming
                if (inputParams.getFormat().contains("qe") && inputParams.isBetaMode()) {
                    boolean otherInputOK = handleQEInputFile(fi);
                }
            }
        }
        if (!poscarOK) {
            this.reportError("Failed at processing POSCAR. "
                   + "There is a problem with your POSCAR. "
                   + "Please check to see if it exists and is reasonable, "
                   + "and contact us if the problem persists.");
            printSummary(inputParams, false);
            this.cleanUp();
            
            return 1;
        }
        
        if (inputParams.getFormat().contains("vasp")) {
            if (!poscarOK || inputParams.getPOSCAR().numDefiningSites() < 1) {
            	this.reportError("Failed at processing POSCAR. "
            	       + "There is a problem with your input structure file. " 
                       + "Please check to see if it is reasonable, "
                       + "and contact us if the problem persists.");
                printSummary(inputParams, false);
                this.cleanUp();
                return 1;
            }
        }
        
        if (!hasPRECALC) { 
            this.reportWarning("Cann't detect PRECALC file. Use default values: MINDDISTANCE=28.1, "
                             + "INCLUDEGAMMA=AUTO");
            // create a default one to use, and delete it later.
            File f = new File(this.workingDir.getPath() + "/PRECALC");
            try (PrintWriter w = new PrintWriter(new FileWriter(this.workingDir.getPath() + "/PRECALC"))) {
                w.println("MINDISTANCE=28.1");
                w.println("INCLUDEGAMMA=AUTO");
            } catch (Exception e) {
            	this.reportError("Cann't detect PRECALC file and cannot create a default one.");
                this.cleanUp();
            }
            handleInputFile(f);
            f.delete();
        }
        //---------------------------------- Parsing ended here. ---------------------------------//

        //---------------- Determine the symmetry type of user input structure(s). ---------------//
        
        // Use a structure array for future expansion to ABINIT.
        Structure[] inStructure = inputParams.getStructure();
        if (inStructure.length == 0) {
        	this.reportError("There is a problem with your input structure file. "
        	      + "Please check to see if it is reasonable, and contact us "
                  + "if the problem persists.");
            printSummary(inputParams, false);
            this.cleanUp();
            return 1;
        }
       
        // Trying to find the space group of a given structure...
        Structure currentStruct = inStructure[0];
        try {
            if (currentStruct.containsCoincidentSites() != -1) {
                Coordinates directCoords = currentStruct.getSiteCoords(
                        currentStruct.containsCoincidentSites());
                Coordinates cartCoords = directCoords.getCartesianCoords();
                String errorLine = "The structure input file contains at least "
                        + "one duplicate site at " 
                        + Arrays.toString(directCoords.getArrayCopy()) 
                        + " in direct coordinates or " + Arrays.toString(cartCoords.getArrayCopy()) 
                        + " in Cartesian coordinates.";
                throw new RuntimeException(errorLine);
            }
            inputParams.setAlteredStructure(currentStruct);
            
            // kppraTotalKPoints = KPPRA * (1 / numAtoms)
            // This is separate from handleInputFile() since we don't know the number of atoms yet
            // when we parse the input files.
            if (inputParams.usingKPPRA()) { 
                int kppra = inputParams.getKPPRA();
                int numAtoms = currentStruct.numDefiningSites();
                int kppraInTotPoints = (int) Math.ceil(1.0 * kppra/numAtoms);
                int minTotalPointsToUse = Math.max(kppraInTotPoints, 
                        inputParams.getMinTotalPoints());
                inputParams.setMinTotalPoints(minTotalPointsToUse);
            }
            
            if (inputParams.getFormat().contains("vasp")) {
                currentStruct = adjustVASPStructure(inputParams, currentStruct, 
                        this.workingDir.getAbsolutePath());
                inputParams.setAlteredStructure(currentStruct);
                // Now uses SpaceGroup as a criteria, consistent with the KPointLattice generation.
                // This is the major function which attempts to assign a space group to the input 
                // structure.
                if (inputParams.getAlteredSpaceGroup() == null) {
                    this.reportError("There is a problem with your INCAR file. "
                           + "Please check the number of ions specified in the MAGMOM flag and "
                           + "make sure it matches with the number of atoms in POSCAR. "
                           + "In addition, make sure the \"*\" sign is directly adjacent "
                           + "to the values. Please contact us if the problem persists.");
                    printSummary(inputParams, false);
                    this.cleanUp();
                    return 1;
                }
            } else if (inputParams.getFormat().contains("qe")) {
                currentStruct = adjustQEStructure(inputParams,currentStruct, 
                        this.workingDir.getAbsolutePath());
                inputParams.setAlteredStructure(currentStruct);
                if (inputParams.getAlteredSpaceGroup() == null) {
                    this.reportError("There is a problem when processing your Quantum Espresso "
                    		+ "input parameters. Please contact us if the problem persists.");
                    printSummary(inputParams, false);
                    this.cleanUp();
                    return 1;
                }
            }
        } catch (RuntimeException e) {
            String additionalMessage = e.getMessage();
            if (additionalMessage == null) {
                additionalMessage = "Please check your files to see if all symmetry-reducing "
                        + "flags are correctly stated. ";
            }
            this.reportError("There is a problem processing your structure. "
                    + additionalMessage + "Please contact us if the problem persists.", e);
            printSummary(inputParams, false);
            this.cleanUp();
            return 1;
        }
        //-------------------------- Symmetry determination ended here ---------------------------//
        
        //---------------------------- Start generating k-point grid -----------------------------//
        long gridStart = System.currentTimeMillis();
        
        // Grid for QE:
        if (inputParams.getFormat().contains("qe")) {
            try {
                QENamelists qe = null; // QE IO object.
                boolean genOK = false;
                int genCounter = 0;
                while(!genOK) {                                               
                    try {
                        // Load only the lattice collection of the space group of the current 
                        // structure (it might be scaled within this loop).
                        if (! this.loadKPointLibrary(inputParams)) {
                            this.printSummary(inputParams, false);
                            this.cleanUp();
                            return 1;
                        }
                        qe = appendQEWithKPoints(inputParams, currentStruct);
                        genOK = true;
                        break;
                    } catch (SymmetryException e) {
                        currentStruct = currentStruct.scaleLatticeConstant(2);
                        inputParams.setUsedMinDistance(inputParams.getUsedMinDistance() * 2);
                        inputParams.precisionAdjusted(true);
                        inputParams.setScalingTimes();
                        inputParams.setAlteredStructure(currentStruct);
                    }
                    genCounter++;
                    if (genCounter > m_NumLoosenPrecision) {
                        throw new RuntimeException("Failed to generate grids due to precision. ");
                    }
                }
          
                if (qe == null) {
                    // TODO: remove magic number 27.
                    this.reportError("There is a problem generating k-point grid based on your input. "
                           + "Please note that, based on the currently used search depths, "
                           + "our application will only return grids with up to "
                           + String.format("%,d", inputParams.getTriclinicSearchDepth() * 27)
                           + " total k-points for triclinic structures, "
                           + String.format("%,d", inputParams.getMonoclinicSearchDepth() * 27)
                           + " total k-points for monoclinic structures, "
                           + "1,259,712 total k-points for cubic structures, "
                           + "and 157,464 total k-points structures in all the other lattice systems. "
                           + "Please contact us if the problem persists or if you need grids with more points.");
                    printSummary(inputParams, false);
                    this.cleanUp();
                    return 1;
                }
                
                String origFileName = inputParams.getFileName();
                qe.writeReturnedContent(this.workingDir.getAbsolutePath() 
                        + System.getProperty("file.separator") 
                        + origFileName);
               
                long gridEnd = System.currentTimeMillis();
                long endTime = System.currentTimeMillis();
                String kPtsSent = logOutputFormat("Finished, time spent: " 
                        + (endTime - startTime) + " ms.");
                inputParams.setGridGenerationTime((gridEnd - gridStart));
                inputParams.setTotalGenerationTime((endTime - startTime));
                logger.println(kPtsSent);
            } catch (RuntimeException e) {
                this.reportError("There is a problem generating the grid. "
                        + "Please check your files and contact us if the problem persists.", e);
                printSummary(inputParams, false);
                this.cleanUp();
                return 1;
            }
        } else {
            // Grid for VASP:
            try {
                KPOINTS kpoints = null; // VASP KPOINTS file IO object.
                boolean genOK = false;
                int genCounter = 0;
                while(!genOK) {
                    try {
                        // Load only the lattice collection of the space group 
                        // of the current structure (it might be scaled within this loop).
                        if (! this.loadKPointLibrary(inputParams)) {
                            this.printSummary(inputParams, false);
                            this.cleanUp();
                            return 1;
                        }
                        
                        if (inputParams.minimizeGridWithDiagonal()) {
                            kpoints = generateKPOINTFileWDiagOpti(inputParams, currentStruct);
                        } else {
                            kpoints = generateKPOINTFile(inputParams);
                        }
                        // If no SymmetryException is thrown when attempting to get a k-point grid,
                        // the generation is deemed to be successful and the code breaks the loop
                        // from here.
                        genOK = true;
                        break;
                    } catch (SymmetryException e) {
                        currentStruct = currentStruct.scaleLatticeConstant(2);
                        inputParams.setUsedMinDistance(inputParams.getUsedMinDistance() * 2);
                        inputParams.precisionAdjusted(true);
                        inputParams.setScalingTimes();
                        inputParams.setAlteredStructure(currentStruct);
                    }
                    genCounter++;
                    if (genCounter > m_NumLoosenPrecision) {
                        throw new RuntimeException("Failed to generate grids due to precision.");
                    }    
                }
                
                // Cannot generate any k-point grids, but since it passed so far, most likely 
                // it is due to user's requirement exceed current limit on total number of k-points.
                if (kpoints == null) {
                    // TODO: remove magic number 27.
                    this.reportError("There is a problem generating k-point grid based on your input. "
                           + "Please note that, based on the currently used search depths, "
                           + "our application will only return grids with up to "
                           + String.format("%,d", inputParams.getTriclinicSearchDepth() * 27)
                           + " total k-points for triclinic structures, "
                           + String.format("%,d", inputParams.getMonoclinicSearchDepth() * 27)
                           + " total k-points for monoclinic structures, "
                           + "1,259,712 total k-points for cubic structures, "
                           + "and 157,464 total k-points structures in all the other lattice systems. "
                           + "Please contact us if the problem persists or if you need grids with more points.");
                    printSummary(inputParams, false);
                    this.cleanUp();
                    return 1;
                }
                
                if (inputParams.kPointsAutoSetting()) {
                    kpoints.useAutomaticOutput();
                }

                kpoints.writeFile(this.workingDir.getAbsolutePath() + System.getProperty("file.separator") + "KPOINTS");

                long endTime = System.currentTimeMillis();
                long gridEnd = System.currentTimeMillis();
                String kPtsSent = logOutputFormat("Finished, time spent: " + (endTime - startTime) + " ms.");
                inputParams.setGridGenerationTime((gridEnd - gridStart));
                inputParams.setTotalGenerationTime((endTime - startTime));
                logger.println(kPtsSent);
            } catch (RuntimeException e) {
                this.reportError("There is a problem generating the grid. " + e.getMessage()
                        + "Please check your files and contact us if the problem persists.", e);
                printSummary(inputParams, false);
                this.cleanUp();
                return 1;
            }
        }
        //---------------------------- Finish generating k-point grid -----------------------------//
        printSummary(inputParams, true);
        this.cleanUp();
        return 0;
    }
    
    /**
     * TODO: Review documentation.
     * 
     * Prepare the raw input structure for symmetry detection:
     *      1. Stretch the structure in the direction(s) vacuum has been detected, to ensure
     *         the lattice vectors parallel with the vacuum direction(s) have lengths of at least
     *         MINDISTANCE + GAPDISTANCE.
     *      2. Parse ISYM, ISPIN, LNONCOLLINEAR, and IBRION, and set the removed symmetries 
     *         accordingly. Note: for LNONCOLLINEAR, VASP recommends remove symmetries, and
     *         structural symmetries are removed.
     *      3. Parse the selective dynamics flags and break the symmetries properly.
     *      4. Parse the MAGMOM flag for COLLINEAR calculations and break the symmetries properly.
     *      
     * @param inputParams
     * @param inStructure
     * @param filePath
     * @return the adjusted structure
     */
    public Structure adjustVASPStructure (ServerInput inputParams, Structure inStructure, 
            String filePath) {
        // Stretch the structure.
        if (inputParams.gapAdjusted()) {
            long start = System.currentTimeMillis();
            GapStretcher stretcher = new GapStretcher(inStructure, inputParams.gapDistance(), 
                    inputParams.getMinDistance(), inputParams.getMinTotalPoints());
            Structure scaled = stretcher.getStretchedStructure();
            int realDimension = stretcher.numContiguousDimensions();
            inputParams.setRealDimension(realDimension);
            // Check whether a new structure object has been created.
            if (stretcher.getStretchedStructure() != stretcher.getOriginalStructure()) {
                POSCAR out = new POSCAR(scaled);
                out.writeFile(filePath + "/POSCAR.gapScaled.vasp");
            }
            inStructure = scaled;
            long end = System.currentTimeMillis();
            inputParams.setGapAdjustTime((end - start));
        }
        // The following considers user's INCAR values
        if (inputParams.isINCARIncluded()) {
            INCAR incar = inputParams.getINCAR();
            //Take into account ISYM (LNONCOLLINEAR override this)
            if (!inputParams.gridRemoveSymmetrySettingProvided() 
                    || inputParams.getGridRemoveSymmetrySetting().equals("none")) {
                if (incar.ISYM() == 0) {
                    inputParams.setGridRemoveSymmetrySettingUsed("structural");
                } else if (incar.ISYM() == -1) {
                    inputParams.setGridRemoveSymmetrySettingUsed("all");
                }
            }
            // Phonon / elastic tensor calculations: remove structural symmetries if using 
            // list format; follow user settings if using generating vector format.
            if (incar.IBRION() == 5 || incar.IBRION() == 6 || incar.IBRION() == 7 || incar.IBRION() == 8) {
            	if (!inputParams.kPointsAutoSetting() && incar.ISYM() >= 1) {
	                if (!inputParams.gridRemoveSymmetrySettingProvided()) {
	                    inputParams.setGridRemoveSymmetrySettingUsed("structural");
	                } else if (inputParams.getGridRemoveSymmetrySetting().equals("none")) {
	                	inputParams.setGridRemoveSymmetrySettingUsed("structural");
	                } else if (inputParams.getGridRemoveSymmetrySetting().equals("time_reversal")) {
	                	inputParams.setGridRemoveSymmetrySettingUsed("all");
	                }
            	}
            } 
            // TODO: Review this comments:
            // VASP ISYM behavior for LNONCOLLINEAR & LSORBIT is to use ISYM=2 when not listed, 
            // the server goes to VASP recommended
            // We currently do not handle high symmetry grids
            // We now take into account more symmetry for noncollinear calculations
            /*
             * Phil: VASP recommends ISYM=-1 for LSORBIT calculation (not sure about 
             * LNONCOLLINEAR), since energy results might not be transferable if k-points are 
             * changed between compared calculations. And that's often the case when symmetry is 
             * on.
             * Current code: remove structural symmetry.
             * 
             */
            if (incar.LNONCOLLINEAR()) {
                if (incar.ISYM() > 0) {
                    if (!inputParams.gridRemoveSymmetrySettingProvided() 
                            || inputParams.getGridRemoveSymmetrySetting().equals("none")) {
                        inputParams.setGridRemoveSymmetrySettingUsed("structural");
                    }
                }
                  if (incar.ISYM() > -1) { 
                      if (inputParams.getGridRemoveSymmetrySetting().equals("time_reversal")) {
                          inputParams.setGridRemoveSymmetrySettingUsed("all");
                      }
                  }    
            } else if (incar.ISPIN() == 2 && incar.MAGMOMString() != null && !incar.LNONCOLLINEAR()) {
                long start = System.currentTimeMillis();
                inStructure = modMagneticMoment(inputParams,inStructure);
                long end = System.currentTimeMillis();
                inputParams.setMagMomAdjustTime(end - start);
                if (inStructure == null) {
                    return null;
                }
            }
        }
          
        boolean selectiveDynamics = inputParams.getPOSCAR().getSelectiveDynamics();
        inputParams.selectiveDynamicsIsUsed(selectiveDynamics);
        if (selectiveDynamics) {
            long start = System.currentTimeMillis();
            inStructure = modVASPSelectiveDynamics(inputParams, inStructure);
            long end = System.currentTimeMillis();
            inputParams.setSelectiveDynamicsAdjustTime(end - start);
        }
        return inStructure;
    }
      
    /**
     * Prepare the raw input structure for symmetry detection.
     * 
     * @param inputParams
     * @param inStructure
     * @param filePath
     * @return the adjusted structure with broken symmetries from selective dynamics and magnetic
     *         moments removed.
     */
    public Structure adjustQEStructure (ServerInput inputParams, 
            Structure inStructure, String filePath) {
        //stretch the structure
        if (inputParams.gapAdjusted()) {
            long start = System.currentTimeMillis();
            GapStretcher stretcher = new GapStretcher(inStructure, inputParams.gapDistance(), 
                    inputParams.getMinDistance(), inputParams.getMinTotalPoints());
            Structure scaled = stretcher.getStretchedStructure();
            int realDimension = stretcher.numContiguousDimensions();
            inputParams.setRealDimension(realDimension);
            if (stretcher.getStretchedStructure() != stretcher.getOriginalStructure()) {
                POSCAR out = new POSCAR(scaled);
                out.writeFile(filePath + "/POSCAR.gapScaled.vasp");
            }
            inStructure = scaled;
            long end = System.currentTimeMillis();
            inputParams.setGapAdjustTime((end - start));
        }   
        
        QENamelists namelists = inputParams.getQEData();
      
        // these are QE parameters that remove symmetry
        boolean noSym = namelists.getBooleanValue("&system", "nosym", false);

        // TODO: I don't think we need this.... 
        boolean noSym_evc = namelists.getBooleanValue("&system", "nosym_evc", false); 
        boolean noinv =  namelists.getBooleanValue("&system", "noinv", false);
        if (!inputParams.gridRemoveSymmetrySettingProvided() 
                || inputParams.getGridRemoveSymmetrySetting().equals("none")) {
            if (noSym) {
                inputParams.setGridRemoveSymmetrySettingUsed("structural");
            } else if (noinv){
                inputParams.setGridRemoveSymmetrySetting("time_reversal");
            } else if (noinv && noSym) {
                inputParams.setGridRemoveSymmetrySettingUsed("all");
            }
        }
        
        int arithNum = inStructure.getDefiningSpaceGroup().getSymmorphicGroup()
                .getArithmeticCrystalClassNumber(inStructure.numPeriodicDimensions());
        if (namelists.getBooleanValue("&system", "noncolin", false)
                || namelists.getBooleanValue("&system", "lspinorb", false) ) {
            if (arithNum > 9) { 
                // TODO: review this comment
                // TODO: Talk about this with Tim
                if (!inputParams.gridRemoveSymmetrySettingProvided() 
                        || inputParams.getGridRemoveSymmetrySetting().equals("none")) {
                    inputParams.setGridRemoveSymmetrySettingUsed("structural");
                }
            }
        }
        //no need to handle magnetism it is already automatically separated in QE input file.
        boolean selectiveDynamics = namelists.useSelectiveDynamics();
        inputParams.selectiveDynamicsIsUsed(selectiveDynamics);
        if (selectiveDynamics) {
            long start = System.currentTimeMillis();
            inStructure = modGenericSelectiveDynamics(inputParams, inStructure, 
                    namelists.numDefiningSites(), namelists.selectiveDynamicsArray());
            long end = System.currentTimeMillis();
            inputParams.setSelectiveDynamicsAdjustTime(end - start);
        }
        return inStructure;
    }
      
    /**
     * Parse the PRECALC file.
     * 
     * @param fi The PRECALC file
     * @return true, if PRECALC is parsed correctly. 
     *         false, otherwise.
     */
    public boolean handleInputFile(File fi) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fi))) {
            String currentLine;
            ArrayList<String> failToParse = new ArrayList<>();
            ArrayList<String> unknownParams = new ArrayList<>();
            String del = "[=]";
            while ((currentLine = reader.readLine()) != null) {
                String cleanLine = stripComments(currentLine);
                if (cleanLine.length() == 0 || cleanLine == null) {
                    continue;
                 }
                String[] split = cleanLine.split(del);
                String param = split[0].trim().toUpperCase();
                if (param.length() == 0) {
                    continue;
                }
                if (split.length < 2) {
                    if (param.equals("INCLUDEGAMMA") ||
                            param.equals("MINDISTANCE") ||
                            param.equals("MINTOTALKPOINTS") ||
                            param.equals("HEADER") ||
                            param.equals("FORCEDIAGONAL") ||
                            param.equals("GAPDISTANCE") ||
                            param.equals("KPPRA") ||
                            param.equals("REMOVE_SYMMETRY")||
                            param.equals("LOW_SYMMETRY_SEARCH_DEPTH") ||
                            param.equals("TRICLINIC_SEARCH_DEPTH") ||
                            param.equals("MONOCLINIC_SEARCH_DEPTH") ||
                            param.equals("WRITE_LATTICE_VECTORS") ||
                            param.equals("BETA_MODE") ||
                            param.equals("PRINT_SUPERTODIRECT") ||
                            param.equals("OPTI_DIAG")) {
                         failToParse.add(param);
                         continue;
                    } else if (!param.equals("BETA_MODE") ||
                            !param.equals("OPTI_DIAG") ||
                            !param.equals("PRINT_SUPERTODIRECT")){
                        unknownParams.add(param);
                        continue;
                    }
                }
                String value = split[1].trim();
                if (param.equals("INCLUDEGAMMA")) {
                    if (value.equalsIgnoreCase("TRUE")){
                        inputParams.setShift("TRUE");
                        continue;
                    } else if (value.equalsIgnoreCase("FALSE")) {
                        inputParams.setShift("FALSE");
                        continue;
                    } else if (value.equalsIgnoreCase("AUTO")){
                        inputParams.setShift("AUTO");
                        continue;
                    } else {
                        failToParse.add("INCLUDEGAMMA");
                        continue;
                    }
                }
                if (param.equals("MINDISTANCE")) {
                    inputParams.promptMinDist(true);
                    Double val = TextFileParser.parseDoubleValue(value);
                    if (val == null || Double.isInfinite(val) || Double.isNaN(val)) {
                        failToParse.add("MINDISTANCE");
                        continue;
                    }
                    inputParams.setMinDistance(val);
                    continue;
                }
                if (param.equals("MINTOTALKPOINTS")) {
                    inputParams.promptMinTot(true);
                    Integer val = TextFileParser.parseIntegerValue(value);
                    if (val == null) {
                        failToParse.add("MINTOTALKPOINTS");
                           continue; 
                    }
                    inputParams.setMinTotalPoints(val);
                    continue;
                }
                if (param.equals("HEADER")) {
                    if (value.equalsIgnoreCase("VERBOSE")){
                        inputParams.setHeaderVerbosity("VERBOSE");
                        continue;
                    } else if (value.equalsIgnoreCase("DEBUG")){
                        inputParams.setHeaderVerbosity("DEBUG");
                        continue;
                    } else if (value.equalsIgnoreCase("SIMPLE")){
                        inputParams.setHeaderVerbosity("SIMPLE");
                        continue;
                    } else {
                        failToParse.add("HEADER");
                        continue;
                    }
                }
                if (param.equals("REMOVE_SYMMETRY")) {
                    if (value.equalsIgnoreCase("ALL")) {
                        inputParams.setGridRemoveSymmetrySetting("all");
                        inputParams.setGridRemoveSymmetrySettingUsed("all");
                        inputParams.setGridIgnoreSymmetryProvided(true);
                        continue;
                    } else if (value.equalsIgnoreCase("STRUCTURAL")) {
                        inputParams.setGridRemoveSymmetrySetting("structural");
                        inputParams.setGridRemoveSymmetrySettingUsed("structural");
                        inputParams.setGridIgnoreSymmetryProvided(true);
                        continue;
                    } else if (value.equalsIgnoreCase("TIME_REVERSAL")) {
                        inputParams.setGridRemoveSymmetrySetting("time_reversal");
                        inputParams.setGridRemoveSymmetrySettingUsed("time_reversal");
                        inputParams.setGridIgnoreSymmetryProvided(true);
                        continue;
                    } else if (value.equalsIgnoreCase("NONE")) { 
                        //includes as much symmetry as possible
                        inputParams.setGridRemoveSymmetrySetting("none");
                        inputParams.setGridIgnoreSymmetryProvided(true);
                        continue;
                    } else { 
                        inputParams.setGridRemoveSymmetrySetting("none");
                        continue;
                    }
                }
                if (param.equals("GAPDISTANCE")) {
                    inputParams.promptGap(true);
                    Double val = TextFileParser.parseDoubleValue(value);
                    if (val == null || Double.isInfinite(val) || Double.isNaN(val)) {
                        failToParse.add("GAPDISTANCE");
                        continue;
                    }
                    inputParams.setGapDistance(val);
                    continue;
                }
                if (param.equals("KPPRA")) {
                    inputParams.promptMinTot(true);
                    Integer val = TextFileParser.parseIntegerValue(value);
                    if (val == null) {
                        failToParse.add("KPPRA");
                           continue; 
                    }
                    inputParams.setKPPRA(val);
                    continue;
                }
                
                if (param.equals("LOW_SYMMETRY_SEARCH_DEPTH")) {
                    inputParams.promptLowSymmetrySearchDepth(true);
                    Integer val = TextFileParser.parseIntegerValue(value);
                    if (val == null) {
                        failToParse.add("LOW_SYMMETRY_SEARCH_DEPTH");
                        continue;
                    }
                    
                    // LOW_SYM has affect on triclinic;
                    if (val != 729 && !inputParams.isTriclinicSearchDepthProvided()) {
                        inputParams.setLowSymmetrySearchDepth(val);
                        inputParams.setTriclinicSearchDepth(val);
                    }
                    
                    // LOW_SYM has affect on monoclinic;
                    if (val != 1728 && !inputParams.isMonoclinicSearchDepthProvided()) {
                        inputParams.setMonoclinicSearchDepth(val);
                    }
                    continue;
                }
                
                if (param.equals("TRICLINIC_SEARCH_DEPTH")) {
                    inputParams.promptTriclinicSearchDepth(true);
                    Integer val = TextFileParser.parseIntegerValue(value);
                    if (val == null) {
                        failToParse.add("TRICLINIC_SEARCH_DEPTH");
                        continue;
                    }
                    
                    // Override what LOW_SYM has set;
                    if (val != 729) {
                        inputParams.setTriclinicSearchDepth(val);
                    }
                    continue;
                }
                
                if (param.equals("MONOCLINIC_SEARCH_DEPTH")) {
                    inputParams.promptMonoclinicSearchDepth(true);
                    Integer val = TextFileParser.parseIntegerValue(value);
                    if (val == null) {
                        failToParse.add("MONOCLINIC_SEARCH_DEPTH");
                        continue;
                    }
                    
                    // Override what LOW_SYM has set;
                    if (val != 1728) {
                        inputParams.setMonoclinicSearchDepth(val);
                    }
                    continue;
                }
                
                if(param.contains("WRITE_LATTICE_VECTORS")) {
                    if (value.equalsIgnoreCase("TRUE")) {
                        inputParams.setKPointsAutoSetting(true);
                        continue;
                    } else if (value.equalsIgnoreCase("FALSE")) {
                        inputParams.setKPointsAutoSetting(false);
                        continue;
                    } else {
                        failToParse.add("WRITE_LATTICE_VECTORS");
                    } 
                }
                
                if (param.contains("BETA_MODE")) { 
                    // All lines written below BETA_MODE will be taken in 
                    // and not considered as an unknown
                    if (value.equalsIgnoreCase("TRUE")) {
                        inputParams.useBetaMode(true);
                        continue;
                    } else if (value.equalsIgnoreCase("FALSE")) {
                        inputParams.useBetaMode(false);
                        continue;
                    } else {
                        failToParse.add("BETA_MODE");
                    }
                }
                if (inputParams.isBetaMode()) {
                    if(param.contains("OPTI_DIAG")) {
                        if (value.equalsIgnoreCase("TRUE")) {
                            inputParams.useDiagonalToMinimize(true);
                            continue;
                        } else if (value.equalsIgnoreCase("FALSE")) {
                            inputParams.useDiagonalToMinimize(false);
                            continue;
                        } else {
                            failToParse.add("OPTI_DIAG");
                        } 
                    }
                    if (param.contains("PRINT_SUPERTODIRECT")) {
                        if (value.equalsIgnoreCase("TRUE")) {
                            inputParams.setKPointUseSuperToDirect(true);
                            continue;
                        } else if (value.equalsIgnoreCase("FALSE")) {
                            inputParams.setKPointUseSuperToDirect(false);
                            continue;
                        } else {
                            failToParse.add("PRINT_SUPERTODIRECT");
                        } 
                    }
                 }
                 else {
                     unknownParams.add(cleanLine);
                 }
            }
            LinkedHashSet<String> noDupFailToParse = new LinkedHashSet<>();
            LinkedHashSet<String> noDupUnknown = new LinkedHashSet<>();
            noDupFailToParse.addAll(failToParse);
            noDupUnknown.addAll(unknownParams);
            failToParse.clear();
            unknownParams.clear();
            failToParse.addAll(noDupFailToParse);
            unknownParams.addAll(noDupUnknown);
            this.inputParams.setUnparseableList(failToParse);
            this.inputParams.setUnrecognizeableList(unknownParams);
            reader.close();
            return true;
        } catch (FileNotFoundException e) {
            fileNotFoundError(this.logger, fi);
            e.printStackTrace(logger);
            return false;
        } catch (IOException e) {
            e.printStackTrace(logger);
            return false;
        } catch (NullPointerException e) {
            e.printStackTrace(logger);
            return false;
        }
    }
      
    public INCAR handleINCAR(File fi) {
        INCAR incar = null;
        try (BufferedReader read = new BufferedReader(new FileReader(fi))) {
            incar = new INCAR(read);
        } catch (FileNotFoundException e) {
            fileNotFoundError(logger, fi);
            e.printStackTrace(this.logger);
            return null;
        } catch (IOException e) {
            e.printStackTrace(this.logger);
            return null;
        }
        return incar;
    }
      
    public boolean handlePOSCAR(File fi) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fi))) {
            POSCAR inPoscar = new POSCAR(reader, true, false);
            inPoscar.labelUnknownSpecies();
            inputParams.setPOSCAR(inPoscar);
            return true;
        } catch (FileNotFoundException e) {
            fileNotFoundError(this.logger, fi);
            e.printStackTrace(this.logger);
            return false;
        } catch (IOException e) { 
            e.printStackTrace(this.logger);
            return false;
        } catch (RuntimeException e) { 
            //actually just NumberFormatException, but in case...
            e.printStackTrace(this.logger);
            return false;
        } 
    }
    
    public boolean handleQEInputFile(File fi) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fi))){
            QENamelists qeInput = new QENamelists(reader);
            inputParams.putQEData(qeInput);
            inputParams.setFileName(fi.getName());
            return true;
        } catch (FileNotFoundException e) {
            fileNotFoundError(this.logger, fi);
            return false;
        } catch (IOException e) {
            e.printStackTrace(this.logger);
            return false;
        } catch (RuntimeException e) {
            e.printStackTrace(this.logger);
            return false;
        }
    }
    
    /**
     * Interprets the request information in a ServerInput object and searches the optimum k-point
     * grid in the loaded collection(s). 
     * 
     * @param inputParams
     * @param includeGamma
     * @return KPointLattice the found optimum k-point lattice.
     */
    public KPointLattice generateKPointLattice(ServerInput inputParams, 
            KPointLibrary.INCLUDE_GAMMA includeGamma) {
        
        SpaceGroup space = inputParams.getAlteredSpaceGroup();
        // This was added to accommodate re-sizing in order to handle precision problems.
        double minDistance = inputParams.getUsedMinDistance(); 
        int minTotalPoints = inputParams.getMinTotalPoints();
        
        // decide which method user used to require the grid size.
        boolean useMinTotalPoints = inputParams.minTotalPointsUsed();
        boolean useDefaultMinDistance = inputParams.defaultMinDistance();
        String removeSymmetrySetting = inputParams.getGridRemoveSymmetrySettingUsed();
        
        KPointLibrary.SYMMETRY_TYPE sType = KPointLibrary.SYMMETRY_TYPE.ALL;
        if (removeSymmetrySetting.equalsIgnoreCase("structural")) {
            sType = KPointLibrary.SYMMETRY_TYPE.TIME_REVERSAL_ONLY;
        } else if (removeSymmetrySetting.equalsIgnoreCase("all")) {
            sType = KPointLibrary.SYMMETRY_TYPE.NONE;
        } else if (removeSymmetrySetting.equalsIgnoreCase("time_reversal")) {
            sType = KPointLibrary.SYMMETRY_TYPE.STRUCTURE_ONLY;
        }
        
        KPointLattice kLat = null; 
         
        if (useMinTotalPoints && useDefaultMinDistance) {          // Only minTotalPoints.
            minDistance = 0.0;
        } else if (!useMinTotalPoints && !useDefaultMinDistance) { // Only minDistance.
            minTotalPoints = 1;
        } else if (!useMinTotalPoints && useDefaultMinDistance){   // Default minDistance.
            minTotalPoints = 1;
        }
        kLat = m_Library.getKPointLattice(space, includeGamma, minDistance, minTotalPoints, sType);
        
        if (kLat == null) { return null; } // Exceed the current grid-size limit.
        return kLat;
    }

    /**
     * Find the optimum k-point lattice and wrap it in a KPOINTS object for output.
     * 
     * @param inputParams
     * @return KPOINTS the IO object representing VASP KPOINTS file.
     * @throws SymmetryException
     */
    public KPOINTS generateKPOINTFile(ServerInput inputParams) throws SymmetryException {     
        
        Structure struct = inputParams.getAlteredStructure();
        String includeGammaStr = inputParams.includeGamma();
        KPointLibrary.INCLUDE_GAMMA includeGamma = null;
        if(includeGammaStr.equals("FALSE")) {
             includeGamma = KPointLibrary.INCLUDE_GAMMA.FALSE;
        } else if (includeGammaStr.equals("TRUE")) {
             includeGamma = KPointLibrary.INCLUDE_GAMMA.TRUE;
        } else {
             includeGamma = KPointLibrary.INCLUDE_GAMMA.AUTO;
        }
        
        KPOINTS kPoints = null;

        // The actual searching function.
        KPointLattice kLat = generateKPointLattice(inputParams, includeGamma);
        
        //check for null first. If it is null then the grid is larger than what we can process
        if (kLat == null) {
            return null;
        }
        
        double pointMapSuccessRatio = 1 - kLat.getMapFailureRatio();
        int numEffectiveKPoints = (int) Math.round(kLat.numTotalKPoints() / pointMapSuccessRatio);
        int arithNum = inputParams.getAlteredSpaceGroup()
                                  .getSymmorphicGroup()
                                  .getArithmeticCrystalClassNumber(
                                      inputParams.getAlteredStructure().numPeriodicDimensions());
        //TODO: review this comment
        // For shifted grids of hexagonal and trigonal lattices, only 1/2 or 1/3 shifts are
        // allowed. Otherwise, the primary 3-fold or 6-fold symmetries will be broken.
        if (!kLat.includesGammaPoint() && arithNum > 37 && arithNum < 59) {
            if (numEffectiveKPoints != kLat.numTotalKPoints() 
             && numEffectiveKPoints != kLat.numTotalKPoints() * 2 
             && numEffectiveKPoints != kLat.numTotalKPoints() * 3) { 
                //TODO: review this comment
                //TODO: Check if this addition is true
                throw new SymmetryException("The distinct points break k-point lattice symmetry.");
            } 
        } else {
            if (numEffectiveKPoints != kLat.numTotalKPoints()) {
                throw new SymmetryException("The distinct points break k-point lattice symmetry.");
            }
        }
        
        long genStart = System.currentTimeMillis();
        kPoints = new KPOINTS(kLat, struct.getDefiningLattice()
                                          .getInverseLattice()
                                          .getLatticeBasis());
        long genFin = System.currentTimeMillis();
        Status.detail("Generating KPOINTS object took: " + (genFin - genStart) + " ms.");
        
        // Split header messages to screen and KPOINTS or put them all in KPOINTS.
        // Stand-alone application default to split header.
        String comment = inputParams.isOutputSeparated() 
                ? setUpSplitHeader(kLat, struct, inputParams, includeGamma) 
                : setUpKPOINTSHeader(kLat, struct, inputParams, includeGamma);
        
        if (inputParams.isOutputSeparated()) {
            String[] spl = comment.split("\\++");

            // Output to screen:
            String commentToScreen = spl[0].trim();
            if ( commentToScreen.length() > 3 ) { // Avoid blank lines in output.
                this.reportBasic(commentToScreen);
            }
            
            // Output to header comment of KPOITNS file:
            String headerComment = spl[2].trim().replace("\r\n", "");    // For Windows
            headerComment = headerComment.replace("\n", ""); // For Linux
            headerComment = headerComment.replace("\r", ""); // For old Mac
            kPoints.setDescription(headerComment);
        } else {
            this.reportBasic(comment);
            kPoints.setDescription(comment);
        }
        
        return kPoints;
    }

    /**
     * TODO: review this documentation. (Should we delete this method? Seems like an obsolete 
     * version of generateKPOINTFile().)
     * BETA_MODE functionality: minimize distinct k-point numbers with diagonal optimizer(?).
     * 
     * @param inputParams
     * @param struct The structure modified by the function adjustVASPStructure.
     * @return KPOINTS An IO object representing VASP KPOINTS file.
     * @throws SymmetryException
     */
    public synchronized KPOINTS generateKPOINTFileWDiagOpti(ServerInput inputParams, 
            Structure struct) throws SymmetryException {
        
        SpaceGroup space = struct.getDefiningSpaceGroup().getSymmorphicGroup();
        
        String includeGammaStr = inputParams.includeGamma();
        KPointLibrary.INCLUDE_GAMMA includeGamma = null;
        if(includeGammaStr.equals("FALSE")) {
            includeGamma = KPointLibrary.INCLUDE_GAMMA.FALSE;
        } else if (includeGammaStr.equals("TRUE")) {
            includeGamma = KPointLibrary.INCLUDE_GAMMA.TRUE;
        } else {
            includeGamma = KPointLibrary.INCLUDE_GAMMA.AUTO;
        }
        
        boolean forceDiagonal = inputParams.isDiagonalGrid();
        double minDistance = inputParams.getMinDistance();
        int minTotalPoints = inputParams.getMinTotalPoints();
        
        String ignoreSymmetrySetting = inputParams.getGridRemoveSymmetrySettingUsed();
        KPointLibrary.SYMMETRY_TYPE sType = KPointLibrary.SYMMETRY_TYPE.ALL;  
        if (ignoreSymmetrySetting.equalsIgnoreCase("structural")) {
            sType = KPointLibrary.SYMMETRY_TYPE.TIME_REVERSAL_ONLY;
        } else if (ignoreSymmetrySetting.equalsIgnoreCase("all")) {
            sType = KPointLibrary.SYMMETRY_TYPE.NONE;
        }
        
        boolean useMinTotalPoints = inputParams.minTotalPointsUsed();
        boolean useDefaultMinDistance = inputParams.defaultMinDistance();
        
        int numDim = struct.getDefiningLattice().numPeriodicVectors();
        int arithNum = space.getArithmeticCrystalClassNumber(numDim);
        Status.detail("The arithmetic number for " + struct.getDescription() + " is " + arithNum);
      
        KPointLattice kLat = null;
        boolean secLatUsed = false;
        
        long st = System.currentTimeMillis();
        long diagTime = 0;
        if(useMinTotalPoints && !useDefaultMinDistance) {
            // Both minDistance and minTotalPoints are specified
            if (forceDiagonal) {
                //kLat = diagonalLibrary.getKPointLattice(struct, includeGamma, 
                //         minDistance, minTotalPoints, sType);
            } else {
                kLat = m_Library.getKPointLattice(struct, 
                                                  includeGamma,
                                                  minDistance, 
                                                  minTotalPoints, 
                                                  sType);
            }
        } else if(useMinTotalPoints && useDefaultMinDistance) { // Only minTotalPoints.
            if (forceDiagonal) {
                minDistance = 0.0;
                //kLat = diagonalLibrary.getKPointLattice(struct, includeGamma, 
                //        minDistance, minTotalPoints, sType);
            } else {
                minDistance = 0.0;
                kLat = m_Library.getKPointLattice(struct, 
                                                  includeGamma, 
                                                  minDistance, 
                                                  minTotalPoints, 
                                                  sType);
            }
        } else if(!useMinTotalPoints && !useDefaultMinDistance) { // Only minDistance.
            if (forceDiagonal) {
                minTotalPoints = 1;
                //kLat = diagonalLibrary.getKPointLattice(struct, includeGamma, 
                //        minDistance, minTotalPoints, sType);
            } else {
                minTotalPoints = 1;
                kLat = m_Library.getKPointLattice(struct, 
                                                  includeGamma, 
                                                  minDistance, 
                                                  minTotalPoints, 
                                                  sType);
            }
        } else { // Use default minDistance (28.1 Angstrom).
            if (forceDiagonal) {
                minTotalPoints = 1;
                //kLat = diagonalLibrary.getKPointLattice(struct, includeGamma, 
                //        minDistance, minTotalPoints, sType);
            } else {
                minTotalPoints = 1;
                kLat = m_Library.getKPointLattice(struct, 
                                                  includeGamma, 
                                                  minDistance, 
                                                  minTotalPoints, 
                                                  sType);
            }
        }
      
        long latTime = System.currentTimeMillis() - st;
        //this is minPeriodicDistance from real lattice (reciprocal of reciprocal)
        double storeMinPerD = kLat.getRealSuperLattice().getMinPeriodicDistance();

        long genStart = System.currentTimeMillis();
        KPOINTS kPoints = new KPOINTS(kLat, struct.getDefiningLattice()
                                                  .getInverseLattice()
                                                  .getLatticeBasis());
        long genFin = System.currentTimeMillis();
        String composition = struct.getCompositionString();
        Status.detail("Generating KPOINTS object took: " + (genFin - genStart) + " ms.");
        boolean includesGammaPoint = kLat.includesGammaPoint();
        String gammaComment = "Grid does not include gamma point. ";
        if (includesGammaPoint){
            gammaComment = "Grid includes gamma point. ";
        }
        String forceD = "";
        if (forceDiagonal) {
            forceD = " FORCEDIAGONAL=" + forceDiagonal;
        }
        String gapLine = "";
        if (inputParams.gapAdjusted()){
            gapLine = " GAPDISTANCE=" + inputParams.gapDistance();
        }
        String sl = "";
        if (secLatUsed) {
            sl = " Diagonal lattice produced less distinct points and was used. "
               + "Diagonal lattice production time: " 
               + diagTime + " ms. Total lattice production time: " + latTime + " ms.";
        }
        String fails = "Unable to properly parse and default values are used for: ";
        if (inputParams.unparseableList().size() == 0) {
            fails = "";
        } else {
            for (int i = 0; i < inputParams.unparseableList().size() - 1; i++) {
                fails = fails + inputParams.unparseableList().get(i) + " ";
            }
            fails = formatWarningComment(fails + inputParams.unparseableList()
            		.get(inputParams.unparseableList().size()-1) + ".");
        }
        String ver = "Generator Version " + m_Version + ". ";
        String unknowns = "Cannot recognize these input parameters: ";
        if (inputParams.unrecognizeableList().size() == 0) {
            unknowns = "";
        } else {
            for (int i = 0; i < inputParams.unrecognizeableList().size()-1; i++) {
                unknowns = unknowns + inputParams.unrecognizeableList().get(i) + " ";
            }
            unknowns = formatWarningComment(unknowns + inputParams.unrecognizeableList()
            		.get(inputParams.unrecognizeableList().size()-1) + ".");
        }
        String selectiveDynamics = "";
        if (inputParams.usingSelectiveDynamics()) {
            selectiveDynamics = "Selective dynamics was detected and taken into account. ";
        }
        String gapWarning = "";
        if (inputParams.gapDistance() == 0) {
            gapWarning = formatWarningComment("GAPDISTANCE=0 will treat all atoms as isolated particles.");
        }
      
        if (!inputParams.isDefault()) {
            if (inputParams.headerVerbosity().equals("VERBOSE")) {
                String comment = ver + selectiveDynamics + fails + unknowns + gapWarning 
                               + "K-point grid has " + kLat.numTotalKPoints() 
                               + " total points. Actual minimum periodic distance is " 
                               + storeMinPerD + " Angstroms. "
                               + gammaComment + "Parameters used: MINDISTANCE=" + minDistance 
                               + " INCLUDEGAMMA=" + includeGamma + forceD 
                               + " MINTOTALKPOINTS=" + inputParams.getMinTotalPoints() 
                               + " KPPRA=" + inputParams.getKPPRA() + gapLine + sl;
                kPoints.setDescription(comment);
            } else {
                String comment = ver + selectiveDynamics + fails + unknowns + gapWarning 
                               + "K-point grid has " + kLat.numTotalKPoints() 
                               + " total points. Actual minimum periodic distance is " 
                               + storeMinPerD + " Angstroms. "  + gammaComment;
                kPoints.setDescription(comment);
            }
            
            if (inputParams.headerVerbosity().equals("DEBUG") && inputParams.isBetaMode()) {
                String comment = ver + selectiveDynamics + fails + unknowns + gapWarning 
                               + "K-point grid has " + kLat.numTotalKPoints() 
                               + " total points. Actual minimum periodic distance is " 
                               + storeMinPerD + " Angstroms. "
                               + gammaComment + "Parameters used: MINDISTANCE=" + minDistance 
                               + " INCLUDEGAMMA=" + includeGamma + forceD
                               + " MINTOTALKPOINTS=" + inputParams.getMinTotalPoints() 
                               + " KPPRA=" + inputParams.getKPPRA() + gapLine + sl;
                kPoints.setDescription(comment);
            }
            if (inputParams.isBetaMode()) {
                String comment = ver + selectiveDynamics + fails + unknowns + gapWarning 
                               + "||BETA_MODE|| K-points for " + composition 
                               + " with MINDISTANCE=" + minDistance 
                               + " Angstroms and INCLUDEGAMMA=" + includeGamma 
                               + " FORCEDIAGONAL=" + forceDiagonal 
                               + " has " + kLat.numTotalKPoints() + " total k-points. "
                               + "Effective minimum distance: " + storeMinPerD + " Angstroms.";
                kPoints.setDescription(comment);
            }
        } else {
            String comment = ver + selectiveDynamics + fails + unknowns + gapWarning 
                           + "K-point grid was generated using default values. "
                           + "Grid has " + kLat.numTotalKPoints() + " total points. "
                           + "Actual minimum periodic distance is " + storeMinPerD + " Angstroms. "  
                           + gammaComment;
            kPoints.setDescription(comment);
        }
        return kPoints;
    }  
    
    /**
     * BETA_MODE functionality: generate the K_POINTS name-list for a QE input file.
     * 
     * @param inputParams
     * @param struct The structure modified by the function adjustQEStructure.
     * @return QENamelists An IO object for QE input file (like KPOINTS for VASP).
     */
    public QENamelists appendQEWithKPoints (ServerInput inputParams, Structure struct) {
        String includeGammaStr = inputParams.includeGamma();
        KPointLibrary.INCLUDE_GAMMA includeGamma = null;
        if(includeGammaStr.equals("FALSE")) {
            includeGamma = KPointLibrary.INCLUDE_GAMMA.FALSE;
        } else if (includeGammaStr.equals("TRUE")) {
            includeGamma = KPointLibrary.INCLUDE_GAMMA.TRUE;
        } else {
            includeGamma = KPointLibrary.INCLUDE_GAMMA.AUTO;
        }
      
        QENamelists qeif = inputParams.getQEData();

        KPointLattice kLat = generateKPointLattice(inputParams,includeGamma);
        if (kLat == null) {
            return null;
        }
        
        long genStart = System.currentTimeMillis();
        
        //Split header messages to screen and KPOINTS or put them all in KPOINTS.
        String comment = inputParams.isOutputSeparated() 
                ? setUpSplitHeader(kLat, struct, inputParams, includeGamma) 
                : setUpKPOINTSHeader(kLat, struct, inputParams, includeGamma);
        this.reportBasic(comment);
        String header = "";
        if (inputParams.isOutputSeparated()) {
            String[] spl = comment.split("\\++");
            header = spl[0] + System.lineSeparator() + "+++" + spl[1] + "+++";
            String realComment = spl[2].replace("\r\n", ""); //For Windows
            realComment = realComment.replace("\n", ""); //For Linux
            realComment = realComment.replace("\r", ""); //For old Macs
            qeif.updateWithKPoints(kLat, struct.getDefiningLattice().getInverseLattice().getLatticeBasis(), header, realComment);
            long genFin = System.currentTimeMillis();
            Status.detail("Generating KPOINTS object took: " + (genFin - genStart) + " ms.");
            return qeif;
        }
        
        qeif.updateWithKPoints(kLat, struct.getDefiningLattice().getInverseLattice().getLatticeBasis(), header, comment);
        inputParams.putQEData(qeif);
        long genFin = System.currentTimeMillis();
        Status.detail("Generating KPOINTS object took: " + (genFin - genStart) + " ms.");       
        return qeif;
    }      
      
    public String setUpKPOINTSHeader(KPointLattice kLat, Structure struct, ServerInput inputParams, KPointLibrary.INCLUDE_GAMMA includeGamma) {
        
        boolean includesGammaPoint = kLat.includesGammaPoint();
        boolean forceDiagonal = inputParams.isDiagonalGrid();
        double minDistance = inputParams.getMinDistance();
        
        Structure origStruct = inputParams.getStructure()[0];
        Structure alteredStruct = inputParams.getAlteredStructure();
        int[][] superToDirect = kLat.getSuperToDirect(alteredStruct.getDefiningLattice());
        SuperLattice alteredSuperLattice = new SuperLattice(alteredStruct.getDefiningLattice(), superToDirect);
        SuperLattice origSuperLattice = new SuperLattice(origStruct.getDefiningLattice(), superToDirect);
        BravaisLattice origKPointLattice = origSuperLattice.getInverseLattice();
        double storeMinPerD = alteredSuperLattice.getMinPeriodicDistance() / inputParams.getScalingTimes();
        double compactRVec = alteredSuperLattice.getLongestCompactVectorLength();
        //double storeKSpa = origKPointLattice.getMinPeriodicDistance();
        double compactKVec = origKPointLattice.getLongestCompactVectorLength();
        
        //============================ MESSAGES ON POSCAR and PRECALC ============================//
        String composition = struct.getCompositionString();
        //boolean ignoreSymmetry = inputParams.symmetrySetting();
        //int symSetting = inputParams.symmetrySettingVASP();
        String reducedDim = formatBasicComment("Actual minimum periodic distance is " + storeMinPerD + " Angstroms.");
        if (inputParams.getRealDimension() == 2) {
            reducedDim = formatBasicComment("Slab structure detected. " + reducedDim);
        } else if (inputParams.getRealDimension() == 1) {
            reducedDim = formatBasicComment("Wire structure detected. " + reducedDim);
        } else if (inputParams.getRealDimension() == 0) {
            reducedDim = formatBasicComment("Isolated particle detected.");
        }

        String gammaComment = formatBasicComment("Grid does not include gamma point.");
        if (includesGammaPoint){
            gammaComment = formatBasicComment("Grid includes gamma point.");
        }
        String forceD = "";
        String forceDiagonalWarning = "";
        if (forceDiagonal) {
            forceD = " FORCEDIAGONAL=" + forceDiagonal;
            forceDiagonalWarning = formatWarningComment("FORCEDIAGONAL is found to not generate fully diagonal grids and has been deprecated. Generating generalized grid...");
        }
        String gapLine = "";
        if (inputParams.gapAdjusted()){
            gapLine = " GAPDISTANCE=" + inputParams.gapDistance();// + " REDUCEDMINTOTALKPOINTS=" + inputParams.getMinTotalPoints();//inputParams.getGapReducedMinTotalPoints();
        }
        
        String searchDepth = "";
        String searchDepthWarning = ""; // deprecate low_symmetry_search_depth;
        String remove_symmetry = inputParams.getGridRemoveSymmetrySettingUsed();
        int numDim = inputParams.getAlteredStructure().numPeriodicDimensions();
        // A safe way to avoid precision errors.
        SpaceGroup symmorphicGroup = inputParams.getAlteredSpaceGroup().getSymmorphicGroup();
        int arithNum = symmorphicGroup.getArithmeticCrystalClassNumber(numDim);
        if (arithNum <= 2 
                || remove_symmetry.equalsIgnoreCase("all")
                || remove_symmetry.equalsIgnoreCase("structural")) {
            if (inputParams.getTriclinicSearchDepth() != 729) {   
                searchDepth = formatBasicComment("Search depth for triclinic structures "
                        + "is changed by user to " + inputParams.getTriclinicSearchDepth() 
                        + ".");
            }
        } else if (arithNum >= 3 && arithNum <= 8) {
            if (inputParams.getMonoclinicSearchDepth() != 1728) {
                searchDepth = formatBasicComment("Search depth for monoclinic structures "
                        + "is changed by user to " + inputParams.getMonoclinicSearchDepth() 
                        + ".");
            }
        }
        
        if (inputParams.isLowSymmetrySearchDepthProvided()) {
            searchDepthWarning += formatWarningComment("LOW_SYMMETRY_SEARCH_DEPTH is deprecated and "
            		+ "will be removed in future releases. Please use MONOCLINIC_SEARCH_DEPTH "
                    + "or TRICLINIC_SEARCH_DEPTH.");
        }
        
        String writeVectorWarning = "";
        String generatingVectorSymWarning = ""; // Generating vectors might not be commensurate with
                                                // crystal reciprocal lattice vectors if symmetries
                                                // were lowered by any reason. (MAGMOM, ISYM, REMOVE_SYMMETRY)
        if (inputParams.kPointsAutoSetting()) {
        	writeVectorWarning = formatWarningComment("WRITE_LATTICE_VECTORS=TRUE might result in "
        			+ "errors if used with VASP versions before 6. For earlier VASP versions, we "
        			+ "recommend setting WRITE_LATTICE_VECTORS to FALSE.");
        	if (inputParams.getGridRemoveSymmetrySettingUsed().equals("structural") ||
        		inputParams.getGridRemoveSymmetrySettingUsed().equals("all")) {
        		// Tim said don't treat as a warning.
        		generatingVectorSymWarning = formatBasicComment("Using WRITE_LATTICE_VECTORS=TRUE "
        				+ "with symmetry disabled might trigger a warning from VASP, but the "
        				+ "results are often still meaningful.");
        	}
        } else {
        	// Technically, it's not a warning.
        	writeVectorWarning = formatBasicComment("WRITE_LATTICE_VECTORS=FALSE detected. If you "
        			+ "are using VASP.6, we recommend setting WRITE_LATTICE_VECTORS=TRUE.");
        }
        
        String fails = "Unable to properly parse and default values are used for: ";
        if (inputParams.unparseableList().size() == 0) {
            fails = "";
        } else {
            for (int i = 0; i < inputParams.unparseableList().size()-1; i++) {
                fails = fails + inputParams.unparseableList().get(i) + " ";
            }
            fails = formatBasicComment(fails + inputParams.unparseableList().get(inputParams.unparseableList().size()-1) + ".");
        }
        String unknowns = "Cannot recognize these input parameters: ";
        if (inputParams.unrecognizeableList().size() == 0) {
            unknowns = "";
        } else {
            for (int i = 0; i < inputParams.unrecognizeableList().size()-1; i++) {
                unknowns = unknowns + inputParams.unrecognizeableList().get(i) + " ";
            }
            unknowns = formatBasicComment(unknowns + inputParams.unrecognizeableList().get(inputParams.unrecognizeableList().size()-1) + ".");
        }
        String symFlag = inputParams.getGridRemoveSymmetrySetting();
        String sym = "";
        if (symFlag.equalsIgnoreCase("all")) {
            sym = formatBasicComment("Generating grid without structural and time-reversal symmetry.");
        } else if (symFlag.equalsIgnoreCase("structural")){
            sym = formatBasicComment("Generating grid without structural symmetry."); 
        } else if (symFlag.equalsIgnoreCase("time_reversal")) {
            sym = formatBasicComment("Generating grid without time-reversal symmetry.");
        }
        /*String lsOrbitWarning = "";
        if (inputParams.useSpinOrbitCoupling() && symSetting != -1) {
            lsOrbitWarning = lineMessageOutputFormat("Warning: When LSORBIT=TRUE, VASP recommends using ISYM=-1.");
        }*/
        String ver = formatBasicComment("Generator version " + m_Version + ".");
        String selectiveDynamics = "";
        if (inputParams.usingSelectiveDynamics()) {
            selectiveDynamics = formatBasicComment("Selective dynamics was detected and taken into account.");
        }
        String gapWarning = "";
        if (inputParams.gapDistance() == 0) {
            gapWarning = formatWarningComment("GAPDISTANCE=0 will treat all atoms as isolated particles.");
        }
        String sizeWarning = "";
        if (!inputParams.isMinDistProvided() && !inputParams.isMinTotProvided() && !inputParams.isKPPRAProvided()) {
            sizeWarning = formatWarningComment("Grid size was not prompted, using default grid size.");
        }
        String oldVersion = "";
        if (isVersionOlder("C2020.11.15", inputParams.getClientVersion())) {
            oldVersion = formatWarningComment("If you are using getKPoints script that is older than C2020.11.15, please consider downloading the latest script from https://gitlab.com/muellergroup/k-pointGridGenerator.");
        }
        
        //============================== MESSAGES ON INCAR SETTINGS ==============================//
        INCAR incar = inputParams.getINCAR();
        String magmom = "";
        String nonCollinear = "";
        String nonCollDet = "";
        String nonCollISYM0Warning = "";
        String charg = "";
        String IBRIONWarning = "";
        if (incar != null) {
            if (incar.ISPIN() == 2 && incar.MAGMOMString() != null && !incar.LNONCOLLINEAR()) {
                magmom = formatBasicComment("ISPIN=2 and MAGMOM detected in INCAR. Reducing grid symmetry accordingly.");
            }
            /*if (incar.LSORBIT()) {
                if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()) {
                    if (incar.ISYM() == 0) {
                        nonCollinear = lineMessageOutputFormat("Warning: Using a grid with time-reversal symmetry for a spin orbit calculation is not recommended.");
                    } else if (incar.ISYM() > 0 ) {
                        nonCollinear = lineMessageOutputFormat("Warning: Using a high symmetry grid for a spin orbit calculation is not recommended. We currently do not generate high symmetry grids for spin orbit calculation. Using only time-reversal symmetry to generate grid.");
                    }
                } else {
                    nonCollinear = lineMessageOutputFormat("Spin orbit calculation detected.");
                }
            }*/
            if (incar.LNONCOLLINEAR() /*&& !incar.LSORBIT()*/) {
                nonCollDet = formatBasicComment("Non-collinear magnetic calculation detected.");
                if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()) {
                    if (incar.ISYM() > 0) {
                        nonCollISYM0Warning = formatWarningComment("Using a grid with time-reversal symmetry for a noncollinear magnetic calculation is not recommended. Consider setting ISYM=-1 in your INCAR file.");
                        nonCollinear = formatWarningComment("Structural symmetry is currently ignored for non-collinear magnetic calculations.");
                    } else if (incar.ISYM() == 0) {
                        nonCollinear = formatWarningComment("Using a grid with time-reversal symmetry for a noncollinear magnetic calculation is not recommended.");
                    }
                } else if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("time_reversal")){
                    if (incar.ISYM() > -1) {
                        nonCollinear = formatWarningComment("User removed time-reversal symmetry. Since structural symmetry is currently ignored for non-collinear magnetic calculations, grid will be generated without any symmetry.");
                        sym = formatBasicComment("Generating grid without structural and time-reversal symmetry.");
                    }
                } else {
                    //nonCollinear = standardUserOutputFormat("Non-collinear calculation detected.");
                }
            }
            
            // Phonon / Elastic tensor: Force symmetries to be switched off only when using list format.
            if (!inputParams.kPointsAutoSetting() && incar.ISYM() >= 1) {
              if (incar.IBRION() >= 5 && incar.IBRION() <= 8 ) {
            	if (!inputParams.gridRemoveSymmetrySettingProvided()) {
            		IBRIONWarning = formatWarningComment("IBRION value might break symmetry. Grid was generated ignoring structural symmetry by default.");
                } else if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("TIME_REVERSAL")) {
                	IBRIONWarning = formatWarningComment("IBRION value might break symmetry. Grid was generated ignoring structural symmetry in addition to time-reversal symmetry.");
                	sym = "";
                } else if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("NONE")) {
                	IBRIONWarning = formatWarningComment("IBRION value might break symmetry. REMOVE_SYMMETRY is overrode and grid was generated ignoring structural symmetry.");
                }
              }
            }
            
            if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()){
                if (incar.ISYM() > 0) {
                    if (incar.ISPIN() == 2 || incar.LNONCOLLINEAR()) {
                        if (incar.ICHARG() == 1 || incar.ICHARG() == 11) {
                            charg = formatWarningComment("ICHARG="+ incar.ICHARG() +" and ISPIN=2. Magnetic moments in CHGCAR file can break symmetry and this is not taken into account during grid generation. Please consider setting REMOVE_SYMMETRY=ALL or REMOVE_SYMMETRY=STRUCTURAL in your PRECALC file.");
                        }
                    }
                }
            }
        }
        String precisionAdjustNotice = "";
        if (inputParams.isPrecisionAdjusted()) {
            precisionAdjustNotice = formatBasicComment("Borderline symmetry detected. Precision in determining symmetry is automatically increased.");
        }
        
        String betaMode = "";
        if (inputParams.isBetaMode()) {
            betaMode = formatWarningComment("BETA_MODE initialized. Beta version functionalities which may not have been thoroughly tested are used. Please contact us at kpoints@jhu.edu if you found any bugs."); 
        }
        /*String spinOrbit = "";
        if (inputParams.useSpinOrbitCoupling() && inputParams.useSpinOrbitCouplingTriggered()) {
            spinOrbit = standardUserOutputFormat("Spin orbit coupling detected. Generating grid without symmetry.");
        }
        String nonCollWarning = "";
        if (inputParams.useNonCollinear() && inputParams.useNonCollinearTriggered() && !inputParams.useSpinOrbitCoupling() && inputParams.LNoncollinearWithoutReducedSymmetry()) {
            nonCollWarning = standardUserOutputFormat("Warning: Non-collinear calculation with symmetry detected in INCAR. We currently do not return these grids and they are not recommended by VASP. Using only time-reversal symmetry in generating grid.");
        }
        String spinOrbitWarning = "";
        if (inputParams.useSpinOrbitCoupling() && inputParams.useSpinOrbitCouplingTriggered() && inputParams.LSorbitWithoutSymmetryOff()) {
            spinOrbitWarning = standardUserOutputFormat("Warning: Spin orbit coupling with symmetry detected in INCAR. We currently do not return these grids and they are not recommended by VASP. No symmetry is used in generating grid.");
        }*/
        //String warningOn4_25=standardUserOutputFormat("There was a bug in version 2016.04.25 that can lead to incorrect k-point weights. Please replace grids that were made with version 2016.04.25. We apologize for the inconvenience.");
        if (!inputParams.isDefault()) {
            if (inputParams.headerVerbosity().equals("VERBOSE")) {
                String comment = ver 
                        //+ warningOn4_25
                        + oldVersion
                        + forceDiagonalWarning
                        //+ spinOrbit
                        //+ spinOrbitWarning
                        + nonCollDet
                        + nonCollinear
                        + nonCollISYM0Warning
                        //+ nonCollWarning
                        + magmom
                        + IBRIONWarning
                        + charg
                        + selectiveDynamics 
                        + precisionAdjustNotice
                        + sym 
                        + searchDepth
                        + searchDepthWarning
                        + writeVectorWarning
                        + generatingVectorSymWarning
                        //+ lsOrbitWarning
                        + fails 
                        + unknowns 
                        + sizeWarning 
                        + gapWarning 
                        + "K-point grid has " + kLat.numTotalKPoints() + " total points and " + kLat.numDistinctKPoints() + " distinct points. " 
                        + reducedDim
                        + gammaComment 
                        + "Parameters used: MINDISTANCE=" + minDistance + " INCLUDEGAMMA=" + includeGamma 
                        + forceD
                        + " MINTOTALKPOINTS=" + inputParams.getMinTotalPoints() 
                        + " KPPRA=" + inputParams.getKPPRA() 
                        + gapLine 
                        + " TRICLINIC_SEARCH_DEPTH=" + inputParams.getTriclinicSearchDepth()
                        + " MONOCLINIC_SEARCH_DEPTH=" + inputParams.getMonoclinicSearchDepth()
                        + ".";
                return comment;
            } else if (inputParams.headerVerbosity().equals("DEBUG") && inputParams.isBetaMode()) {
                String comment = ver 
                        //+ warningOn4_25 
                        + oldVersion
                        + forceDiagonalWarning
                        //+ spinOrbit
                        //+ spinOrbitWarning
                        + nonCollDet
                        + nonCollinear
                        + nonCollISYM0Warning
                        //+ nonCollWarning
                        + magmom
                        + IBRIONWarning
                        + charg
                        + selectiveDynamics 
                        + precisionAdjustNotice
                        + sym 
                        + searchDepth
                        + searchDepthWarning
                        + writeVectorWarning
                        + generatingVectorSymWarning
                        //+ lsOrbitWarning
                        + fails 
                        + unknowns 
                        + sizeWarning 
                        + gapWarning 
                        + "||BETA_MODE|| K-points for " + composition + " with MINDISTANCE=" + minDistance + " "
                        + ", and INCLUDEGAMMA=" + includeGamma
                        + " FORCEDIAGONAL=" + forceDiagonal 
                        + " has " + kLat.numTotalKPoints() + " total points and " 
                        + kLat.numDistinctKPoints() + " distinct points. " 
                        + reducedDim + ", has length of compact k-vector: " + compactKVec + " 1/Angstroms, "
                        + "and length of compact r-vector: " + compactRVec + " Angstroms.";
                return comment;
            } else if (inputParams.isBetaMode()) {
                String comment = ver 
                        //+ warningOn4_25
                        + oldVersion
                        + forceDiagonalWarning
                        //+ spinOrbit
                        //+ spinOrbitWarning
                        + nonCollDet
                        + nonCollinear
                        + nonCollISYM0Warning
                        //+ nonCollWarning
                        + magmom
                        + IBRIONWarning
                        + charg
                        + selectiveDynamics 
                        + precisionAdjustNotice
                        + sym 
                        + searchDepth
                        + searchDepthWarning
                        + writeVectorWarning
                        + generatingVectorSymWarning
                        //+ lsOrbitWarning
                        + fails 
                        + unknowns 
                        + sizeWarning 
                        + gapWarning
                        + betaMode
                        + "||BETA_MODE|| K-points for " + composition + " with MINDISTANCE=" + minDistance + " Angstroms and INCLUDEGAMMA=" + includeGamma + " FORCEDIAGONAL=" + forceDiagonal + " has " + kLat.numTotalKPoints() + " total points and " + kLat.numDistinctKPoints() + " distinct points. " 
                        + reducedDim;
                return comment;
            } else { // HEADER = SIMPLE
                String comment = ver 
                        //+ warningOn4_25
                        + oldVersion
                        + forceDiagonalWarning
                        //+ spinOrbit
                        //+ spinOrbitWarning
                        + nonCollDet
                        + nonCollinear
                        + nonCollISYM0Warning
                        //+ nonCollWarning
                        + magmom
                        + IBRIONWarning
                        + charg
                        + selectiveDynamics
                        + precisionAdjustNotice
                        + sym 
                        + searchDepth
                        + searchDepthWarning
                        + writeVectorWarning
                        + generatingVectorSymWarning
                        //+ lsOrbitWarning
                        + fails 
                        + unknowns 
                        + sizeWarning 
                        + gapWarning 
                        + "K-point grid has " + kLat.numTotalKPoints() +
                        " total points and " + kLat.numDistinctKPoints() + " distinct points. " 
                        + reducedDim 
                        + gammaComment;
                return comment;
            }
        } else { // No PRECALC provided. Use default settings.
            String comment = ver 
                    //+ warningOn4_25
                    + oldVersion
                    + forceDiagonalWarning
                    //+ spinOrbit
                    //+ spinOrbitWarning
                    + nonCollDet
                    + nonCollinear
                    + nonCollISYM0Warning
                    //+ nonCollWarning
                    + magmom
                    + IBRIONWarning
                    + charg
                    + selectiveDynamics 
                    + precisionAdjustNotice
                    + sym  
                    + searchDepth
                    + searchDepthWarning
                    + writeVectorWarning
                    + generatingVectorSymWarning
                    //+ lsOrbitWarning
                    + fails 
                    + unknowns 
                    + sizeWarning 
                    + gapWarning 
                    + "K-point grid was generated using default values. Grid has " + kLat.numTotalKPoints() +
                    " total points and " + kLat.numDistinctKPoints() + " distinct points. " 
                    + reducedDim 
                    + gammaComment;
            return comment;
        }
    }
    
    public String setUpSplitHeader(KPointLattice kLat, Structure struct, ServerInput inputParams, KPointLibrary.INCLUDE_GAMMA includeGamma) {
        //double storeMinPerD = inputParams.getDatabaseFoundMinDistance();//kLat.getRealSuperLattice().getMinPeriodicDistance(); //this is minPeriodicDistance from real lattice (reciprocal of reciprocal)
        //double storeKSpa = kLat.getKPointLattice().getMinPeriodicDistance();
        //SuperLattice sla = kLat.getReciprocalSpaceLattice();
        
        boolean includesGammaPoint = kLat.includesGammaPoint();
        boolean forceDiagonal = inputParams.isDiagonalGrid();
        double minDistance = inputParams.getMinDistance();
        
        Structure origStruct = inputParams.getStructure()[0];
        Structure alteredStruct = inputParams.getAlteredStructure();
        int[][] superToDirect = kLat.getSuperToDirect(alteredStruct.getDefiningLattice());
        SuperLattice alteredSuperLattice = new SuperLattice(alteredStruct.getDefiningLattice(), superToDirect);
        SuperLattice origSuperLattice = new SuperLattice(origStruct.getDefiningLattice(), superToDirect);
        BravaisLattice origKPointLattice = origSuperLattice.getInverseLattice();
        double storeMinPerD = alteredSuperLattice.getMinPeriodicDistance() / inputParams.getScalingTimes();
        double compactRVec = alteredSuperLattice.getLongestCompactVectorLength();
        //double storeKSpa = origKPointLattice.getMinPeriodicDistance();
        double compactKVec = origKPointLattice.getLongestCompactVectorLength();
        
        String composition = struct.getCompositionString();
        
        //boolean ignoreSymmetry = inputParams.symmetrySetting();
        //int symSetting = inputParams.symmetrySettingVASP();
        //The line below was written to test for when there's no symmetry
        //String nu = (kLat.numDistinctKPoints() == kLat.numTotalKPoints())? "YUP" : "NOPE";
        String minPDLine = formatBasicComment("Actual minimum periodic distance is " + storeMinPerD + " Angstroms.");
        
        //============================ MESSAGES ON POSCAR and PRECALC ============================//
        String reducedDim = "";
        if (inputParams.getRealDimension() == 2) {
            reducedDim = formatBasicLine("Slab structure detected.");
        } else if (inputParams.getRealDimension() == 1) {
            reducedDim = formatBasicLine("Wire structure detected.");
        } else if (inputParams.getRealDimension() == 0) {
            reducedDim = formatBasicLine("Isolated particle detected.");
            minPDLine = ""; //not return anything for isolated particles;
        }
        String gammaComment = formatBasicComment("Grid does not include gamma point.");
        if (includesGammaPoint){
            gammaComment = formatBasicComment("Grid includes gamma point.");
        }
        String forceD = "";
        String forceDiagonalWarning = "";
        if (forceDiagonal) {
            forceD = " FORCEDIAGONAL=" + forceDiagonal;
            forceDiagonalWarning = formatWarningLine("FORCEDIAGONAL is found to not generate fully diagonal grids and has been deprecated. Generating generalized grid...");
        }
        String gapLine = "";
        if (inputParams.gapAdjusted()){
            gapLine = " GAPDISTANCE=" + inputParams.gapDistance();// + " REDUCEDMINTOTALKPOINTS=" + inputParams.getMinTotalPoints();//inputParams.getGapReducedMinTotalPoints();
        }
        
        String searchDepth = "";
        String searchDepthWarning = ""; // deprecate low_symmetry_search_depth;
        String remove_symmetry = inputParams.getGridRemoveSymmetrySettingUsed();
        int numDim = inputParams.getAlteredStructure().numPeriodicDimensions();
        // A safe way to avoid precision errors.
        SpaceGroup symmorphicGroup = inputParams.getAlteredSpaceGroup().getSymmorphicGroup();
        int arithNum = symmorphicGroup.getArithmeticCrystalClassNumber(numDim);
        if (arithNum <= 2 
                || remove_symmetry.equalsIgnoreCase("all")
                || remove_symmetry.equalsIgnoreCase("structural")) {
            if (inputParams.getTriclinicSearchDepth() != 729) {   
                searchDepth = formatBasicComment("Search depth for triclinic structures "
                        + "is changed by user to " + inputParams.getTriclinicSearchDepth() + ".");
            }
        } else if (arithNum >= 3 && arithNum <= 8) {
            if (inputParams.getMonoclinicSearchDepth() != 1728) {
                searchDepth = formatBasicComment("Search depth for monoclinic structures "
                        + "is changed by user to " + inputParams.getMonoclinicSearchDepth() + ".");
            }
        }
        
        if (inputParams.isLowSymmetrySearchDepthProvided()) {
            searchDepthWarning += formatWarningLine("LOW_SYMMETRY_SEARCH_DEPTH "
                    + "is deprecated and will be removed in future releases. "
                    + "Please use MONOCLINIC_SEARCH_DEPTH or TRICLINIC_SEARCH_DEPTH.");
        }
        
        String writeVectorWarning = "";
        String generatingVectorSymWarning = ""; // Generating vectors might not be commensurate with
                                                // crystal reciprocal lattice vectors if symmetries
                                                // were lowered by any reason. (MAGMOM, ISYM, REMOVE_SYMMETRY)
        if (inputParams.kPointsAutoSetting()) {
        	writeVectorWarning = formatWarningLine("WRITE_LATTICE_VECTORS=TRUE might result in "
        			+ "errors if used with VASP versions before 6. For earlier VASP versions, we "
        			+ "recommend setting WRITE_LATTICE_VECTORS to FALSE.");
        	if (inputParams.getGridRemoveSymmetrySettingUsed().equals("structural") ||
        		inputParams.getGridRemoveSymmetrySettingUsed().equals("all")) {
        		// Tim said don't treat as a warning.
        		generatingVectorSymWarning = formatBasicLine("Using WRITE_LATTICE_VECTORS=TRUE "
        				+ "with symmetry disabled might trigger a warning from VASP, but the "
        				+ "results are often still meaningful.");
        	}
        } else {
        	// It's technically not a warning.
        	writeVectorWarning = formatBasicLine("WRITE_LATTICE_VECTORS=FALSE detected. If you "
        			+ "are using VASP.6, we recommend setting WRITE_LATTICE_VECTORS=TRUE.");
        }
        
        String fails = "Unable to properly parse and default values are used for: ";
        if (inputParams.unparseableList().size() == 0) {
            fails = "";
        } else {
            for (int i = 0; i < inputParams.unparseableList().size()-1; i++) {
                fails = fails + inputParams.unparseableList().get(i) + " ";
            }
            fails = formatWarningLine(fails + inputParams.unparseableList().get(inputParams.unparseableList().size()-1) + ".");
        }
        String unknowns = "Cannot recognize these input parameters: ";
        if (inputParams.unrecognizeableList().size() == 0) {
            unknowns = "";
        } else {
            for (int i = 0; i < inputParams.unrecognizeableList().size()-1; i++) {
                unknowns = unknowns + inputParams.unrecognizeableList().get(i) + " ";
            }
            unknowns = formatWarningLine(unknowns + inputParams.unrecognizeableList().get(inputParams.unrecognizeableList().size()-1) + ".");
        }
        String ver = formatBasicComment("Generator version " + m_Version + ".");
        String selectiveDynamics = "";
        if (inputParams.usingSelectiveDynamics()) {
            selectiveDynamics = formatBasicLine("Selective dynamics was detected and taken into account.");
        }
        String gapWarning = "";
        if (inputParams.gapDistance() == 0) {
            gapWarning = formatWarningLine("GAPDISTANCE=0 will treat all atoms as isolated particles.");
        }
        String symFlag = inputParams.getGridRemoveSymmetrySetting(); // It's the user-specified value. Not the actually used one.
        String sym = "";
        if (symFlag.equalsIgnoreCase("all")) {
            sym = formatBasicLine("Grid was generated ignoring structural and time-reversal symmetry.");
        } else if (symFlag.equalsIgnoreCase("structural")){
            sym = formatBasicLine("Grid was generated ignoring structural symmetry."); 
        } else if (symFlag.equalsIgnoreCase("time_reversal")) {
            sym = formatBasicLine("Grid was generated ignoring time-reversal symmetry.");   
        }
        /*String lsOrbitWarning = "";
        if (inputParams.useSpinOrbitCoupling() && symSetting != -1) {
            lsOrbitWarning = formatWarningLine("When LSORBIT=TRUE, VASP recommends using ISYM=-1.");
        }*/
        String sizeWarning = "";
        if (!inputParams.isMinDistProvided() && !inputParams.isMinTotProvided() && !inputParams.isKPPRAProvided()) {
            sizeWarning = formatWarningLine("Grid size was not provided. Using default grid size.");
        }
        String oldVersion = "";
        if (isVersionOlder("C2020.11.15",inputParams.getClientVersion())) {
            oldVersion = formatWarningLine("If you are using a getKPoints script that is older than C2020.11.15, please consider downloading the latest script from https://gitlab.com/muellergroup/k-pointGridGenerator.");
        }
        
        //============================== MESSAGES ON INCAR SETTINGS ==============================//
        INCAR incar = inputParams.getINCAR();
        String magmom = "";
        String nonCollinear = "";
        String nonCollDet = "";
        String nonCollISYM0Warning = "";
        String charg = "";
        String IBRIONWarning="";
        //Make it as broad as possible, check for null instead
        if (incar != null) {
            if (incar.ISPIN() == 2 && incar.MAGMOMString() != null && !incar.LNONCOLLINEAR()) {
                magmom = formatBasicLine("ISPIN=2 and MAGMOM detected in INCAR. Reducing grid symmetry accordingly.");
            }
            /*if (incar.LSORBIT()) {
                if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()) {
                    if (incar.ISYM() == 0) {
                        nonCollinear = formatWarningLine("Using a grid with time-reversal symmetry for a spin orbit calculation is not recommended.");
                    } else if (incar.ISYM() > 0 ) {
                        nonCollinear = formatWarningLine("Using a high symmetry grid for a spin orbit calculation is not recommended. We currently do not generate high symmetry grids for spin orbit calculation. Using only time-reversal symmetry to generate grid.");
                    }
                } else {
                    nonCollinear = lineMessageOutputFormat("Spin orbit calculation detected.");
                }
            }*/
            if (incar.LNONCOLLINEAR() /*&& !incar.LSORBIT()*/) {
                nonCollDet = formatBasicLine("Non-collinear magnetic calculation detected.");
                if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()) {
                    if (incar.ISYM() > 0) {
                        nonCollISYM0Warning = formatWarningLine("Using a grid with time-reversal symmetry for a noncollinear magnetic calculation is not recommended. Consider setting ISYM=-1 in your INCAR file.");
                        nonCollinear = formatWarningLine("Structural symmetry is currently ignored for non-collinear magnetic calculations.");
                    } else if (incar.ISYM() == 0) {
                        nonCollinear = formatWarningLine("Using a grid with time-reversal symmetry for a noncollinear magnetic calculation is not recommended.");
                    }
                } else if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("time_reversal")){
                    if (incar.ISYM() > -1) {
                        nonCollinear = formatWarningLine("User removed time-reversal symmetry. Since structural symmetry is currently ignored for non-collinear magnetic calculations, grid will be generated without any symmetry.");
                        sym = formatBasicLine("Grid was generated ignoring structural and time-reversal symmetry.");
                    }
                } else {
                    //nonCollinear = lineMessageOutputFormat("Non-collinear magnetic calculation detected.");
                }
            } //Make sure noncollinear is on when spin orbit is on, check value of ISYM, if ISYM = -1 don't use time reversal symmetry (message)
            //Two cases:  1)  useNonCollinear && ISYM > 0.  "WARNING:  Non-collinear calculation detected with ISYM > 0. Using only time-reversal symmetry in generating grid."  2)  useNonCollinear && ISYM <= 0.  "Non-collinear calculation detected."
            
            // Phonon / Elastic tensor: Force symmetries to be switched off only when using list format.
            if (!inputParams.kPointsAutoSetting() && incar.ISYM() >= 1) {
                if (incar.IBRION() >= 5 && incar.IBRION() <= 8 ) {
                	if (!inputParams.gridRemoveSymmetrySettingProvided()) {
                		IBRIONWarning = formatWarningLine("IBRION value might break symmetry. Grid was generated ignoring structural symmetry by default.");
                    } else if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("TIME_REVERSAL")) {
                    	IBRIONWarning = formatWarningLine("IBRION value might break symmetry. Grid was generated ignoring structural symmetry in addition to time-reversal symmetry.");
                    	sym = "";
                    } else if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("NONE")) {
                    	IBRIONWarning = formatWarningLine("IBRION value might break symmetry. REMOVE_SYMMETRY is overrode and grid was generated ignoring structural symmetry.");
                    }
                }
            }
            
            if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()){
                if (incar.ISYM() > 0) {
                    if (incar.ISPIN() == 2 || incar.LNONCOLLINEAR()) {
                        if (incar.ICHARG() == 1 || incar.ICHARG() == 11) {
                            charg = formatWarningLine("ICHARG="+ incar.ICHARG() +" and ISPIN=2. Magnetic moments in CHGCAR file can break symmetry and this is not taken into account during grid generation. Please consider setting REMOVE_SYMMETRY=ALL or REMOVE_SYMMETRY=STRUCTURAL in your PRECALC file.");
                        }
                    }
                }
            }
        }
        String betaMode = "";
        if (inputParams.isBetaMode()) {
            betaMode = formatWarningLine("BETA_MODE initialized. Beta version functionalities which may not have been thoroughly tested are used. Please contact us at kpoints@jhu.edu if you found any bugs."); 
        }
        
        String precisionAdjustNotice = "";
        if (inputParams.isPrecisionAdjusted()) {
            precisionAdjustNotice = formatBasicLine("Borderline symmetry detected. Precision in determining symmetry is automatically increased.");
        }
        //Added ICHARG check.  If ICHARG=1  or 11, then if structural symmetry is requested "WARNING: ICHARG=whatever detected and ISPIN=2. Magnetic moments in CHGCAR file can break symmetry, and this is not taken into account during grid generation. Please consider setting SYMMETRY=NONE or SYMMETRY=TIME_REVERSAL in your PRECALC file.
        /*String spinOrbit = "";
        if (inputParams.useSpinOrbitCoupling() && inputParams.useSpinOrbitCouplingTriggered()) {
            spinOrbit = lineMessageOutputFormat("Spin orbit coupling detected. Generating grid without symmetry.");
        }
        String nonCollWarning = "";
        if (inputParams.useNonCollinear() && inputParams.useNonCollinearTriggered() && !inputParams.useSpinOrbitCoupling() && inputParams.LNoncollinearWithoutReducedSymmetry()) {
            nonCollWarning = formatLineWarning("Non-collinear calculation with symmetry detected in INCAR. We currently do not return these grids. Using only time-reversal symmetry in generating grid.");
        }
        String spinOrbitWarning = "";
        if (inputParams.useSpinOrbitCoupling() && inputParams.useSpinOrbitCouplingTriggered() && inputParams.LSorbitWithoutSymmetryOff()) {
            spinOrbitWarning = formatLineWarning("Spin orbit coupling with symmetry detected in INCAR. We currently do not generate grids with structural symmetry and without time-reversal symmetry. Grid was generated without symmetry.");
        }*/
        //String warningOn4_25 = formatBasicComment("Important Notice: There was a bug in version 2016.04.25 that can lead to incorrect k-point weights. Please replace grids that were made with version 2016.04.25. We apologize for the inconvenience.");
        String separator = formatBasicLine("+++SEPARATE_HERE+++");
        if (!inputParams.isDefault()) {
            if (inputParams.headerVerbosity().equals("VERBOSE")) { // HEADER = VERBOSE
                String comment = ver + System.lineSeparator() 
                        //+ warningOn4_25
                        + oldVersion
                        + forceDiagonalWarning
                        //+ spinOrbit
                        //+ spinOrbitWarning
                        + nonCollDet
                        + nonCollinear
                        + nonCollISYM0Warning
                        //+ nonCollWarning
                        + magmom
                        + IBRIONWarning
                        + charg
                        + selectiveDynamics
                        + precisionAdjustNotice
                        + sym
                        + searchDepth
                        + searchDepthWarning
                        + writeVectorWarning
                        + generatingVectorSymWarning
                        //+ lsOrbitWarning
                        + fails 
                        + unknowns 
                        + sizeWarning 
                        + gapWarning 
                        + "K-point grid has " + kLat.numTotalKPoints() + " total points and " + kLat.numDistinctKPoints() + " distinct points." + System.lineSeparator() 
                        + reducedDim
                        + separator 
                        + ver + "Parameters used: MINDISTANCE=" + minDistance 
                        + " INCLUDEGAMMA=" + includeGamma + forceD
                        + " MINTOTALKPOINTS=" + inputParams.getMinTotalPoints() 
                        + " KPPRA=" + inputParams.getKPPRA() 
                        + gapLine 
                        + " TRICLINIC_SEARCH_DEPTH=" + inputParams.getTriclinicSearchDepth()
                        + " MONOCLINIC_SEARCH_DEPTH=" + inputParams.getMonoclinicSearchDepth()
                        + ". "
                        + "K-point grid has " + kLat.numTotalKPoints() + " total points and "
                        + kLat.numDistinctKPoints() + " distinct points. " 
                        + minPDLine 
                        + gammaComment
                        + searchDepth;
                return comment;
            } else if (inputParams.isBetaMode()) { // BETA_MODE = TRUE
                String comment = ver + System.lineSeparator() 
		                //+ warningOn4_25
		                + oldVersion
		                + forceDiagonalWarning
		                //+ spinOrbit
		                //+ spinOrbitWarning
		                + nonCollDet
		                + nonCollinear
		                + nonCollISYM0Warning
		                //+ nonCollWarning
		                + magmom
		                + IBRIONWarning
		                + charg
		                + selectiveDynamics
		                + precisionAdjustNotice
		                + sym 
		                + searchDepth
		                + searchDepthWarning
		                + writeVectorWarning
		                + generatingVectorSymWarning
		                //+ lsOrbitWarning
		                + fails 
		                + unknowns 
		                + sizeWarning
		                + gapWarning
		                + reducedDim
		                + gapWarning
		                + betaMode
		                + separator 
		                + ver + "||BETA_MODE|| K-points for " + composition 
		                + " with MINDISTANCE=" + minDistance 
		                + " Angstroms and INCLUDEGAMMA=" + includeGamma 
		                + " FORCEDIAGONAL=" + forceDiagonal 
		                + " has " + kLat.numTotalKPoints() 
		                + " total points and " + kLat.numDistinctKPoints() 
		                + " distinct points. Effective minDistance " 
		                + storeMinPerD + " Angstroms. " 
		                + searchDepth;
		        return comment;
            } else if (inputParams.isBetaMode() && inputParams.headerVerbosity().equals("DEBUG")) {
            	// BETA_MODE = TRUE && HEADER = DEBUG
                String comment = ver + System.lineSeparator() 
                        //+ warningOn4_25
                        + oldVersion
                        + forceDiagonalWarning
                        //+ spinOrbit
                        //+ spinOrbitWarning
                        + nonCollDet
                        + nonCollinear
                        + nonCollISYM0Warning
                        //+ nonCollWarning
                        + magmom
                        + IBRIONWarning
                        + charg
                        + selectiveDynamics
                        + precisionAdjustNotice
                        + sym  
                        + searchDepth
                        + searchDepthWarning
                        + writeVectorWarning
                        + generatingVectorSymWarning
                        //+ lsOrbitWarning
                        + fails 
                        + unknowns 
                        + sizeWarning 
                        + gapWarning 
                        + separator 
                        + ver + "||BETA_MODE|| K-points for " + composition 
                        + " with MINDISTANCE=" + minDistance 
                        + " and INCLUDEGAMMA=" + includeGamma 
                        + " FORCEDIAGONAL=" + forceDiagonal 
                        + " has " + kLat.numTotalKPoints() 
                        + " total points and " + kLat.numDistinctKPoints() 
                        + " distinct points. Effective minDistance of " 
                        + storeMinPerD + " Angstroms, has length of compact k-vector: " 
                        + compactKVec + " 1/Angstroms, and length of compact r-vector: " 
                        + compactRVec + " Angstroms. " 
                        + searchDepth;
                return comment;
            } else  { // HEADER = SIMPLE
                String comment = ver + System.lineSeparator() 
                        //+ warningOn4_25
                        + oldVersion
                        + forceDiagonalWarning
                        //+ spinOrbit
                        //+ spinOrbitWarning
                        + nonCollDet
                        + nonCollinear
                        + nonCollISYM0Warning
                        //+ nonCollWarning
                        + magmom
                        + IBRIONWarning
                        + charg
                        + selectiveDynamics 
                        + precisionAdjustNotice
                        + sym  
                        + searchDepth
                        + searchDepthWarning
                        + writeVectorWarning
                        + generatingVectorSymWarning
                        //+ lsOrbitWarning
                        + fails 
                        + unknowns 
                        + sizeWarning 
                        + gapWarning 
                        + reducedDim 
                        + separator 
                        + ver 
                        + "K-point grid has " + kLat.numTotalKPoints() + " total points and " 
                        + kLat.numDistinctKPoints() + " distinct points. " 
                        + minPDLine 
                        + gammaComment
                        + searchDepth;
                return comment;
            }
        } else { // No PRECALC is provided. Used default settings.
            String comment = ver + System.lineSeparator() 
                    //+ warningOn4_25
                    + oldVersion
                    + forceDiagonalWarning
                    //+ spinOrbit
                    //+ spinOrbitWarning
                    + nonCollDet
                    + nonCollinear
                    + nonCollISYM0Warning
                    //+ nonCollWarning
                    + magmom
                    + IBRIONWarning
                    + charg
                    + selectiveDynamics 
                    + precisionAdjustNotice
                    + sym  
                    + searchDepth
                    + searchDepthWarning
                    + writeVectorWarning
                    + generatingVectorSymWarning
                    //+ lsOrbitWarning
                    + fails 
                    + unknowns 
                    + sizeWarning 
                    + gapWarning
                    + reducedDim
                    + separator 
                    + ver 
                    + "K-point grid was generated using default values. "
            		+ "Grid has " + kLat.numTotalKPoints() + " total points and "
                    + kLat.numDistinctKPoints() + " distinct points. "
                    + minPDLine 
                    + gammaComment;
            return comment;
        }
    }
    
    /**
     * Load one set of k-point grids the altered space group. Each time the structure is
     * re-scaled and the space group changed, a new collection is loaded. 
     * 
     * @param inputParams
     * @return true, if successfully loaded a collection or dynamically generated a collection.
     *         false, otherwise.
     */
    public boolean loadKPointLibrary (ServerInput inputParams){
        
        int numDim = 3; // Only have collections for 3D grid. 
        
        String removeSymmetrySetting = inputParams.getGridRemoveSymmetrySettingUsed();
        KPointLibrary.SYMMETRY_TYPE sType = KPointLibrary.SYMMETRY_TYPE.ALL;
        
        if (removeSymmetrySetting.equalsIgnoreCase("structural")) {
            sType = KPointLibrary.SYMMETRY_TYPE.TIME_REVERSAL_ONLY;
        } else if (removeSymmetrySetting.equalsIgnoreCase("all")) {
            sType = KPointLibrary.SYMMETRY_TYPE.NONE;
        } else if (removeSymmetrySetting.equalsIgnoreCase("time_reversal")) {
            sType = KPointLibrary.SYMMETRY_TYPE.STRUCTURE_ONLY;
        }
        
        // If the structure has been adjusted, we try to re-find the new space group of the altered
        // structure.
        SpaceGroup structSpaceGroup = inputParams.getAlteredSpaceGroup();
        
        // The space group from which user-required symmetries have been deleted.
        SpaceGroup removeSymSpace = KPointLibrary.getArithmeticGroup(structSpaceGroup, sType);
        
        // Since we use centro-symmorphic collections for now, we can only load grids with full
        // symmetry and redundant ones are removed later. Hence, although STRUCTURE_ONLY, we need 
        // to add inversion.
        int arithNum = (sType == SYMMETRY_TYPE.STRUCTURE_ONLY) ? 
                removeSymSpace.getSymmorphicGroup(true).getArithmeticCrystalClassNumber(numDim) :
                removeSymSpace.getArithmeticCrystalClassNumber(numDim);
        
        SpaceGroup space = null;
        try {
            // arithNum has to be 1 of the 25 symmorphic space groups which have inversion centers.
            // Otherwise, it will throw NullPointerException
            space = SpaceGroup.getSymmorphicSpaceGroup(numDim, arithNum);
        } catch (NullPointerException e) {
            String err = "Try to load a non-centrosymmetric lattice collection!";
            this.reportError(err, e);
            return false;
        }
        
        // Maximum number of k-points in explicitly generated grids.
        int maxNumKpts = 5832;
        if (arithNum <= 2) {
            maxNumKpts = inputParams.getTriclinicSearchDepth();
        } else if (arithNum >= 3 && arithNum <= 8 ) {
            maxNumKpts = inputParams.getMonoclinicSearchDepth();
        } else if (arithNum >= 62) {
            maxNumKpts = 46656;
        }
        
        if (arithNum < 9) {
            long genStart = System.nanoTime();
            
            m_Library.setFactory(arithNum, numDim, true, 
                    new DynamicLatticeGenerator(KPointLibrary.INCLUDE_GAMMA.TRUE, maxNumKpts));
            m_Library.setFactory(arithNum, numDim, false,
                    new DynamicLatticeGenerator(KPointLibrary.INCLUDE_GAMMA.FALSE, maxNumKpts));

            long genFin = System.nanoTime();
            
            Status.detail("Generating lattice for low symmetry group "  + arithNum + " takes " 
                    + (genFin - genStart)/1000000.0 + " ms." );
        } else {
    
            long readStart = System.nanoTime();
            
            KPointLatticeCollection gammaCollLoad = null;
            KPointLatticeCollection shiftedCollLoad = null;
            try {
                boolean useBinary = true;
                
                gammaCollLoad = loadCollection(collectionDirectory, space, numDim, maxNumKpts, 
                                               true, useBinary);
                shiftedCollLoad = loadCollection(collectionDirectory, space, numDim, maxNumKpts, 
                                                 false, useBinary);
            } catch (FileNotFoundException e) {
                String err = "Lattice Collections not found! " 
                        + System.lineSeparator()
                        + "Please check whether the LATTICE_COLLECTIONS variable is set correctly, "
                        + "or whether the collection files are of the version: "
                        + KPointLatticeCollection.m_CollectionVersion + ".";
                this.reportError(err, e);
                return false;
            } catch (IOException e) {
                String err;
                if (e.getMessage() != null) {
                    err = e.getMessage();
                } else {
                    err = "Fail to load the lattice collection!";
                }
                this.reportError(err, e);
                return false;
            }
            
            long readEnd = System.nanoTime();
            
            Status.detail("Reading the KPointLatticeCollection took: " 
                    + (readEnd - readStart)/1000000.0 + " ms.");
            m_Library.setFactory(arithNum, numDim, true, gammaCollLoad);
            m_Library.setFactory(arithNum, numDim, false, shiftedCollLoad);
            /*
            // Try the dynamic generator.
            m_Library.setFactory(arithNum, numDim, true, 
                    new DynamicLatticeGenerator(KPointLibrary.INCLUDE_GAMMA.TRUE, maxNumKpts));
            m_Library.setFactory(arithNum, numDim, false,
                    new DynamicLatticeGenerator(KPointLibrary.INCLUDE_GAMMA.FALSE, maxNumKpts));
            */
        }
        return true;
    }
    
    /*
     * Load one collection for the given space group. (For modularization and reducing repetition 
     * of codes.)
     */
    private static KPointLatticeCollection loadCollection(String collLoc, 
            SpaceGroup space, int numDim, int maxNumKpts, boolean includeGamma, boolean useBinary) 
                    throws FileNotFoundException, IOException { 
        int arithNum = space.getArithmeticCrystalClassNumber(numDim);
        String spt = System.getProperty("file.separator");
        String shiftPath = includeGamma ? 
                spt + "Gamma" + spt + "lattices_G_" : 
                spt + "Shifted" + spt + "lattices_S_";
        String filePath = collLoc + shiftPath + arithNum + "_" + maxNumKpts
                + "_" + KPointLatticeCollection.m_CollectionVersion;
        KPointLatticeCollection collLoad = 
                new KPointLatticeCollection(filePath, useBinary);
        collLoad.setDefaultFilePath(filePath);
        
        return collLoad;
    }
    
    /*
     * Strip comments in a line in VASP input files.
     */
    private String stripComments(String line) {
        int index = line.indexOf("#");
        if (index >= 0) {
          line = line.substring(0, index);
        }
        int index2 = line.indexOf("!");
        if (index2 >= 0) {
          line = line.substring(0, index2);
        }
        return line;
    }
    
    /**
     * Old method dedicated for VASP.
     * 
     * Detect selective dynamics flags and mark the atoms with extra labels to indicate atoms with 
     * restricted freedom are of different type. 
     * 
     * Restricted atoms get "_SD" added to their symbol. For partially restricted atoms, 
     * extra atoms at slightly perturbed positions are added with species name "SD_F" and "SD_T" 
     * to safely break the symmetry. "SD_F" for one-dimensional frozen case, and "SD_T" for 
     * two-dimensional frozen case. These additional atoms are added only within this code. 
     * The POSCAR won't be changed in any way.
     * 
     * @param inputParams
     * @param inStructure
     * @return structure with restricted atoms got extra labels in their species symbol, and extra
     *         atoms added with species symbols: "SD_F" or "SD_T". 
     */
    public static Structure modVASPSelectiveDynamics(ServerInput inputParams, 
            Structure inStructure) {
        
        String filePath = inputParams.filePath();
        double perturbationLength = 0.01;
        
        POSCAR poscarVersion = inputParams.getPOSCAR();
        StructureBuilder builder = new StructureBuilder(inStructure);
        Vector[] cellVectors = inStructure.getCellVectors();
        for (int siteNum = 0; siteNum < poscarVersion.numDefiningSites(); siteNum++) {
            int numFrozen = 0;
            int lastF = -1;
            int lastT = -1;
            for (int dimNum = 0; dimNum < 3; dimNum++) {
                boolean canMove = poscarVersion.getSelectiveDynamics(siteNum, dimNum);
                if (canMove) {
                    lastT = dimNum;
                } else {
                    numFrozen++;
                    lastF = dimNum;
                }
            }
            if (numFrozen == 0) {continue;}
            Coordinates siteCoords = builder.getSiteCoords(siteNum);
            if (numFrozen == 1) {
                Vector vector = cellVectors[lastF].resize(perturbationLength);
                Coordinates coords1 = siteCoords.translateBy(vector);
                Coordinates coords2 = siteCoords.translateBy(vector.multiply(-1));
                builder.addSite(coords1, Species.get("SD_F"));
                builder.addSite(coords2, Species.get("SD_F"));
            } else if (numFrozen == 2) {
                Vector vector = cellVectors[lastT].resize(perturbationLength);
                Coordinates coords1 = siteCoords.translateBy(vector);
                Coordinates coords2 = siteCoords.translateBy(vector.multiply(-1));
                builder.addSite(coords1, Species.get("SD_T"));
                builder.addSite(coords2, Species.get("SD_T"));
            } else if (numFrozen == 3) {
                Species currSpecies = builder.getSiteSpecies(siteNum);
                String symbol = currSpecies.getElementSymbol().concat("_SD");
                builder.setSiteSpecies(siteNum, Species.get(symbol));
            }
        }
        inStructure = new Structure (builder);
        POSCAR out2 = new POSCAR(inStructure);
        out2.writeFile(filePath + "/POSCAR.selectiveDynamics.vasp");
        return inStructure;
    }
        
    /**
     * More generic version of the function modVASPSelectiveDynamics(), expanded to include support
     * for Quantum Espresso.
     * 
     * @param inputParams
     * @param inStructure
     * @param numSites
     * @param selectiveDynamicsArray
     * @return structure with restricted atoms got extra labels in their species symbol, and extra
     *         atoms added with species symbols: "SD_F" or "SD_T". 
     */
    public static Structure modGenericSelectiveDynamics(ServerInput inputParams, 
            Structure inStructure, int numSites, boolean[][] selectiveDynamicsArray) {
        String filePath = inputParams.filePath();
        double perturbationLength = 0.01;
        
        StructureBuilder builder = new StructureBuilder(inStructure);
        Vector[] cellVectors = inStructure.getCellVectors();
        for (int siteNum = 0; siteNum < numSites; siteNum++) {
            int numFrozen = 0;
            int lastF = -1;
            int lastT = -1;
            for (int dimNum = 0; dimNum < 3; dimNum++) {
                boolean canMove = selectiveDynamicsArray[siteNum][dimNum];
                if (canMove) {
                    lastT = dimNum;
                } else {
                    numFrozen++;
                    lastF = dimNum;
                }
            }
            if (numFrozen == 0) {continue;}
            Coordinates siteCoords = builder.getSiteCoords(siteNum);
            if (numFrozen == 1) {
                Vector vector = cellVectors[lastF].resize(perturbationLength);
                Coordinates coords1 = siteCoords.translateBy(vector);
                Coordinates coords2 = siteCoords.translateBy(vector.multiply(-1));
                builder.addSite(coords1, Species.get("SD_F"));
                builder.addSite(coords2, Species.get("SD_F"));
            } else if (numFrozen == 2) {
                Vector vector = cellVectors[lastT].resize(perturbationLength);
                Coordinates coords1 = siteCoords.translateBy(vector);
                Coordinates coords2 = siteCoords.translateBy(vector.multiply(-1));
                builder.addSite(coords1, Species.get("SD_T"));
                builder.addSite(coords2, Species.get("SD_T"));
            } else if (numFrozen == 3) {
                Species currSpecies = builder.getSiteSpecies(siteNum);
                String symbol = currSpecies.getElementSymbol().concat("_SD");
                builder.setSiteSpecies(siteNum, Species.get(symbol));
            }
        }
        inStructure = new Structure (builder);
        POSCAR out2 = new POSCAR(inStructure);
        out2.writeFile(filePath + "/POSCAR.selectiveDynamics.vasp");
        return inStructure;
    }
    
    /**
     * Parse the MAGMOM string and label the atoms' species name with magnetic moments. When
     * determine symmetry, the atoms with different species name will be determined as different.
     * Hence, symmetries will be properly broken.
     * 
     * @param inputParams
     * @param inStructure
     * @return a structure with atoms labeled with magnetic moments.
     */
    protected static Structure modMagneticMoment(ServerInput inputParams, Structure inStructure) {
        String filePath = inputParams.filePath();
        POSCAR poscarVersion = inputParams.getPOSCAR();
        INCAR incar = inputParams.getINCAR();
        StructureBuilder builder = new StructureBuilder(inStructure);
        //Create a structure void of any sites.
        StructureBuilder outStruct = new StructureBuilder(inStructure);
        outStruct.removeAllSites();

        // Parse the MAGMOM string. Collinear calculation only has one double for each atom. 
        String magmomValue = incar.MAGMOMString();
        String[] magneticValues = magmomValue.split("\\s+");
        ArrayList<Integer> specifiedAtoms = new ArrayList<Integer>();
        ArrayList<Double> specifiedMoments = new ArrayList<Double>();
        int totalSpecifiedAtoms = 0;
        for (int a = 0; a < magneticValues.length; a++) {
            String pair = magneticValues[a];
            // Check whether it contains a multiplication
            if(!pair.matches(".*\\d.*")){ //break if there are no numbers, that means we're done
                break;
            }
            if (pair.contains("*")) {
                String[] atmomPair = pair.split("[*]");
                try {
                    Integer at = Integer.parseInt(atmomPair[0]);
                    totalSpecifiedAtoms = totalSpecifiedAtoms + at;
                    specifiedAtoms.add(at);
                    specifiedMoments.add(Double.parseDouble(atmomPair[1]));
                } catch (NumberFormatException | NullPointerException e) {
                    throw new NumberFormatException(
                            "MAGMOM values are not directly adjacent to \"*\".");
                }
            } else { //No, then it is the moment.
                specifiedAtoms.add(1);
                specifiedMoments.add(Double.parseDouble(pair));
                totalSpecifiedAtoms++;
            }
        }
            
        // There could be atoms that are not specified above.
        if (totalSpecifiedAtoms != poscarVersion.numDefiningSites()) {
            return null; //VASP would return an error here.
        }
        int siteCounter = 0;
        int sitePad = 0;
        for (int b = 0; b < specifiedAtoms.size(); b++) {
            int numAt = specifiedAtoms.get(b);
            double mom = specifiedMoments.get(b);
            sitePad = sitePad + numAt;
            while (siteCounter < sitePad) {
                Coordinates siteCoords = builder.getSiteCoords(siteCounter);
                Species siteSpec = builder.getSiteSpecies(siteCounter);
                String specName = siteSpec.getElementSymbol() + "_" + mom + " ";
                outStruct.addSite(siteCoords, Species.get(specName, siteSpec.getIsotope(), 
                                                                siteSpec.getOxidationState()));
                siteCounter++;
            }
        }
        inStructure = new Structure (outStruct);
        POSCAR out2 = new POSCAR(inStructure);
        out2.writeFile(filePath + "/POSCAR.magmom.vasp");
        return inStructure;
    }
    
    /**
     * Read in the MAGMOM line and discard the broken symmetries.
     * Note: the space group is altered within this method. "inputParams.m_AlteredSpaceGroup" 
     *       is changed. But the atoms are not labeled. Therefore, if we try to re-find the space
     *       group in cases of border-line symmetry detected, we need to re-parse the MAGMOM
     *       again. 
     * @param inputParams
     */
    protected static void modMagneticMomentNCL(ServerInput inputParams) {
        long start = System.currentTimeMillis();
          
        if (!inputParams.isINCARIncluded()) {return;}
        if (!inputParams.getINCAR().LSORBIT() && !inputParams.getINCAR().LNONCOLLINEAR()) {return;}
                
        INCAR incar = inputParams.getINCAR();
        Structure currentStructure = inputParams.getAlteredStructure();
        SpaceGroup currentSpaceGroup = inputParams.getAlteredSpaceGroup();
             
        String saxisString = null;
        if (inputParams.getINCAR().SAXIS() == null) {
            saxisString = "0 0 1"; //VASP default
        } else {
            saxisString = incar.SAXIS();
        }
        
        ArrayList<Double> momentValues = new ArrayList<Double>();
        if (inputParams.getINCAR().MAGMOMString() == null) {
            int numAt = currentStructure.numDefiningSites();
            int numMom = numAt * 3;
            for (int siteNum = 0; siteNum < numMom; siteNum++) {
                momentValues.add(1.0); //VASP default "1 1 1" for each atom
            }
        } else {
            momentValues = incar.MAGMOM();
        }
            
        // The values specified are not divisible by 3 (3 directions in space)
        if ((momentValues.size()) % 3 != 0) {
            throw new RuntimeException("The number of magnetic moment vectors specified "
                    + "does not match the number of atoms.");
        }
        double[][] momentVectors = new double[(momentValues.size())/3][3];
        for (int c = 0; c < momentVectors.length; c++) {
            for (int d = 0; d < momentVectors[c].length; d++) {
                momentVectors[c][d] = momentValues.get(d +(c*3));
            }
        }

        String[] saxisVals = saxisString.trim().split("[\\s+]");
        if (saxisVals.length != 3) {
            throw new RuntimeException("The number of SAXIS vector component is not equal to 3.");
        }
        double[] saxis = new double[3];
        for (int g = 0; g < saxisVals.length; g++) {
            saxis[g] = Double.parseDouble(saxisVals[g].trim());
        }
            
        double alpha = Math.atan(saxis[1]/saxis[0]);
        if (saxis[0] == 0 && saxis[1] == 0) {
            alpha = 0; //Math.PI/2;
        } else if (saxis[0] == 0 && saxis[1] != 0) {
            alpha = Math.PI/2;
        }
        double beta = Math.atan(Math.sqrt(saxis[0]*saxis[0] + saxis[1]*saxis[1])/saxis[2]);
        if (saxis[2] == 0) {
            beta = Math.PI/2;
        }
            
        double[][] transformedVectors = new double[momentVectors.length][3];
        Vector[] magVec = new Vector[momentVectors.length];
        for (int vecCount = 0; vecCount < momentVectors.length; vecCount++) {
            double vx = momentVectors[vecCount][0];
            double vy = momentVectors[vecCount][1];
            double vz = momentVectors[vecCount][2];        
            transformedVectors[vecCount][0] = Math.cos(beta) * Math.cos(alpha) * vx 
                                            - Math.sin(alpha) * vy 
                                            + Math.sin(beta) * Math.cos(alpha) * vz;
            transformedVectors[vecCount][1] = Math.cos(beta) * Math.sin(alpha) * vx 
                                            + Math.cos(alpha) * vy 
                                            + Math.sin(beta) * Math.sin(alpha) * vz;
            transformedVectors[vecCount][2] = -Math.sin(beta) * vx + Math.cos(beta) * vz;
            
            Status.detail("transformed vector: " + vecCount + " is " + 
                    Arrays.toString(transformedVectors[vecCount]));
            
            magVec[vecCount] = new Vector(transformedVectors[vecCount], 
                    CartesianBasis.getInstance());
        }
              
        // Now we find the symmetry operations compatible with the vector
        ArrayList<SymmetryOperation> keepers = new ArrayList<SymmetryOperation>();
        try {
            for (int opNum = 0; opNum < currentSpaceGroup.numOperations(); opNum++) {
                SymmetryOperation op = currentSpaceGroup.getOperation(opNum);
                boolean isOpOK = true;
                Status.detail("opNum: " + opNum);
                for (int siteNum = 0; siteNum < currentStructure.numDefiningSites(); siteNum++) {
                  
                    Coordinates siteCoords = currentStructure.getSiteCoords(siteNum);
                    Coordinates oppedCoords = op.operate(siteCoords);
                    int oppedIndex = currentStructure.getSite(oppedCoords).getIndex();

                    //Operate the vector with the symmetry operation of the site
                    Vector origVec = magVec[siteNum];
                    Vector oppedVec = op.operate(origVec);
                    Vector compareVec = magVec[oppedIndex];
                
                    //Subtract the two vectors, VASP precision is at 1E-6
                    if (compareVec.subtract(oppedVec).length() > 1E-6) {
                        isOpOK = false;
                        Status.detail("The two vectors are not the same.");
                        break;
                    }
                }
                if (isOpOK) {
                    Status.detail("op is OK");
                    keepers.add(op);
                }
            }
        } catch (NullPointerException e) {
            throw new SymmetryException("Unable to find site equivalencies, "
                    + "loosening precision might help.");
        }

        // Finally we complete the space group.
        SymmetryOperation[] keeperArray = keepers.toArray(new SymmetryOperation[0]);
        int numKeepers = keeperArray.length;
        keeperArray = SpaceGroup.completeGroup(keeperArray, currentStructure.getDefiningLattice());
        if (keeperArray.length != numKeepers) { // We didn't find some ops;
          throw new SymmetryException("Unable to find some symmetry operations "
                  + "while adjusting for non-collinear calculations.");
        }
        SpaceGroup space = new SpaceGroup(keeperArray, currentSpaceGroup.getLattice());
        
        inputParams.setAlteredSpaceGroup(space);
        long end = System.currentTimeMillis();
        inputParams.setMAGMOMNCLAdjustTime(end - start);
    }

//----------------------------------IO driver classes below---------------------------------------//
    
    private static void fileNotFoundError(PrintWriter logger, File fi) {
        String msg = formatErrorComment(fi.getName() 
                + " cannot be opened for reading! " 
                + "Please check its permission status, "
                + "or other processes have locked the file.");
        logger.println(msg);
    }
    
    /**
     * Emulate the function "tee" in Linux: Report message to both logger file and standard output.
     * Since each instance has its own logger, this function can only be non-static.
     */
    private void reportBasic(String str) {
        Status.basic(str);
        this.logger.println(str);
    }
    
    private void reportError(String err) {
        System.err.println("*** ERROR: " + err + " ***");
        this.logger.println("*** ERROR: " + err + " ***");
    }
    
    private void reportWarning(String warning) {
    	System.out.println("*** WARNING: " + warning + " ***");
    	this.logger.println("*** WARNING: " + warning + " ***");
    }
    
    private void reportError(String err, Exception e) {
    	System.err.println("*** ERROR: " + err + " ***");
        this.logger.println("*** ERROR: " + err + " ***");
        e.printStackTrace(this.logger);
    }
    
    private void cleanUp() {
        this.logger.flush();
        this.logger.close();
    }
 
    public static String logOutputFormat(String line) {
        return new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss.SSSSSS")
                .format(Calendar.getInstance().getTime()) + "     | " + line;
    }
 
    /**
     * As part of the header comment of KPOINTS.
     * @param message
     * @return
     */
    public static String formatBasicComment(String message) {
        return message + " "; 
    }
    
    /**
     * As part of the header comment of KPOINTS. Therefore, don't add "***" at the beginning.
     * @param warning
     * @return
     */
    public static String formatWarningComment(String warning) {
    	return "WARNING: " + warning + " ";
    }
    
    /**
    * As part of the header comment of KPOINTS. Therefore, don't add "***" at the beginning.
    * @param error
    * @return
    */
    public static String formatErrorComment(String error) {
    	return "ERROR: " + error + " ";
    }
 
    /**
     * As a line to standard output.
     * @param line
     * @return
     */
    public static String formatBasicLine(String line) {
        return line + System.lineSeparator();
    }
    
    public static String formatWarningLine(String warning) {
    	return "*** WARNING: " + warning + " ***" + System.lineSeparator();
    }
 
    public boolean isVersionOlder(String currentVersion, String query) {
        String procVer = currentVersion.replace("C", "").trim();
        String[] date = procVer.split("[\\.]");
        String procQ = query.replace("C", "").trim();
        String[] qSplit = procQ.split("[\\.]");
        if (qSplit.length != date.length) {
            return true;
        }
        if (Integer.parseInt(qSplit[0].trim()) > Integer.parseInt(date[0].trim())) {
            return false;
        } else if (Integer.parseInt(qSplit[0].trim()) < Integer.parseInt(date[0].trim())){
            return true;
        } else {
            if (Integer.parseInt(qSplit[1].trim()) > Integer.parseInt(date[1].trim())) {
                return false;
            } else if (Integer.parseInt(qSplit[1].trim()) < Integer.parseInt(date[1].trim())) {
                return true;
            } else {
                if (Integer.parseInt(qSplit[2].trim()) >= Integer.parseInt(date[2].trim())) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }
    
    public boolean isInteger( String input ) {
        try{
            Integer.parseInt( input );
            return true;
        } catch( Exception e ) {
            return false;
        }
    }
         
    /**
     * This method retrieves information from a given request, and then prints them into 
     * a new log file in a given user location.
     * 
     * @param inputParams ServerInput object containing the necessary information.
     * @param fileDestination The location where the summary file will be located.
     * @throws NullPointerException
     * @throws IOException
     */
    private void printSummary(ServerInput inputParams, boolean requestCompleted) 
            throws NullPointerException {
        this.logger.println();
        this.logger.println("-------------------------SUMMARY-------------------------");
        this.logger.println("MINTOTALPOINTS = " + inputParams.getMinTotalPoints());
        this.logger.println("MINDISTANCE = " + inputParams.getMinDistance());
        this.logger.println("HEADER = " + inputParams.headerVerbosity());
        this.logger.println("REMOVE_SYMMETRY = " + inputParams.getGridRemoveSymmetrySetting() 
                          + " # As listed in the PRECALC file");
        this.logger.println("INCLUDEGAMMA = " + inputParams.includeGamma());
        this.logger.println("KPPRA = " + inputParams.getKPPRA());
        this.logger.println("TRICLINIC_SEARCH_DEPTH = " + inputParams.getTriclinicSearchDepth());
        this.logger.println("MONOCLINIC_SEARCH_DEPTH = " + inputParams.getMonoclinicSearchDepth());
        this.logger.println("WRITE_LATTICE_VECTORS = " + inputParams.kPointsAutoSetting());
        this.logger.println("---END_PRECALC_PARAMETERS---");
        this.logger.println("Scaling_Lattice_Times = " + inputParams.getScalingTimes());
        this.logger.println("Remove_Symmetry_Used_in_Grid_Generation = " 
                          + inputParams.getGridRemoveSymmetrySettingUsed());
        Structure struct = inputParams.getAlteredStructure();
        if (struct == null) { // For the case of no structures in a failed request.
            this.logger.println("Request_Completed = FALSE");
            this.logger.println("NOTE : No structures detected.");
            this.logger.println("Format = " + inputParams.getFormat());
            this.logger.println("Generator_Version = " + m_Version);
            this.logger.println("MessageList = " 
                               + inputParams.isOutputSeparated());
            this.logger.println("Read_INCAR_Time = " 
                               + inputParams.readINCARTime() + " ms.");
            this.logger.println("Read_PRECALC_Time = " 
                               + inputParams.readPRECALCTime() + " ms.");
            this.logger.println("Read_Structure_Time = " 
                               + inputParams.readStructureTime() + " ms.");
            this.logger.println("Gap_Adjust_Time = " + inputParams.gapAdjustTime() + " ms.");
            this.logger.println("Selective_Dynamics_Adjustment_Time = " 
                               + inputParams.selectiveDynamicsAdjustTime() + " ms.");
            this.logger.println("Magnetic_Moment_NonCollinear_Adjustment_Time = " 
                               + inputParams.MAGMOMNCLAdjustTime() + " ms.");
            this.logger.println("Magnetic_Moment_Adjustment_Time = " 
                               + inputParams.magMomAdjustTime() + " ms.");
            this.logger.println("KPoints_IO_Time = " 
                               + inputParams.readKPointsIOTime() + " ms.");
            this.logger.println("Grid_Generation_Time = " 
                               + inputParams.getGridGenerationTime() + " ms.");
            this.logger.println("Total_Grid_Generation_Time = " 
                               + inputParams.getTotalGenerationTime() + " ms.");
            return;
        }
        
        //TODO: For Abinite, we should use an structure array.
        
        // If true, the structure has duplicate sites. Terminate here.
        if (struct.containsCoincidentSites() != -1) {
            return;
        }
        SpaceGroup space = inputParams.getAlteredSpaceGroup().getSymmorphicGroup();
        this.logger.println("Arithmetic_Group_Number = " + space.getArithmeticCrystalClassNumber(
                struct.getDefiningLattice().numPeriodicVectors()));
        this.logger.println("Format = " + inputParams.getFormat());
        this.logger.println("Generator_Version = " + m_Version);
        this.logger.println("MessageList = " + inputParams.isOutputSeparated());
        this.logger.println("Read_INCAR_Time = " 
                          + inputParams.readINCARTime() + " ms.");
        this.logger.println("Read_PRECALC_Time = " 
                          + inputParams.readPRECALCTime() + " ms.");
        this.logger.println("Read_Structure_Time = " 
                          + inputParams.readStructureTime() + " ms.");
        this.logger.println("Gap_Adjust_Time = " 
                          + inputParams.gapAdjustTime() + " ms.");
        this.logger.println("Selective_Dynamics_Adjustment_Time = " 
                          + inputParams.selectiveDynamicsAdjustTime() + " ms.");
        this.logger.println("Magnetic_Moment_Adjustment_Time = " 
                          + inputParams.magMomAdjustTime() + " ms.");
        this.logger.println("KPoints_IO_Time = " 
                          + inputParams.readKPointsIOTime() + " ms."); 
        this.logger.println("Grid_Generation_Time = " 
                          + inputParams.getGridGenerationTime() + " ms.");
        this.logger.println("Total_Grid_Generation_Time = " 
                          + inputParams.getTotalGenerationTime() + " ms.");
        if (requestCompleted) {
            this.logger.println("Request_Completed = TRUE");
        } else {
            this.logger.println("Request_Completed = FALSE");
        }
    }

}
