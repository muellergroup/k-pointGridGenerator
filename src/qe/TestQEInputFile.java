package qe;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import matsci.io.vasp.POSCAR;
import matsci.structure.Structure;

public class TestQEInputFile {
	
	public static void main(String[] args) {
		/*String a = "CELL_PARAMETERS    alat";
		String del = "\\s+";
		String[] g = a.split(del);
		ArrayList<String> ab = new ArrayList<>();
		ab.add(a);
		System.out.println(ab.get(0));*/
		/*ArrayList<String> g = new ArrayList<String>();
		String a = "/";
		g.add(a);
		a = null;
		System.currentTimeMillis();
		if (a.equals("/")) {
			System.out.println("aaa");;
		}*/
	/*	String j = "-1 0.0 1";
		if (j.matches(".*[a-zA-Z].*")) {
			System.out.println("aaa");
		}
		/*String k = j.replaceAll("D", "E");
		double l = Double.parseDouble(k);
		System.out.println("k");*/

		String path = args[0];
/*		String out = args[1];
		String[] allFiles = listFilesInFolder(path);
		for (int a = 0; a < allFiles.length; a++) {
			//QEInputFile qeI = new QEInputFile(path + "/" + allFiles[a]);
			System.out.println("Now processing: " + path + "/" + allFiles[a]);
			QEStructure qeI = null;
			try {
				qeI = new QEStructure(path + "/" + allFiles[a]);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (qeI == null) {
				System.out.println("Cannot process: " + path + "/" + allFiles[a]);
				return;
			}
			Structure stru = new Structure(qeI);
			POSCAR poscar = new POSCAR(stru);
			poscar.writeFile(out + "/" + allFiles[a] + ".vasp");
		}*/
		QEInputFile qeI = new QEInputFile(path + "/input.qe");
		Structure stru = new Structure(qeI);
		POSCAR poscar = new POSCAR(stru);
		poscar.writeFile(path + "/POSCAR");
		
	/*	String file = args[0];
		QEStructure qeI = null;
		try {
			qeI = new QEStructure(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (qeI == null) {
			System.out.println("Cannot process: " + file);
			return;
		}
		Structure stru = new Structure(qeI);
		POSCAR poscar = new POSCAR(stru);
		poscar.writeFile(file + ".vasp");*/
		
	}
	
	public static String[] listFilesInFolder(String dir) {
		File dirName = new File(dir);
		String[] fileFilter = new String[dirName.listFiles().length];
		int counter = 0;
	    for (final File fileEntry : dirName.listFiles()) {
	        if (fileEntry.isFile()) {
	        	fileFilter[counter] = fileEntry.getName();
	        	counter++;
	        } 
	    }
	    
	    //now remove all the nulls (i.e. the names of all the directories.)
		List<String> list = new ArrayList<String>();
		for (String s : fileFilter) {
			if(s != null && s.length() > 0) {
				list.add(s);
			}
		}
		fileFilter = list.toArray(new String[list.size()]);
		
		//System.out.println("dirList: " +Arrays.toString(fileFilter));
	    return fileFilter;
	}

}
