package qe;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;

import abinit.AbinitInfile2;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.CartesianScaleBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.IStructureData;
import matsci.structure.reciprocal.KPointLattice;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

/**
 * Quantum Espresso input file parser
 * Currently has more bugs than there are in an ants nest.
 * 
 * Started: Oct 28, 2016
 * v0: Nov 2, 2016
 * @author PW
 *
 */

public class QEInputFile implements IStructureData{
	
	private String m_Description = "Unknown System";
	private double m_FormatPrecision = 1E-15;
	private double[] m_VectorLengths= new double[3]; //a,b,c
	//I'm storing angles and not cosines (in radii) because it's more convenient
	private double[] m_VectorAngles = new double[3]; //angleAB,angleAC,angleBC
	//TODO: I don't think this is necessary, if I transform everything in terms of lengths and angles
	private double[] m_Celldm = new double[6];
	private ArrayList<String> m_RawInput = new ArrayList<>();;
	private LinkedHashMap<String, Object> m_Param = new LinkedHashMap<>();
	private ArrayList<String> m_FileContent = new ArrayList<>();;
	//I would like to return the "same" file as input only with the kpoints section being different
	private ArrayList<String> m_ReturnedContent = new ArrayList<>();
	//private Structure m_Structure;
	private Species[] m_Species;
	private Coordinates[] m_Coordinates;
	private int m_NumDefiningSites; //feels redundant, but we'll see
	private Vector[] m_Vectors;
	private boolean[] m_IsVectorPeriodic = new boolean[] {true, true, true}; // QE is only deals with periodic lattices?
	private double m_BohrScaling = 0.52917720859; //TODO: check what value used in QE
	private AbstractLinearBasis m_CoordinateBasis;
	private AbstractLinearBasis m_VectorBasis;
	private QEKPoints m_KPoints;
	/*Possible flags of interest
	 * ibrav (of course!)
	 * tot_magnetization
	 * starting_magnetization
	 *  nosym, true == no structural symmetry
	 *  noinv, true == no time-reversal symmetry 
	 *  nosym_evc, true == k-points still has BZ symmetry, so no effect? TODO: Check with Tim
	 *  no_t_rev, true == disable rotation and time-reversal
	 *  force_symmorphic, true == force group to be symmorphic, this is what we already do, I think
	 *  use_all_frac, true == not sure TODO: look this up
	 *  occupations, tetrahedra --> auto generation, yay!
	 *  nspin TODO: see how they specify the spins
	 *  ncolin, TODO: check angle1 and angle2
	 *  space_group, seriously!?
	 *  uniqueb --> corresponds to unique ibrav, what?
	 *  origin_choice --> 1 or 2 corresponds to the first or second origin
	 *  rhombohedral, true then atomic coordinates have rhombohedral basis, false then hexagonal basis
	 */


	@Override
	public String getDescription() {
		return m_Description;
	}

	@Override
	public Vector[] getCellVectors() {
		return java.util.Arrays.copyOf(m_Vectors,m_Vectors.length);
	}

	@Override
	public boolean[] getVectorPeriodicity() {
		return java.util.Arrays.copyOf(m_IsVectorPeriodic, m_IsVectorPeriodic.length);
	}

	@Override
	public int numDefiningSites() {
		return m_NumDefiningSites;
	}

	@Override
	public Coordinates getSiteCoords(int index) {
		return m_Coordinates[index];
	}

	@Override
	public Species getSiteSpecies(int index) {
		return m_Species[index];
	}
	
	public QEInputFile (String fileName) {
		FileReader reader;
		try {
			reader = new FileReader(fileName);
			this.readFile(reader);
		} catch (IOException e){// | NullPointerException e) {
			throw new RuntimeException("Error reading from file: " + fileName);
		}
		
	}
	
	public QEInputFile (Reader reader) {
		try {
			readFile(reader);
		} catch (NullPointerException e) {
			throw new RuntimeException("Error parsing QE file.");
		}
	}
	
	private void processInput() {
		
	}
	
	public void updateWithKPoints(KPointLattice kpointLattice, String header, String comment) {
		m_KPoints = new QEKPoints(kpointLattice);
		m_KPoints.setDescription(comment);
		ArrayList<String> appender = m_KPoints.toArrayList();
		ArrayList<String> commented = new ArrayList<String>();
		commented.add(header);
		commented.addAll(m_ReturnedContent); //TODO: Comment K-Point related lines
		commented.addAll(appender);
		m_ReturnedContent = commented;
	}
	/*
	public void updateKPointsWithDescription(String description) {
		m_KPoints.setDescription(description);
	}*/
	
	
	public void writeReturnedContent(String fileName) { 
	    try {
	      FileWriter fileWriter = new FileWriter(fileName);
	      BufferedWriter writer = new BufferedWriter(fileWriter);
	      this.writeReturnedContent(writer);
	      writer.flush();
	      writer.close();
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write file " + fileName, e);
	    } 
	  }
	  //same case as this
	  public void writeReturnedContent(Writer writer) {
	    try {
	      //writeManual(writer);
	      writeFromList(writer);
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write kpoints.", e);
	    } 
	  }
	  private void writeFromList(Writer writer) throws IOException {
		  for (int a = 0; a < m_ReturnedContent.size(); a++) {
			  writer.write(m_ReturnedContent.get(a));
			  writer.write(System.lineSeparator());
		  }
	  }
	
	
	private void simpleReadFile2 (Reader reader) { //This means multiple arrayList reading for param filling, most foolproof
		Scanner scanner = new Scanner(reader);
		String del = "\\s+";
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			System.currentTimeMillis();
			m_RawInput.add(currentLine);
			m_FileContent.add(currentLine);
			m_ReturnedContent.add(currentLine);
		}
		scanner.close();
	}
	
	private void simpleReadFile1 (Reader reader) { //this is in between, still trying to fill some params
		Scanner scanner = new Scanner(reader);
		String del = "\\s+";
		boolean readMultiLine = false;
		ArrayList<String> multiLineStorage = new ArrayList<String>();
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			System.currentTimeMillis();
			m_RawInput.add(currentLine);
			m_FileContent.add(currentLine);
			m_ReturnedContent.add(currentLine);
			String cleanLine = stripComments(currentLine);
			
			if (cleanLine.length() == 0 || cleanLine == null) {
				if (readMultiLine) {
					//when reading a multi line empty line is considered a close
					//TODO: Check if this is true
					String par = multiLineStorage.get(0);
					multiLineStorage.remove(0);
					String[] str = multiLineStorage.toArray(new String[multiLineStorage.size()]);
					m_Param.put(par, str);
					multiLineStorage = new ArrayList<String>();
				}
				 continue;
	     	}
			String[] split = cleanLine.trim().replaceAll("\'", "").replaceAll(",", "").replaceAll("=", " ").split(del);
			
			String param = split[0].trim();
			if (split.length == 0) {
				continue;
			}
			if (split.length < 2) {
				continue;
			}
			System.out.println(param);
			if (m_Param.containsKey(param)) {
				//TODO: Check what happens when the same parameter is specified twice
				continue ;
			}
			if (param.equals("ATOMIC_POSITIONS") || param.equals("CELL_PARAMETERS")) {
				readMultiLine = true;
				multiLineStorage.add(param);
				multiLineStorage.add(split[1].trim());
				continue;
			}
			
			m_Param.put(param, split[1].trim());
		}
		scanner.close();
		
	}
	
	private void readFile (Reader reader) { //this is a try hard method, would be most efficient if works with the input file.
		Scanner scanner = new Scanner(reader);
		String del = "\\s+";
		boolean readingCoordinates = false;
		boolean readingVectors = false;
		ArrayList<Species> species = new ArrayList<Species>();
		ArrayList<Coordinates> coords = new ArrayList<Coordinates>();
		ArrayList<Vector> latticeVecs = new ArrayList<Vector>();
		while (scanner.hasNextLine()) {
			String currentLine = scanner.nextLine();
			System.currentTimeMillis();
			m_RawInput.add(currentLine);
			m_FileContent.add(currentLine);
			m_ReturnedContent.add(currentLine);
			String cleanLine = stripComments(currentLine);
			if (cleanLine.contains("CELL_PARAMETERS")) {
				System.currentTimeMillis();
			}
			if (cleanLine.length() == 0 || cleanLine == null) {
				if (readingCoordinates) { //TODO: check if coordinates reading are considered done when empty
					readingCoordinates = false;
				} else if (readingVectors) {
					m_Vectors = new Vector[latticeVecs.size()];
					for (int b = 0; b < latticeVecs.size(); b++) {
						m_Vectors[b] = latticeVecs.get(b);
					}
					readingVectors = false;
				}
				 continue;
	     	}
			String[] split = cleanLine.trim().replaceAll("\'", "").replaceAll(",", "").replaceAll("=", " ").split(del);
			System.out.println("---");
			for (int a = 0; a < split.length; a++) {
				System.out.println(split[a]);
			}
			if (split.length == 0) {
				continue;
			}
			String param = split[0].trim();
			if (split.length == 0) {
				continue;
			}
			System.out.println(param);
			if (readingCoordinates) {
				Species spec = Species.get(split[0]);
				species.add(spec);
				double[] coordVals = new double[] {Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3])};
				Coordinates coord = new Coordinates (coordVals, m_CoordinateBasis);
				coords.add(coord);
				continue;
			}else if (readingVectors) {
				double[] coordVals = new double[] {Double.parseDouble(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2])};
				Coordinates vCoor = new Coordinates(coordVals, m_VectorBasis);
				Vector vec = new Vector(vCoor);
				latticeVecs.add(vec);
				continue;
			} else if (m_Param.containsKey(param)) {
				//TODO: Check what happens when the same parameter is specified twice
				continue ;
			}
			//TODO: Decide whether to keep this portion here or move it to a separate method
			//Pro: other flags' effects can be handled normally
			//Con: Slower...
			if (param.equals("ATOMIC_POSITIONS")) {
				readingCoordinates = true;
				if (split[1].trim().toLowerCase().equals("alat")) {
					m_CoordinateBasis = CartesianBasis.getInstance();
					//TODO : should be scaled to either celldm or A (lattice parameter)
					//TODO: Ask Tim technically should be just normal cartesian, so potentially the same as Angstrom
				} else if (split[1].trim().toLowerCase().equals("bohr")) {
					m_CoordinateBasis = new CartesianScaleBasis(m_BohrScaling);
				} else if (split[1].trim().toLowerCase().equals("angstrom")) {
					m_CoordinateBasis = CartesianBasis.getInstance();
				} else if (split[1].trim().toLowerCase().equals("crystal")) {
					m_CoordinateBasis = new LinearBasis(m_Vectors);
				} else if (split[1].trim().toLowerCase().equals("crystal_sg")) {
					//This is going to make things confusing.
					//Need to consider:
					//uniqueb, rhombohedral, and origin_choice... OUCH!
				} else {
					m_CoordinateBasis = new CartesianScaleBasis(m_BohrScaling);
				}
			} else if (param.equals("CELL_PARAMETERS")){
				readingVectors = true;
				if (split[1].trim().toLowerCase().equals("alat")) {
					m_VectorBasis = CartesianBasis.getInstance();
					//TODO: Same as above
				} else if (split[1].trim().toLowerCase().equals("bohr")) {
					m_VectorBasis = new CartesianScaleBasis(m_BohrScaling);
				} else if (split[1].trim().toLowerCase().equals("angstrom")) {
					m_VectorBasis = CartesianBasis.getInstance();
				} else {
					m_VectorBasis = new CartesianScaleBasis(m_BohrScaling);
				}
			}
			if (split.length < 2) {
				continue;
			}
			m_Param.put(param, split[1].trim());
		}
		scanner.close();
		m_Species = new Species[species.size()];
		m_Coordinates = new Coordinates[coords.size()];
		
		for (int a = 0; a < coords.size(); a++) {
			m_Coordinates[a] = coords.get(a);
			m_Species[a] = species.get(a);
		}
		m_NumDefiningSites = m_Coordinates.length;
		System.currentTimeMillis();
	}
	
	private String stripComments(String line) {
		int index = line.indexOf("#");
	    if (index >= 0) {
	      line = line.substring(0, index);
	    }
	    int index2 = line.indexOf("!");
	    if (index2 >= 0) {
	      line = line.substring(0, index2);
	    }
	    return line;
	}
	
	private double[][] getLatticeVectorsDirections (int ibrav) {
		double a = m_VectorLengths[0];
		double b = m_VectorLengths[1];
		double c = m_VectorLengths[2];
		double cosab = Math.cos(m_VectorAngles[0]);
		double cosac = Math.cos(m_VectorAngles[1]);
		double cosbc = Math.cos(m_VectorAngles[2]);
		double sinab = Math.sin(m_VectorAngles[0]);
		double sinac = Math.sin(m_VectorAngles[1]);
		double sinbc = Math.sin(m_VectorAngles[2]);
		double tx = Math.sqrt((1-cosbc)/2);
		double ty = Math.sqrt((1-cosac)/6);
		double tz = Math.sqrt((1+2*cosab)/3);
		if (ibrav == 1) {
			return new double[][] { {1,0,0},{0,1,0},{0,0,1} };
		} else if (ibrav == 2) {
			return new double[][] { {a*1,0,0},{0,a*1,0},{0,0,a*1} };
		} else if (ibrav == 3) {
			double cons = a/2;
			return new double[][] { {cons*-1,0,cons*1},{0,cons*1,cons*1},{cons*-1,cons*1,0} };
		} else if (ibrav == 4) {
			return new double[][] { {a*1,0,0},{-1/2,Math.sqrt(3)/2,0},{0,0,c/a} };
		} else if (ibrav == 5) {
			return new double[][] { {a*tx,a*-ty,a*tz},{0,a*2*ty,a*tz},{a*-tx,a*-ty,a*tz} };
		} else if (ibrav == -5) {
			double aprime = a/Math.sqrt(3);
			double u = tz - 2*Math.sqrt(2)*ty;
			double v = tz + Math.sqrt(2) * ty;
			return new double[][] { {aprime*u,aprime*v,aprime*v},{aprime*v,aprime*u,aprime*v},{aprime*v,aprime*v,aprime*u} };
		} else if (ibrav == 6) {
			return new double[][] { {a,0,0},{0,a,0},{0,0,c} };
		} else if (ibrav == 7) {
			return new double[][] { {a/2,-a/2,c/2},{a/2,a/2,c/2},{-a/2,-a/2,c/2} };
		} else if (ibrav == 8) {
			return new double[][] { {a,0,0},{0,b,0},{0,0,c} };
		} else if (ibrav == 9) {
			return new double[][] { {a/2,b/2,0},{-a/2,b/2,0},{0,0,c} };
		} else if (ibrav == -9) {
			return new double[][] { {a/2,-b/2,0},{a/2,b/2,0},{0,0,c} };
		} else if (ibrav == 10) {
			return new double[][] { {a/2,0,c/2},{a/2,b/2,0},{0,b/2,c/2} };
		} else if (ibrav == 11) {
			return new double[][] { {a/2,b/2,c/2},{-a/2,b/2,c/2},{-a/2,-b/2,c/2} };
		} else if (ibrav == 12) {
			return new double[][] { {a,0,0},{b*cosab,b*sinab,0},{0,0,c} };
		} else if (ibrav == -12) {
			return new double[][] { {a,0,0},{0,b,0},{c*cosac,0,c*sinac} };
		} else if (ibrav == 13) {
			return new double[][] { {a/2,0,-c/2},{b*cosab,b*sinab,0},{a/2,0,c/2} };
		} else if (ibrav == 14) {
			return new double[][] { {a,0,0},{b*cosab,b*sinab,0},{c*cosac,(c*(cosbc-cosac*cosab))/sinab
				,c*Math.sqrt(1 + 2*cosbc*cosac*cosab - (cosbc*cosbc) - (cosac*cosac) - (cosab*cosab)/sinab)} };
		} else {
			return null;
		}
		
		
		
	}
	
  public class Structure implements IStructureData {
	    
	    private Structure() {
	      
	    }

	    public String getDescription() {
	      return QEInputFile.this.getDescription();
	    }

	    public Vector[] getCellVectors() {
	      return QEInputFile.this.getCellVectors();
	    }

	    public boolean[] getVectorPeriodicity() {
	      return QEInputFile.this.getVectorPeriodicity();
	    }

	    public int numDefiningSites() {
	      return QEInputFile.this.numDefiningSites();
	    }

	    public Coordinates getSiteCoords(int index) {
	      return QEInputFile.this.getSiteCoords(index);
	    }

	    public Species getSiteSpecies(int index) {
	      return QEInputFile.this.getSiteSpecies(index);
	    }
	    
	  }
  
public class QEKPoints {
	  
	  private String m_Comment = "Quantum Espresso k-points";
	  private String m_Header = "";
	  private Coordinates[] m_Coordinates;
	  private AbstractBasis m_Basis = CartesianBasis.getInstance(); //For simplicity use Cartesian basis (our default)
	  private double[] m_Weights;
	  private int[][] m_SuperToDirect;
	  private double[] m_Shifts;
	 // private int[][] m_Tetrahedra;
	 // private int[] m_TetrahedraWeights;
	 // private double m_TetrahedraVolumeFraction;
	  
	  
	  public QEKPoints(Coordinates[] coords, double[] weights) {
	    m_Coordinates = (Coordinates[]) ArrayUtils.copyArray(coords);
	    m_Weights = ArrayUtils.copyArray(weights);
	  }
	  
	  
	 /* public QEKPoints(int[][] superToDirect, double[] shifts) {
		  m_SuperToDirect = ArrayUtils.copyArray(superToDirect);
		  m_Shifts = ArrayUtils.copyArray(shifts);
	  }*/ //no such function in QE
	  
	  public QEKPoints(KPointLattice lattice, LinearBasis basis) {
	    m_Coordinates = lattice.getDistinctKPointCoords();
	    m_Weights = lattice.getDistinctKPointWeights();
	    m_Basis = new LinearBasis(CartesianBasis.getInstance().getOrigin(), basis.getBasisToCartesian());
	  }
	  
	  /*public QEKPoints(KPointLattice lattice, BravaisLattice definingLattice) {
		  m_Coordinates = lattice.getDistinctKPointCoords();
		  m_Weights = lattice.getDistinctKPointWeights();
		  //m_Basis = definingLattice.getReciprocalLattice().getLatticeBasis();
		  m_Shifts = lattice.getShiftArray(definingLattice);
		  m_SuperToDirect = lattice.getSuperToDirect(definingLattice);
	  }*/
	  
	  public QEKPoints(KPointLattice lattice) {
	    this(lattice, lattice.getReciprocalSpaceLattice().getLatticeBasis());
	  }
	  
	  public void setHeader (String header) {
		  m_Header = header;
	  }
	  
	  public String getHeader() {
		  return m_Header;
	  }
	  
	  public void setDescription(String comment) {
	    m_Comment = comment;
	  }
	  
	  /*public String setComment() {
	    return m_Comment;
	  }*/
	  
	  public ArrayList<String> toArrayList() {
		  ArrayList<String> list = new ArrayList<String>();
		  list = toStringList();
		  return list;
	  }
	  
	  //this should not be used, but here for debugging purposes
	  public void writeFile(String fileName) {
	    
	    try {
	      FileWriter fileWriter = new FileWriter(fileName);
	      BufferedWriter writer = new BufferedWriter(fileWriter);
	      this.writeCoordinates(writer);
	      writer.flush();
	      writer.close();
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write file " + fileName, e);
	    } 
	  }
	  //same case as this
	  public void writeCoordinates(Writer writer) {
	    try {
	      //writeManual(writer);
	      writeFromList(writer);
	    } catch (IOException e) {
	      throw new RuntimeException("Failed to write kpoints.", e);
	    } 
	  }
	  
	  
	  private void writeFromList(Writer writer) throws IOException {
		  ArrayList<String> list = new ArrayList<String>();
		  list = toStringList();
		  for (int a = 0; a < list.size(); a++) {
			  writer.write(list.get(a));
			  writer.write(System.lineSeparator());
		  }
	  }
	  
	  private ArrayList<String> toStringList() {
		  ArrayList<String> list = new ArrayList<String>();
		  list.add("# " + m_Comment);
		  
		  list.add("K_POINTS crystal");
		  list.add(m_Coordinates.length + "");
		  String formatString = "0.00000000000000";
		  DecimalFormat formatter = new DecimalFormat(formatString);
		    
		  for (int pointNum = 0; pointNum < m_Coordinates.length; pointNum++) {
		    double[] coordArray = m_Coordinates[pointNum].getCoordArray(m_Basis);
		      for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
		        coordArray[dimNum] -= Math.floor(coordArray[dimNum]);
		      }
		      
		    list.add(formatter.format(coordArray[0]) + " " + formatter.format(coordArray[1]) + " "
		    		+ formatter.format(coordArray[2]) + " " +  m_Weights[pointNum]);
		  }
		  return list;
	  }	  
	 /* private void writeTetrahedra(Writer writer) throws IOException {
	    if (m_Tetrahedra == null) {return;}
	    writer.write("Tetrahedra");
	    this.newLine(writer);
	    writer.write(m_Tetrahedra.length + "    " + m_TetrahedraVolumeFraction);
	    this.newLine(writer);
	    for (int tetNum = 0; tetNum < m_Tetrahedra.length; tetNum++) {
	      writer.write(m_TetrahedraWeights[tetNum] + "    ");
	      int[] tet = m_Tetrahedra[tetNum];
	      for (int pointNum = 0; pointNum < tet.length; pointNum++) {
	        writer.write((tet[pointNum]+1) + "   ");
	      }
	      this.newLine(writer);
	    }
	  }*/
	  
	  private void newLine(Writer writer) throws IOException {
	    writer.write("\n");
	  }

}

}