package qe;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractLinearBasis;

/**
 * Based off the latgen.f90 class in Quantum Espresso.
 * Any errors are partially theirs. ;)
 * @author PW
 *
 */

public class QELatticeGenerator {
	
	private int m_Ibrav;
	private double[] m_Celldm = new double[6];
	private double[] m_a1 = new double[3];
	private double[] m_a2 = new double[3];
	private double[] m_a3 = new double[3];
	//private double omega;
	private final double sr2 = 1.414213562373;
	private final double sr3 = 1.732050807569;
	private AbstractLinearBasis m_VectorBasis;
	
	public QELatticeGenerator(int ibrav, double[] celldm) {
		for (int a = 0; a < m_Celldm.length; a++) {
			m_Celldm[a] = celldm[a];
		}
		m_Ibrav = ibrav;
		if(ibrav == 1) { //simple cubic
			m_a1[0] = m_Celldm[0];
			m_a2[1] = m_Celldm[0];
			m_a3[2] = m_Celldm[0];
		} else if (ibrav == 2) { //fcc
			double term = m_Celldm[0]/2.0;
			m_a1[0] = -term;
			m_a1[2] = term;
			m_a2[1] = term;
			m_a2[2] = term;
			m_a3[0] = -term;
			m_a3[1] = term;
		} else if (ibrav == 3) { //bcc
			double term = m_Celldm[0]/2.0;
			for (int a = 0; a < m_a1.length; a++) {
				m_a1[a] = term;
				m_a2[a] = term;
				m_a3[a] = term;
			}
			m_a2[0] = -term;
			m_a3[0] = -term;
			m_a3[1] = -term;
		} else if (ibrav == 4) { //hexagonal
			if (m_Celldm[2] < 0) {
				throw new RuntimeException("Hexagonal lattice had negative number specified as c-axis length.");
			}
			double cbya = m_Celldm[2];
			m_a1[0] = m_Celldm[0];
			m_a2[0] = -m_Celldm[0]/2.0;
			m_a2[1] = m_Celldm[0]*sr3/2.0;
			m_a3[2] = m_Celldm[0]*cbya;
		} else if (Math.abs(ibrav) == 5) { //trigonal
			if (m_Celldm[3] <= -0.5 || m_Celldm[3] >= 1.0) { //TODO: Implement precision, in Fortran it's 1E-15 
				throw new RuntimeException("Invalid celldm(4) value.");
			}
			double term1 = Math.sqrt(1.0 + 2.0 * m_Celldm[3]);
			double term2 = Math.sqrt(1.0 - m_Celldm[3]);
			if (ibrav == 5) {
				m_a2[1] = sr2*m_Celldm[0]*term2/sr3;
				m_a2[2] = m_Celldm[0]*term1/sr3;
				m_a1[0] = m_Celldm[0]*term2/sr2;
				m_a1[1] = -m_a1[0]/sr3;
				m_a1[2] = m_a2[2];
				m_a3[0] = -m_a1[0];
				m_a3[1] = m_a1[1];
				m_a3[2] = m_a2[2];
			} else if (ibrav == -5) {
				m_a1[0] = m_Celldm[0]*(term1-2.0*term2)/3.0;
				m_a1[1] = m_Celldm[0]*(term1+term2)/3.0;
				m_a1[2] = m_a1[1];
				m_a2[0] = m_a1[2];
				m_a2[1] = m_a1[0];
				m_a2[2] = m_a1[1];
				m_a3[0] = m_a1[1];
				m_a3[1] = m_a1[2];
				m_a3[2] = m_a1[0];
			}
		} else if (ibrav == 6) { //tetragonal
			if (m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			double cbya = m_Celldm[2];
			m_a1[0] = m_Celldm[0];
			m_a2[1] = m_Celldm[0];
			m_a3[2] = m_Celldm[0] * cbya;
		} else if (ibrav == 7) { //body centered tetragonal
			if (m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			double cbya = m_Celldm[2];
			m_a2[0] = m_Celldm[0]/2.0;
			m_a2[1] = m_a2[0];
			m_a2[2] = cbya*m_Celldm[0]/2.0;
			m_a1[0] = m_a2[0];
			m_a1[1] = -m_a2[0];
			m_a1[2] = m_a2[2];
			m_a3[0] = -m_a2[0];
			m_a3[1] = -m_a2[0];
			m_a3[2] = m_a2[2];
		} else if (ibrav == 8) { //orthorombic
			if (m_Celldm[1] <= 0.0) {
				throw new RuntimeException("Invalid celldm(2) value.");
			}
			if ( m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			m_a1[0] = m_Celldm[0];
			m_a2[1] = m_Celldm[0] * m_Celldm[1];
			m_a3[2] = m_Celldm[0] * m_Celldm[2];
		} else if (Math.abs(ibrav) == 9) { //single face centered orthorombic
			if (m_Celldm[1] <= 0.0) {
				throw new RuntimeException("Invalid celldm(2) value.");
			}
			if ( m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			if (ibrav == 9) { //old pwscf description
				m_a1[0] = 0.5 * m_Celldm[0];
				m_a1[1] = m_a1[0] * m_Celldm[1];
				m_a2[0] = -m_a1[0];
				m_a2[1] = m_a1[1];
			} else { //alternate description //PW: Why can't you just stick with ibrav0 and have the users git good at it????
				m_a1[0] = 0.5 * m_Celldm[0];
				m_a1[1] = -m_a1[0] * m_Celldm[1];
				m_a2[0] = m_a1[0];
				m_a2[1] = -m_a1[1];
			}
			m_a3[2] = m_Celldm[0] * m_Celldm[2];
		} else if (ibrav == 10) { //face centered orthorombic 
			if (m_Celldm[1] <= 0.0) {
				throw new RuntimeException("Invalid celldm(2) value.");
			}
			if ( m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			m_a2[0] = 0.5 * m_Celldm[0];
			m_a2[1] = m_a2[0] * m_Celldm[1];
			m_a1[0] = m_a2[0];
			m_a1[2] = m_a2[0] * m_Celldm[2];
			m_a3[1] = m_a2[0] * m_Celldm[1];
			m_a3[2] = m_a1[2];
		} else if (ibrav == 11) { //body centered orthorombic
			if (m_Celldm[1] <= 0.0) {
				throw new RuntimeException("Invalid celldm(2) value.");
			}
			if ( m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			m_a1[0] = 0.5 * m_Celldm[0];
			m_a1[1] = m_a1[0] * m_Celldm[1];
			m_a1[2] = m_a1[0] * m_Celldm[2];
			m_a2[0] = -m_a1[0];
			m_a2[1] = m_a1[1];
			m_a2[2] = m_a1[2];
			m_a3[0] = - m_a1[0];
			m_a3[1] = -m_a1[1];
			m_a3[2] = m_a1[2];
		} else if (ibrav == 12) { //monoclinic with unique c-axis 
			if (m_Celldm[1] <= 0.0 ) {
				throw new RuntimeException("Invalid celldm(2) value.");
			}
			if (m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			if (Math.abs(m_Celldm[3]) >= 1.0) {
				throw new RuntimeException("Invalid celldm(4) value.");
			}
			double sen = Math.sqrt(1.0 - Math.pow(m_Celldm[3],2));
			m_a1[0] = m_Celldm[0];
			m_a2[0] = m_Celldm[0] * m_Celldm[1] * m_Celldm[3];
			m_a2[1] = m_Celldm[0] * m_Celldm[1] * sen;
			m_a3[2] = m_Celldm[0] * m_Celldm[2];
		} else if (ibrav == -12) { //monoclinic with unique b-axis
			if (m_Celldm[1] <= 0.0 ) {
				throw new RuntimeException("Invalid celldm(2) value.");
			}
			if (m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			if (Math.abs(m_Celldm[4]) >= 1.0) {
				throw new RuntimeException("Invalid celldm(5) value.");
			}
			double sen=Math.sqrt(1.0 - Math.pow(m_Celldm[4],2));
			m_a1[0] = m_Celldm[0];
			m_a2[1] = m_Celldm[0] * m_Celldm[1];
			m_a3[0] = m_Celldm[0] * m_Celldm[2] * m_Celldm[4];
			m_a3[2] = m_Celldm[0] * m_Celldm[2] * sen;
		} else if (ibrav == 13) { //single face centered monoclinic
			if (m_Celldm[1] <= 0.0 ) {
				throw new RuntimeException("Invalid celldm(2) value.");
			}
			if (m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			if (Math.abs(m_Celldm[3]) >= 1.0) {
				throw new RuntimeException("Invalid celldm(4) value.");
			}
			double sen = Math.sqrt(1.0 - Math.pow(m_Celldm[3],2));
			m_a1[0] = 0.5 * m_Celldm[0]; 
			m_a1[2] = -m_a1[0] * m_Celldm[2];
			m_a2[0] = m_Celldm[0] * m_Celldm[1] * m_Celldm[3];
			m_a2[1] = m_Celldm[0] * m_Celldm[1] * sen;
			m_a3[0] = m_a1[0];
			m_a3[2] = -m_a1[2];
		} else if (ibrav == 14) { //triclinic
			if (m_Celldm[1] <= 0.0) {
				throw new RuntimeException("Invalid celldm(2) value.");
			} 
			if (m_Celldm[2] <= 0.0) {
				throw new RuntimeException("Invalid celldm(3) value.");
			}
			if (Math.abs(m_Celldm[3]) >= 1.0) {
				throw new RuntimeException("Invalid celldm(4) value.");
			} 
			if (Math.abs(m_Celldm[4]) >= 1.0) {
				throw new RuntimeException("Invalid celldm(5) value.");
			} 
			if (Math.abs(m_Celldm[5]) >= 1.0) {
				throw new RuntimeException("Invalid celldm(6) value.");
			}
			double singam = Math.sqrt(1.0 - Math.pow(m_Celldm[5],2));
			double term = (1.0 + 2.0 * m_Celldm[3] * m_Celldm[4] * m_Celldm[5]
					- Math.pow(m_Celldm[3],2) - Math.pow(m_Celldm[4],2) - Math.pow(m_Celldm[5],2));
			if (term < 0.0) {
				throw new RuntimeException("celldm values do not make sense, please check them.");
			}
			term = Math.sqrt(term/(1.0 - Math.pow(m_Celldm[5],2)));
			m_a1[0] = m_Celldm[0];
			m_a2[0] = m_Celldm[0] * m_Celldm[1] * m_Celldm[5];
			m_a2[1] = m_Celldm[0] * m_Celldm[1] * singam;
			m_a3[0] = m_Celldm[0] * m_Celldm[2] * m_Celldm[4];
			m_a3[1] = m_Celldm[0] * m_Celldm[2] * (m_Celldm[3] - m_Celldm[4] * m_Celldm[5])/singam;
			m_a3[2] = m_Celldm[0] * m_Celldm[2] * term;
		} else {
			throw new RuntimeException("Bravais lattice specified does not exist, please check your ibrav value.");
		}
		
	}
	/*//This will currently throw an error because basis was never set
	public Vector[] getLatticeVectors() { 
		AbstractLinearBasis basis = m_VectorBasis;
		return getLatticeVectors(basis);
	}*/
	
	public Vector[] getLatticeVectors(AbstractLinearBasis basis) {
		Coordinates vCoor1 = new Coordinates(m_a1, basis);
		Vector vec1 = new Vector(vCoor1);
		Coordinates vCoor2 = new Coordinates(m_a2, basis);
		Vector vec2 = new Vector(vCoor2);
		Coordinates vCoor3 = new Coordinates(m_a3, basis);
		Vector vec3 = new Vector(vCoor3);
		Vector[] vectors = new Vector[] {vec1, vec2, vec3};
		return java.util.Arrays.copyOf(vectors,vectors.length);
	}


}
