package qe;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

//import org.apache.commons.fileupload.FileItem;
//import org.json.JSONException;

import generators.KPointLatticeCollection;
import generators.KPointLibrary;
import generators.LowSymmetryGenerator;
import kptServer.INCAR;
import kptServer.ServerInput;
//import kptServer.ServerInput;
import matsci.Species;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.Structure;
import matsci.structure.reciprocal.KPointLattice;
import matsci.structure.symmetry.SymmetryException;
import matsci.structure.superstructure.SuperLattice;
import utils.TextFileParser;

public class TestQEKPointGeneration {

	private static KPointLibrary library;
	public static void main(String[] args) throws NullPointerException, IOException {
		//CartesianBasis.setPrecision(3E-6);
		//CartesianBasis.setAngleToleranceDegrees(1E-3);
		CartesianBasis.setPrecision(3E-6);
		CartesianBasis.setAngleToleranceDegrees(1E-3);
		String workingDir = args[0];
		String fileLoc = args[1];
		//String poscarLoc = fileLoc + "/input.qe";
		String precalcLoc = fileLoc + "/PRECALC";
		//String kpointsLoc = fileLoc + "/input.qe.updated";*/
		int[] arithNums = new int[] {1, 2, 7, 8, 18, 19, 20, 21, 26, 27, 36, 37, 40, 41, 48, 49, 50, 53, 58, 62, 63, 64, 71, 72, 73};
	    String collectionDirectory=workingDir + "/minDistanceCollections";
	    
	    
	    library = new KPointLibrary();
	     
	      
	    for (int a = 0; a < arithNums.length; a++) {
	     	int maxNumKpts = 5832;
	    	SpaceGroup space = SpaceGroup.getSymmorphicSpaceGroup(3, arithNums[a]);
	  		int numDim = 3;
			int arithNum = arithNums[a];
			if (arithNum < 9) {
				long genStart = System.currentTimeMillis();
				LowSymmetryGenerator gammaGenerator = new LowSymmetryGenerator (729, true, false, false);
				LowSymmetryGenerator shiftedGenerator = new LowSymmetryGenerator (729, false, false, false);
				LowSymmetryGenerator diagonalGammaGenerator = new LowSymmetryGenerator (729, true, false, true);
				LowSymmetryGenerator diagonalShiftedGenerator = new LowSymmetryGenerator (729, false, false, true);
				long genFin = System.currentTimeMillis();
				library.setFactory(arithNum, numDim, true, gammaGenerator);
				library.setFactory(arithNum, numDim, false, shiftedGenerator);
				System.out.println("Generating lattice for low symmetry group " + arithNum + " takes " + (genFin - genStart) + " ms." );
			} else {
				long readStart = System.currentTimeMillis();
				KPointLatticeCollection gammaCollLoad = null;
				KPointLatticeCollection shiftedCollLoad = null;
				KPointLatticeCollection diagonalGammaLoad = null;
				KPointLatticeCollection diagonalShiftedLoad = null;
				try {
					gammaCollLoad = loadCollection(collectionDirectory, space, numDim, maxNumKpts, true);
					shiftedCollLoad = loadCollection(collectionDirectory, space, numDim, maxNumKpts, false);
				} 
				/*
				catch (JSONException e) {
				
					System.err.println("WARNING!! There is a problem with the JSON format");
					e.printStackTrace();
				}*/
				catch (IOException e) {
					System.err.println("File not found, please check the supplied path.");
					e.printStackTrace();
				}
				
				long readEnd = System.currentTimeMillis();
				System.out.println("Reading the KPointLatticeCollection took: " + (readEnd - readStart) + " ms.");
				library.setFactory(arithNum, numDim, true, gammaCollLoad);
				library.setFactory(arithNum, numDim, false, shiftedCollLoad);
				
			}
	      }
	    ServerInput inputParams = new ServerInput();
	    File precalc = new File(precalcLoc);
	    try {
			InputStream inStream = new FileInputStream(precalc);
			handleInputFile(inStream, inputParams);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		String[] allFiles = listFilesInFolder(fileLoc);
		for (int a = 0; a < allFiles.length; a++) {
			if (allFiles[a].equals("PRECALC")) {
				continue;
			}
			if (!allFiles[a].equals("input.qe")) {
				continue;
			}
			//QEInputFile qeI = new QEInputFile(path + "/" + allFiles[a]);
			System.out.println("Now processing: " + fileLoc + "/" + allFiles[a]);
			QENamelists qeI = null;
			try {
				qeI = new QENamelists(fileLoc + "/" + allFiles[a]);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (qeI == null) {
				System.out.println("Cannot process: " + fileLoc + "/" + allFiles[a]);
				return;
			}
			inputParams.putQEData(qeI);
			Structure struct = new Structure(qeI);
			
			QENamelists out = appendQEWithKPoints(inputParams, struct, fileLoc);
			out.writeReturnedContent(fileLoc + "/" + allFiles[a] + ".updated");
			
			Structure stru = new Structure(qeI);
			POSCAR poscar = new POSCAR(stru);
			poscar.writeFile(fileLoc + "/" + allFiles[a] + ".vasp");
		}
	    
	}
	
	  public static KPointLattice generateKPointLattice(ServerInput inputParams, Structure struct, KPointLibrary.INCLUDE_GAMMA includeGamma) {
		    //SpaceGroup space = struct.getDefiningSpaceGroup().getSymmorphicGroup(true);
		    SpaceGroup space = struct.getDefiningSpaceGroup().getSymmorphicGroup();
		    //boolean forceDiagonal = inputParams.isDiagonalGrid();
		    double minDistance = inputParams.getMinDistance();
		    int minTotalPoints = inputParams.getMinTotalPoints();
		    /*if (inputParams.gapAdjusted()) {
		    	minTotalPoints = inputParams.getGapReducedMinTotalPoints();
		    }*/
		    boolean useMinTotalPoints = inputParams.minTotalPointsUsed();
		    boolean useDefaultMinDistance = inputParams.defaultMinDistance();
		    String removeSymmetrySetting = inputParams.getGridRemoveSymmetrySettingUsed();
		    KPointLibrary.SYMMETRY_TYPE sType = KPointLibrary.SYMMETRY_TYPE.ALL;
		    //boolean ignoreSymmetry = inputParams.symmetrySetting();
		    if (removeSymmetrySetting.equalsIgnoreCase("structural")) {
		    	sType = KPointLibrary.SYMMETRY_TYPE.TIME_REVERSAL_ONLY;
		    } else if (removeSymmetrySetting.equalsIgnoreCase("all")) {
		    	sType = KPointLibrary.SYMMETRY_TYPE.NONE;
		    } else if (removeSymmetrySetting.equalsIgnoreCase("time_reversal")) {
		    	sType = KPointLibrary.SYMMETRY_TYPE.STRUCTURE_ONLY;
		    }
		    //completeIgnoreSym is turning off time reversal symmetry which is ISYM=-1 in VASP
		    //KPointLibrary.SYMMETRY_TYPE ignoreTimeReversal = inputParams.symmetrySetting() && inputParams.ignoreSymmetryWarning() ? KPointLibrary.SYMMETRY_TYPE.NONE : KPointLibrary.SYMMETRY_TYPE.TIME_REVERSAL_ONLY;
			int numDim = struct.getDefiningLattice().numPeriodicVectors();
			int arithNum = space.getArithmeticCrystalClassNumber(numDim);
			System.out.println("The arithmetic number for " + struct.getDescription() + " is " + arithNum);
			
			KPointLattice kLat = null; //If this stays null, then total number of points > larger what we can do.
			if(useMinTotalPoints && !useDefaultMinDistance) {//Both minDistance and minTotalPoints are specified
			
				kLat = library.getKPointLattice(struct, includeGamma, minDistance, minTotalPoints, sType);
			}
			else if(useMinTotalPoints && useDefaultMinDistance) {//Only minTotalPoints
				
				minDistance = 0.0;
				kLat = library.getKPointLattice(struct, includeGamma, minDistance, minTotalPoints, sType);
			}
			else if(!useMinTotalPoints && !useDefaultMinDistance) {//Only minDistance
				
				minTotalPoints = 1;
				kLat = library.getKPointLattice(struct, includeGamma, minDistance, minTotalPoints, sType);
			}
			else {
				minTotalPoints = 1;
				kLat = library.getKPointLattice(struct, includeGamma, minDistance, minTotalPoints, sType);
			}		
			if (kLat == null) {
				return null;
			}
			return kLat;
	  }
	  

	  public static String setUpKPOINTSHeader(KPointLattice kLat, Structure struct, ServerInput inputParams, KPointLibrary.INCLUDE_GAMMA includeGamma) {
		  double storeMinPerD = kLat.getRealSuperLattice().getMinPeriodicDistance(); //this is minPeriodicDistance from real lattice (reciprocal of reciprocal)
			double storeKSpa = kLat.getKPointLattice().getMinPeriodicDistance();
			SuperLattice sla = kLat.getReciprocalSpaceLattice();
			double compactKVec = sla.getPrimLattice().getLongestCompactVectorLength();
			double compactRVec = kLat.getRealSuperLattice().getPrimLattice().getLongestCompactVectorLength();
			String composition = struct.getCompositionString();
			boolean includesGammaPoint = kLat.includesGammaPoint();
		    boolean forceDiagonal = inputParams.isDiagonalGrid();
		    double minDistance = inputParams.getMinDistance();
		    INCAR incar = inputParams.getINCAR();
		    //boolean ignoreSymmetry = inputParams.symmetrySetting();
		    //int symSetting = inputParams.symmetrySettingVASP();
		    String reducedDim = standardUserOutputFormat("Actual minimum periodic distance is " + storeMinPerD + " Angstroms.");
		    if (inputParams.getRealDimension() == 2) {
		    	reducedDim = standardUserOutputFormat("Slab structure detected. " + reducedDim);
		    } else if (inputParams.getRealDimension() == 1) {
		    	reducedDim = standardUserOutputFormat("Wire structure detected. " + reducedDim);
		    } else if (inputParams.getRealDimension() == 0) {
		    	reducedDim = standardUserOutputFormat("Isolated particle detected.");
		    }

			String gammaComment = standardUserOutputFormat("Grid does not include gamma point.");
			if (includesGammaPoint){
				gammaComment = standardUserOutputFormat("Grid includes gamma point.");
			}
			String forceD = "";
			String forceDiagonalWarning = "";
			if (forceDiagonal) {
				forceD = " FORCEDIAGONAL=" + forceDiagonal;
				forceDiagonalWarning = standardUserOutputFormat("Warning: FORCEDIAGONAL is found to not generate fully diagonal grids and has been deprecated. Generating generalized grid...");
			}
			String gapLine = "";
			if (inputParams.gapAdjusted()){
				gapLine = " GAPDISTANCE=" + inputParams.gapDistance();// + " REDUCEDMINTOTALKPOINTS=" + inputParams.getMinTotalPoints();//inputParams.getGapReducedMinTotalPoints();
			}
			String fails = "Unable to properly parse and default values are used for: ";
			if (inputParams.unparseableList().size() == 0) {
				fails = "";
			} else {
				for (int i = 0; i < inputParams.unparseableList().size()-1; i++) {
					fails = fails + inputParams.unparseableList().get(i) + " ";
				}
				fails = standardUserOutputFormat(fails + inputParams.unparseableList().get(inputParams.unparseableList().size()-1) + ".");
			}
			String unknowns = "Cannot recognize these input parameters: ";
			if (inputParams.unrecognizeableList().size() == 0) {
				unknowns = "";
			} else {
				for (int i = 0; i < inputParams.unrecognizeableList().size()-1; i++) {
					unknowns = unknowns + inputParams.unrecognizeableList().get(i) + " ";
				}
				unknowns = standardUserOutputFormat(unknowns + inputParams.unrecognizeableList().get(inputParams.unrecognizeableList().size()-1) + ".");
			}
			String symFlag = inputParams.getGridRemoveSymmetrySetting();
			String sym = "";
			if (symFlag.equalsIgnoreCase("all")) {
				sym = standardUserOutputFormat("Generating grid without structural and time-reversal symmetry.");
			} else if (symFlag.equalsIgnoreCase("structural")){
				sym = standardUserOutputFormat("Generating grid without structural symmetry.");	
			} else if (symFlag.equalsIgnoreCase("time_reversal")) {
				sym = standardUserOutputFormat("Generating grid without time-reversal symmetry.");
			}
			/*String lsOrbitWarning = "";
			if (inputParams.useSpinOrbitCoupling() && symSetting != -1) {
				lsOrbitWarning = lineMessageOutputFormat("Warning: When LSORBIT=TRUE, VASP recommends using ISYM=-1.");
			}*/
			String ver = standardUserOutputFormat("Server version: NONE .");
			String selectiveDynamics = "";
			if (inputParams.usingSelectiveDynamics()) {
				selectiveDynamics = standardUserOutputFormat("Selective dynamics was detected and taken into account.");
			}
			String gapWarning = "";
			if (inputParams.gapDistance() == 0) {
				gapWarning = standardUserOutputFormat("Warning, GAPDISTANCE=0 will treat all atoms as isolated particles.");
			}
			String sizeWarning = "";
			if (!inputParams.isMinDistProvided() && !inputParams.isMinTotProvided() && !inputParams.isKPPRAProvided()) {
				sizeWarning = standardUserOutputFormat("Warning: Grid size was not prompted, using default grid size.");
			}
			String oldVersion = "";
			/*if (isVersionOlder("C2016.06.06", inputParams.getClientVersion()) && !inputParams.isINCARIncluded()) {
				oldVersion = standardUserOutputFormat("Warning: INCAR file not detected. If you are using getKPoints script that is older than C2016.06.06, please consider downloading the latest script from http://muellergroup.jhu.edu/K-Points.html so that the grid can be adjusted according to your INCAR values.");
			}*/
			String magmom = "";
			String nonCollinear = "";
			String nonCollDet = "";
			String nonCollISYM0Warning = "";
			String charg = "";
			if (incar != null) {
				if (incar.ISPIN() == 2 && incar.MAGMOMString() != null && !incar.LNONCOLLINEAR()) {
					magmom = standardUserOutputFormat("ISPIN=2 and MAGMOM detected in INCAR. Reducing grid symmetry accordingly.");
				}
				/*if (incar.LSORBIT()) {
					if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()) {
						if (incar.ISYM() == 0) {
							nonCollinear = lineMessageOutputFormat("Warning: Using a grid with time-reversal symmetry for a spin orbit calculation is not recommended.");
						} else if (incar.ISYM() > 0 ) {
							nonCollinear = lineMessageOutputFormat("Warning: Using a high symmetry grid for a spin orbit calculation is not recommended. We currently do not generate high symmetry grids for spin orbit calculation. Using only time-reversal symmetry to generate grid.");
						}
					} else {
						nonCollinear = lineMessageOutputFormat("Spin orbit calculation detected.");
					}
				}*/
				if (incar.LNONCOLLINEAR() /*&& !incar.LSORBIT()*/) {
					nonCollDet = standardUserOutputFormat("Non-collinear magnetic calculation detected.");
					if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()) {
						if (incar.ISYM() > 0) {
							nonCollISYM0Warning = standardUserOutputFormat("Warning: Using a grid with time-reversal symmetry for a noncollinear magnetic calculation is not recommended. Consider setting ISYM=-1 in your INCAR file.");
							nonCollinear = standardUserOutputFormat("Warning: Structural symmetry is currently ignored for non-collinear magnetic calculations.");
						} else if (incar.ISYM() == 0) {
							nonCollinear = standardUserOutputFormat("Warning: Using a grid with time-reversal symmetry for a noncollinear magnetic calculation is not recommended.");
						}
					} else if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("time_reversal")){
						if (incar.ISYM() > -1) {
							nonCollinear = standardUserOutputFormat("Warning: User removed time-reversal symmetry. Since structural symmetry is currently ignored for non-collinear magnetic calculations grid will be generated without any symmetry.");
							sym = standardUserOutputFormat("Generating grid without structural and time-reversal symmetry.");
						}
					} else {
						//nonCollinear = standardUserOutputFormat("Non-collinear calculation detected.");
					}
				}
				if (inputParams.getGridRemoveSymmetrySetting().equalsIgnoreCase("none") || !inputParams.gridRemoveSymmetrySettingProvided()){
					if (incar.ISYM() > 0) {
						if (incar.ISPIN() == 2 || incar.LNONCOLLINEAR()) {
							if (incar.ICHARG() == 1 || incar.ICHARG() == 11) {
								charg = standardUserOutputFormat("Warning: ICHARG="+ incar.ICHARG() +" and ISPIN=2. Magnetic moments in CHGCAR file can break symmetry and this is not taken into account during grid generation. Please consider setting REMOVE_SYMMETRY=ALL or REMOVE_SYMMETRY=STRUCTURAL in your PRECALC file.");
							}
						}
					}
				}
			}
			/*String spinOrbit = "";
			if (inputParams.useSpinOrbitCoupling() && inputParams.useSpinOrbitCouplingTriggered()) {
				spinOrbit = standardUserOutputFormat("Spin orbit coupling detected. Generating grid without symmetry.");
			}
			String nonCollWarning = "";
			if (inputParams.useNonCollinear() && inputParams.useNonCollinearTriggered() && !inputParams.useSpinOrbitCoupling() && inputParams.LNoncollinearWithoutReducedSymmetry()) {
				nonCollWarning = standardUserOutputFormat("Warning: Non-collinear calculation with symmetry detected in INCAR. We currently do not return these grids and they are not recommended by VASP. Using only time-reversal symmetry in generating grid.");
			}
			String spinOrbitWarning = "";
			if (inputParams.useSpinOrbitCoupling() && inputParams.useSpinOrbitCouplingTriggered() && inputParams.LSorbitWithoutSymmetryOff()) {
				spinOrbitWarning = standardUserOutputFormat("Warning: Spin orbit coupling with symmetry detected in INCAR. We currently do not return these grids and they are not recommended by VASP. No symmetry is used in generating grid.");
			}*/
			//String warningOn4_25=standardUserOutputFormat("There was a bug in version 2016.04.25 that can lead to incorrect k-point weights. Please replace grids that were made with version 2016.04.25. We apologize for the inconvenience.");
			if (!inputParams.isDefault()) {
				if (inputParams.headerVerbosity().equals("VERBOSE")) {
					String comment = ver 
							//+ warningOn4_25
							+ oldVersion
							+ forceDiagonalWarning
							//+ spinOrbit
							//+ spinOrbitWarning
							+ nonCollDet
							+ nonCollinear
							+ nonCollISYM0Warning
							//+ nonCollWarning
							+ magmom
							+ charg
							+ selectiveDynamics 
							+ sym 
							//+ lsOrbitWarning
							+ fails 
							+ unknowns 
							+ sizeWarning 
							+ gapWarning 
							+ "K-point grid has " + kLat.numTotalKPoints() + " total points and " + kLat.numDistinctKPoints() + " distinct points. " 
							+ reducedDim
							+ gammaComment 
							+ "Parameters used: MINDISTANCE=" + minDistance + " INCLUDEGAMMA=" + includeGamma 
							+ forceD
							+ " MINTOTALKPOINTS=" + inputParams.getMinTotalPoints() 
							+ " KPPRA=" + inputParams.getKPPRA() 
							+ gapLine;
					return comment;
				} if (inputParams.headerVerbosity().equals("DEBUG") && inputParams.isBetaMode()) {
					String comment = ver 
							//+ warningOn4_25 
							+ oldVersion
							+ forceDiagonalWarning
							//+ spinOrbit
							//+ spinOrbitWarning
							+ nonCollDet
							+ nonCollinear
							+ nonCollISYM0Warning
							//+ nonCollWarning
							+ magmom
							+ charg
							+ selectiveDynamics 
							+ sym 
							//+ lsOrbitWarning
							+ fails 
							+ unknowns 
							+ sizeWarning 
							+ gapWarning 
							+ "||BETA_MODE|| K-points for " + composition + " with MINDISTANCE=" + minDistance + " and INCLUDEGAMMA=" + includeGamma + " FORCEDIAGONAL=" + forceDiagonal + " has " + kLat.numTotalKPoints() + " total points and " + kLat.numDistinctKPoints() + " distinct points. " 
							+ reducedDim + "Or in reciprocal space: " + storeKSpa + " 1/Angstroms, has length of compact k-vector: " + compactKVec + " 1/Angstroms, and length of compact r-vector: " + compactRVec + " Angstroms.";
					return comment;
				} if (inputParams.isBetaMode()) {
					String comment = ver 
							//+ warningOn4_25
							+ oldVersion
							+ forceDiagonalWarning
							//+ spinOrbit
							//+ spinOrbitWarning
							+ nonCollDet
							+ nonCollinear
							+ nonCollISYM0Warning
							//+ nonCollWarning
							+ magmom
							+ charg
							+ selectiveDynamics 
							+ sym 
							//+ lsOrbitWarning
							+ fails 
							+ unknowns 
							+ sizeWarning 
							+ gapWarning 
							+ "||BETA_MODE|| K-points for " + composition + " with MINDISTANCE=" + minDistance + " Angstroms and INCLUDEGAMMA=" + includeGamma + " FORCEDIAGONAL=" + forceDiagonal + " has " + kLat.numTotalKPoints() + " total points and " + kLat.numDistinctKPoints() + " distinct points. " 
							+ reducedDim;
					return comment;
				} else {
					//String comment = "K-points for " + composition + " with MINDISTANCE=" + minDistance + " Angstroms and INCLUDEGAMMA=" + includeGamma + " FORCEDIAGONAL=" + forceDiagonal + " has " + kLat.numTotalKPoints() + " total k-points. Effective minimum distance: " + storeMinPerD + " Angstroms.";
					String comment = ver 
							//+ warningOn4_25
							+ oldVersion
							+ forceDiagonalWarning
							//+ spinOrbit
							//+ spinOrbitWarning
							+ nonCollDet
							+ nonCollinear
							+ nonCollISYM0Warning
							//+ nonCollWarning
							+ magmom
							+ charg
							+ selectiveDynamics 
							+ sym 
							//+ lsOrbitWarning
							+ fails 
							+ unknowns 
							+ sizeWarning 
							+ gapWarning 
							+ "K-point grid has " + kLat.numTotalKPoints() +
							" total points and " + kLat.numDistinctKPoints() + " distinct points. " 
							+ reducedDim 
							+ gammaComment;
					return comment;
				}
			} else {
				String comment = ver 
						//+ warningOn4_25
						+ oldVersion
						+ forceDiagonalWarning
						//+ spinOrbit
						//+ spinOrbitWarning
						+ nonCollDet
						+ nonCollinear
						+ nonCollISYM0Warning
						//+ nonCollWarning
						+ magmom
						+ charg
						+ selectiveDynamics 
						+ sym 
						//+ lsOrbitWarning
						+ fails 
						+ unknowns 
						+ sizeWarning 
						+ gapWarning 
						+ "K-point grid was generated using default values. Grid has " + kLat.numTotalKPoints() +
						" total points and " + kLat.numDistinctKPoints() + " distinct points. " 
						+ reducedDim 
						+ gammaComment;
				return comment;
			}
	  }
	  
	  public static boolean handleInputFile(InputStream inStream, ServerInput inputParams) throws IOException, NullPointerException {
     	 Reader read = new InputStreamReader(inStream);
     	 //TODO: Move the part below to ServerInput class constructor
     	 BufferedReader reader = new BufferedReader(read); //read directly from inputStream
     	 String currentLine;
     	 ArrayList<String> failToParse = new ArrayList<>();
     	 ArrayList<String> unknownParams = new ArrayList<>();
     	 String del = "[=]";
		 while ((currentLine = reader.readLine()) != null) {
	   		 /*if (currentLine.equals("#")|| currentLine.equals("!")) {
	   			 continue;
	   		 }*/
	   		 //String cleanLine = currentLine.split("[#,!]")[0];
			 String cleanLine = stripComments(currentLine);
			 if (cleanLine.length() == 0 || cleanLine == null) {
				 continue;
	     	 }
			 String[] split = cleanLine.split(del);
			 String param = split[0].trim();
			 if (param.length() == 0) {
				 continue;
			 }
			 if (split.length < 2) {
				 if (param.equals("INCLUDEGAMMA") ||
						 param.equals("MINDISTANCE") ||
						 param.equals("MINTOTALKPOINTS") ||
						 param.equals("HEADER") ||
						 param.equals("FORCEDIAGONAL") ||
						 param.equals("GAPDISTANCE") ||
						 param.equals("KPPRA") ||
						 param.equals("REMOVE_SYMMETRY")||
						 param.equals("BETA_MODE") ||
						 param.equals("PRINT_SUPERTODIRECT") ||
						 param.equals("OPTI_DIAG")) {
					 failToParse.add(param);
					 continue;
				 } else if (!param.equals("BETA_MODE") ||
						 !param.equals("OPTI_DIAG") ||
						 !param.equals("PRINT_SUPERTODIRECT")){
					 unknownParams.add(param);
					 continue;
				 }
			 }
			 String value = split[1].trim();
			 if (param.equals("INCLUDEGAMMA")) {
	   			if (value.equalsIgnoreCase("TRUE")){
	  				 inputParams.setShift("TRUE");
	  				 continue;
	   			} else if (value.equalsIgnoreCase("FALSE")) {
	   				 inputParams.setShift("FALSE");
	   				 continue;
	   			} else if (value.equalsIgnoreCase("AUTO")){
	   				inputParams.setShift("AUTO");
	   				continue;
	   			} else {
	   				 failToParse.add("INCLUDEGAMMA");
	   				 continue;
	   			}
			 }
			 if (param.equals("MINDISTANCE")) {
				 inputParams.promptMinDist(true);
				 Double val = TextFileParser.parseDoubleValue(value);
				 if (val == null || Double.isInfinite(val) || Double.isNaN(val)) {
					 failToParse.add("MINDISTANCE");
					 continue;
				 }
				 inputParams.setMinDistance(val);
				 continue;
			 }
			 if (param.equals("MINTOTALKPOINTS")) {
				 inputParams.promptMinTot(true);
				 Integer val = TextFileParser.parseIntegerValue(value);
				 if (val == null) {
					 failToParse.add("MINTOTALKPOINTS");
		   			 continue; 
				 }
				 inputParams.setMinTotalPoints(val);
				 continue;
			 }
			 if (param.equals("HEADER")) {
				 if (value.equalsIgnoreCase("VERBOSE")){
					 inputParams.setHeaderVerbosity("VERBOSE");
					 continue;
				 } else if (value.equalsIgnoreCase("DEBUG")){
					 inputParams.setHeaderVerbosity("DEBUG");
					 continue;
				 } else if (value.equalsIgnoreCase("SIMPLE")){
					 inputParams.setHeaderVerbosity("SIMPLE");
					 continue;
			    } else {
			    	failToParse.add("HEADER");
			    	continue;
			    }
			 }
			 if (param.equals("REMOVE_SYMMETRY")) {
				 if (value.equalsIgnoreCase("ALL")) {//EDIT ACCORDINGLY
					 inputParams.setGridRemoveSymmetrySetting("all");
					 inputParams.setGridRemoveSymmetrySettingUsed("all");
					 inputParams.setGridIgnoreSymmetryProvided(true);
					 continue;
				 } else if (value.equalsIgnoreCase("STRUCTURAL")) {
					 inputParams.setGridRemoveSymmetrySetting("structural");
					 inputParams.setGridRemoveSymmetrySettingUsed("structural");
					 inputParams.setGridIgnoreSymmetryProvided(true);
					 continue;
				 } else if (value.equalsIgnoreCase("TIME_REVERSAL")) {
					 inputParams.setGridRemoveSymmetrySetting("time_reversal");
					 inputParams.setGridRemoveSymmetrySettingUsed("time_reversal");
					 inputParams.setGridIgnoreSymmetryProvided(true);
					 continue;
				 } else if (value.equalsIgnoreCase("NONE")) { //includes as much symmetry as possible
					 inputParams.setGridRemoveSymmetrySetting("none");
					 inputParams.setGridIgnoreSymmetryProvided(true);
					 continue;
				 } else { 
					 inputParams.setGridRemoveSymmetrySetting("none");
					 continue;
				 }
			 }
			 /*if (param.equals("FORCEDIAGONAL")) {
				 if (value.equalsIgnoreCase("TRUE")) {
					 inputParams.setForceDiagonal(true);
					 continue;
    			 } else if (value.equalsIgnoreCase("FALSE")) {
    				inputParams.setForceDiagonal(false);
    				continue;
    			 } else {
    				failToParse.add("FORCEDIAGONAL");
    			 }
			 }*/ 
			 if (param.equals("GAPDISTANCE")) {
				 inputParams.promptGap(true);
				 Double val = TextFileParser.parseDoubleValue(value);
				 if (val == null || Double.isInfinite(val) || Double.isNaN(val)) {
					 failToParse.add("GAPDISTANCE");
					 continue;
				 }
				 inputParams.setGapDistance(val);
				 continue;
			 }
			 if (param.equals("KPPRA")) {
				 inputParams.promptMinTot(true);
				 Integer val = TextFileParser.parseIntegerValue(value);
				 if (val == null) {
					 failToParse.add("KPPRA");
		   			 continue; 
				 }
				 inputParams.setKPPRA(val);
				 continue;
			 }
			 if (cleanLine.contains("BETA_MODE")) { //All lines written below BETA_MODE will be taken in and not considered as an unknown
				 if (value.equalsIgnoreCase("TRUE")) {
					 inputParams.useBetaMode(true);
					 continue;
    			 } else if (value.equalsIgnoreCase("FALSE")) {
    				inputParams.useBetaMode(false);
    				continue;
    			 } else {
    				failToParse.add("BETA_MODE");
    			 }
				//inputParams.useBetaMode(true);
			 }
			 if (inputParams.isBetaMode()) {
				 if(cleanLine.contains("OPTI_DIAG")) {
					 if (value.equalsIgnoreCase("TRUE")) {
						 inputParams.useDiagonalToMinimize(true);
						 continue;
	    			 } else if (value.equalsIgnoreCase("FALSE")) {
	    				inputParams.useDiagonalToMinimize(false);
	    				continue;
	    			 } else {
	    				failToParse.add("OPTI_DIAG");
	    			 } 
					//inputParams.useDiagonalToMinimize(true);
				 }
				 if (cleanLine.contains("PRINT_SUPERTODIRECT")) {
					 if (value.equalsIgnoreCase("TRUE")) {
						 inputParams.setKPointUseSuperToDirect(true);
						 continue;
	    			 } else if (value.equalsIgnoreCase("FALSE")) {
	    				inputParams.setKPointUseSuperToDirect(false);
	    				continue;
	    			 } else {
	    				failToParse.add("PRINT_SUPERTODIRECT");
	    			 } 
					 //inputParams.setKPointUseSuperToDirect(false);
				 }
			 }
			 else {
				 unknownParams.add(cleanLine);
			 }
			
		}
		//removing all the duplicates
		LinkedHashSet<String> noDupFailToParse = new LinkedHashSet<>();
		LinkedHashSet<String> noDupUnknown = new LinkedHashSet<>();
		noDupFailToParse.addAll(failToParse);
		noDupUnknown.addAll(unknownParams);
		failToParse.clear();
		unknownParams.clear();
		failToParse.addAll(noDupFailToParse);
		unknownParams.addAll(noDupUnknown);
     	inputParams.setUnparseableList(failToParse);
     	inputParams.setUnrecognizeableList(unknownParams);
     	reader.close();
     	return true;
	  }
	
	private static KPointLatticeCollection loadCollection(String collLoc, SpaceGroup space, int numDim, int maxNumKpts, boolean includeGamma) 
    throws IOException {
	//throws JSONException, IOException {
		int arithNum = space.getArithmeticCrystalClassNumber(numDim);
        String shiftPath = includeGamma ? "/Gamma/lattices_G_" : "/Shifted/lattices_S_";
        String filePath = collLoc + shiftPath + arithNum + "_" + maxNumKpts;
        KPointLatticeCollection collLoad = new KPointLatticeCollection(filePath);
        collLoad.setDefaultFilePath(filePath);
        
        return collLoad;
	}
	private static String stripComments(String line) {
		int index = line.indexOf("#");
	    if (index >= 0) {
	      line = line.substring(0, index);
	    }
	    int index2 = line.indexOf("!");
	    if (index2 >= 0) {
	      line = line.substring(0, index2);
	    }
	    return line;
	}
	 public static String standardUserOutputFormat(String line) {
		 String out = line + " "; 
		 return out;
	 }
	 public static QENamelists appendQEWithKPoints (ServerInput inputParams, Structure struct, String fileLoc) {
		  String includeGammaStr = inputParams.includeGamma();
		  KPointLibrary.INCLUDE_GAMMA includeGamma = null;
		  if(includeGammaStr.equals("FALSE")) {
			  includeGamma = KPointLibrary.INCLUDE_GAMMA.FALSE;
		  } else if (includeGammaStr.equals("TRUE")) {
			  includeGamma = KPointLibrary.INCLUDE_GAMMA.TRUE;
		  } else {
			  includeGamma = KPointLibrary.INCLUDE_GAMMA.AUTO;
		  }
		  
		  QENamelists qeif = inputParams.getQEData();
		  
		  int genCounter = 0;
		  boolean genOK = false;			
		  while(!genOK) {
			  try {
				  KPointLattice kLat = generateKPointLattice(inputParams,struct, includeGamma);
					int[][] sup = kLat.getSuperToDirect(struct.getDefiningLattice());
					for (int a = 0; a < sup.length; a++) {
						System.out.println(sup[a][0] + " " + sup[a][1] + " " + sup[a][2] + " ");
					}
					System.out.println("##COORDS##");
					Coordinates[] coor = kLat.getDistinctKPointCoords();
					
					for (int a = 0; a < coor.length; a++) {
						double[] co = coor[a].getCoordArray(kLat.getReciprocalSpaceLattice().getLatticeBasis());
						System.out.println(co[0] + " " + co[1] + " " + co[2] + " " );
					}
				  if (kLat == null) {
					  return null;
				  }
				  long genStart = System.currentTimeMillis();
				  String comment = setUpKPOINTSHeader(kLat, struct, inputParams, includeGamma);
				  String header = "";
				  if (inputParams.isOutputSeparated()) {
					  String[] spl = comment.split("\\++");
					  header = spl[0] + System.lineSeparator() + "+++" + spl[1] + "+++";// + System.lineSeparator();
					  String realComment = spl[2].replace("\n", "");
					  qeif.updateWithKPoints(kLat, struct.getDefiningLattice().getInverseLattice().getLatticeBasis(), header, realComment);
					  long genFin = System.currentTimeMillis();
					  System.out.println("Generating KPOINTS object took: " + (genFin - genStart) + " ms.");
					  POSCAR realSLat = new POSCAR(kLat.getRealSuperLattice().getRepresentativeStructure(Species.hydrogen));
						realSLat.writeFile(fileLoc + "/QEKPointLatticeRealSuperLattice_"+ genCounter +".vasp");
						POSCAR recipSLat = new POSCAR(kLat.getReciprocalSpaceLattice().getRepresentativeStructure(Species.hydrogen));
						recipSLat.writeFile(fileLoc + "/QEKPointLatticeRciprocalSuperLattice_"+ genCounter +".vasp");
					  return qeif;
				  }
				  qeif.updateWithKPoints(kLat, struct.getDefiningLattice().getInverseLattice().getLatticeBasis(), header, comment);
				  inputParams.putQEData(qeif);
				  long genFin = System.currentTimeMillis();
				  System.out.println("Generating KPOINTS object took: " + (genFin - genStart) + " ms.");
				  POSCAR realSLat = new POSCAR(kLat.getRealSuperLattice().getRepresentativeStructure(Species.hydrogen));
					realSLat.writeFile(fileLoc + "/QEKPointLatticeRealSuperLattice_"+ genCounter +".vasp");
					POSCAR recipSLat = new POSCAR(kLat.getReciprocalSpaceLattice().getRepresentativeStructure(Species.hydrogen));
					recipSLat.writeFile(fileLoc + "/QEKPointLatticeRciprocalSuperLattice_"+ genCounter +".vasp");
				  return qeif;
			  } catch (SymmetryException e) {
					struct = struct.scaleLatticeConstant(2);
					inputParams.setScalingTimes();
					inputParams.setUsedMinDistance(inputParams.getUsedMinDistance() * 2);
					inputParams.precisionAdjusted(true);
				}
				genCounter++;
				if( genCounter > 4) {
					return null;
				}
			}
		  return qeif;
	 	}	  
	 
	 public boolean handleQEInputFile(File fi, ServerInput inputParams, PrintWriter logger, File errorInfo, String fileName) throws IOException {
	     try (BufferedReader reader = new BufferedReader(new FileReader(fi))){
	         /* QEInputFile qeInput = new QEInputFile(reader);
		      inputParams.putQEInputFile(qeInput);*/
	         QENamelists qeInput = new QENamelists(reader);
	         inputParams.putQEData(qeInput);
	         inputParams.setFileName(fileName);
	         return true;
	     } catch (FileNotFoundException e) {
	         FileOutputStream fos = new FileOutputStream(errorInfo, true);
             PrintStream errorStream = new PrintStream(fos);
             e.printStackTrace(errorStream);
	         return false;
	     } catch (RuntimeException e) {
	         FileOutputStream fos = new FileOutputStream(errorInfo, true);
	         PrintStream errorStream = new PrintStream(fos);
	         e.printStackTrace(errorStream);
	         errorStream.close();
	         return false;
	     }
     }
	 
		public static String[] listFilesInFolder(String dir) {
			File dirName = new File(dir);
			String[] fileFilter = new String[dirName.listFiles().length];
			int counter = 0;
		    for (final File fileEntry : dirName.listFiles()) {
		        if (fileEntry.isFile()) {
		        	fileFilter[counter] = fileEntry.getName();
		        	counter++;
		        } 
		    }
		    
		    //now remove all the nulls (i.e. the names of all the directories.)
			List<String> list = new ArrayList<String>();
			for (String s : fileFilter) {
				if(s != null && s.length() > 0) {
					list.add(s);
				}
			}
			fileFilter = list.toArray(new String[list.size()]);
			
			//System.out.println("dirList: " +Arrays.toString(fileFilter));
		    return fileFilter;
		}


}
