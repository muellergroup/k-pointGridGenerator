import java.io.*;
import java.util.ArrayList;

/**
 * An application that takes in input files, INCAR + POSCAR ([file].qe for QE), 
 * and returns KPOINTS file (append the list to input file for QE).
 * 
 * @version February 18, 2019 | 2019.02.18
 * @author pwisesa
 */
public class GridGenerator {

    public static void main(String[] args) {
        //System.out.println("Application version: " + KPointCalculator.m_Version);
        ArrayList<File> dirList = parseArguments(args);
        if (dirList.size() != 0) {
            //System.out.println("Number of Valid Paths: " + dirList.size() + ".");
            /*
            for (File f: dirList) {
                System.out.println(f.getAbsolutePath());
            }
            */
        } else {
            System.out.println("No valid path is given. Exiting...");
            System.exit(0);
        }
        
        KPointCalculator.init();
        
        int errCounter = 0;
        for (File dir: dirList) {
            try {
                KPointCalculator kpcalculator = new KPointCalculator (dir);
                if (kpcalculator.process() != 0) {
                    errCounter++;
                }
            // Internally uncaught exceptions. Prompt to user.
            } catch (NullPointerException e) {
                System.err.println(dir.getAbsolutePath());
                e.printStackTrace();
                String err = "Error: One or more of your inputs was invalid. " +
                        "Please check your inputs and/or contact us for help.";
                System.err.println(err);
                errCounter++;
            /*
            } catch (IOException e) {
                System.err.println(dir.getAbsolutePath());
                String err = "!!!There is a problem with the code." + 
                        "If this problem persists please contact us.";
                System.err.println(err);
                errCounter++;
            */
            } catch (Exception e) {
                System.err.println(dir.getAbsolutePath());
                e.printStackTrace();
                String err = "Error: There is a problem with processing your request."
                        + "If this problem persists please contact us.";
                System.err.println(err);
                errCounter++;
            }
        }
        
        //System.out.println(); //Blank line to separate output.
        if (errCounter > 0) {
            //System.out.println(errCounter + " cases failed.");
            System.exit(1);
        } else {            
            //System.out.println("All cases succeed.");
            System.exit(0);
        }
    } 
    
    /*
     * Handle command line arguments. Ditch incorrect paths, and store the 
     * valid paths left.
     * 
     * @return ArrayList<File> A list of valid paths to be processed.
     */
    private static ArrayList<File> parseArguments (String[] args) {
        ArrayList<File> dirList = new ArrayList<>();
        if (args.length == 0) { 
            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(System.in))) {
                // Check whether any file is redirected here.
                // TODO: Buggy code here.
                if (System.in.available() != 0) {
                    String i = in.readLine();
                    while (i != null) {
                        File f = checkDirValid(i);
                        if (f != null) { dirList.add(f); }
                        i = in.readLine();
                    }
                }
            } catch (IOException e) {
                System.err.println("Error: Fail to read! Check whether it's a dir.");
                //System.exit(1);  //Exit from the main(), not here.
            }
        } else { 
            for (int i = 0; i < args.length; i++) {
                if (!args[i].equals("-f")) {
                    File f = checkDirValid(args[i]);
                    if (f != null) { dirList.add(f); }
                } else {
                    readDirFromFiles(dirList, args, i);
                    break;
                }
            }
        }
        return dirList; 
    }
    
    /*
     * Files containing valid paths are given. Traverse these paths to pick
     * the valid ones.
     */
    private static void readDirFromFiles(ArrayList<File> dirList, 
            String args[], int i) {
        for (int j = i + 1; j < args.length; j++) {
            try (BufferedReader in = 
                    new BufferedReader(new FileReader(args[j]))) {
                String str = in.readLine();
                while(str != null) {
                    File f = checkDirValid(str);
                    if (f != null) { dirList.add(f); }
                    str = in.readLine();
                }
            } catch (FileNotFoundException e) {
                System.err.println(
                        "Error: Cannot open file " + args[j] + " for reading!");
            } catch (IOException e) {
                System.err.println(
                        "Error: Fail to read from file " + args[j] + "!");
            }
        }
    }
    
    /*
     * @param path The path to be checked as valid calculation directory.
     * @return File If path is a folder and contains at least POSCAR or QE 
     *              input files, returns the new File(path).
     *              If path is a file or dones't contain valid inputs, return 
     *              null.
     */
    private static File checkDirValid(String path) {
        File f = null;
        // Empty abstract pathname will be resolved into user.dir.
        if (path != null && path.length() > 0) {
            f = new File(path);
            if (f.isDirectory()) {
                boolean hasPOSCAR = false;
                boolean hasQE = false;
                String[] fs = f.list();
                for (String i: fs) {
                    if (i.equals("POSCAR")) { hasPOSCAR = true; }
                    if (i.contains(".qe") || i.contains(".in")) { hasQE = true; }
                }
                if (hasPOSCAR || hasQE) {
                    return f;
                } else {
                    System.err.println("Error: " + path + 
                        " doesn't contain valid input files!");
                }
            } else {
                System.err.println("Error: " + path + " is not a folder!");
            }
        }
        return null;
    }
    
}