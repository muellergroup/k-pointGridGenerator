# K-point Grid Generator
***
This program utilizes informatics to help generate more efficient k-point grids for DFT calculations. Based on our benchmarking results on 102 randomly selected materials from the ICSD database, calculations using our grid are about 2 time faster than these using the usual Monkhorst-Pack grid.

## Theory
For the theories and the performance benchmark, check our papers: 

Wisesa, Pandu, Kyle A. McGill, and Tim Mueller. 2016. ["Efficient generation of generalized Monkhorst-Pack grids through the use of informatics"](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.93.155109). *Physical Review B* **93** (15).

Wang, Yunzhe, Pandu Wisesa, Adarsh Balasubramanian, and Tim Mueller. 2019. ["Algorithms and Code for the Generation of Generalized Monkhorst-Pack Grids"](https://arxiv.org/abs/1907.13610). *arXiv.org preprint*, arXiv:1907.13610.

For more information, check [our group web site](http://muellergroup.jhu.edu/).

## Installation
There are two ways to install this program. Users can either **download the executable jar** or **build the program from scratch**, if the pre-built version doesn't work on your local machine for some reasons.

#### Download the Pre-built Executable Jar
You can download the pre-build excutable here: [k-pointGridGenerator_2019.08.01.tar.gz](http://muellergroup.jhu.edu/Scripts/k-pointGridGenerator_2019.08.01.tar.gz). 

Once the download completes, untar it by the command:
```
tar -xzf ./k-pointGridGenerator.tar.gz 
```
Then, you will see a folder **k-pointGridGenerator.tar.gz** appear. Inside it, there are four things: a **README.txt**, a script **getKPoints**, the executable jar **GridGenerator.jar** and the **minDistanceCollection** folder, which stores all efficient k-point grids the program has found so far.

#### Build the executable from Scratch
If the pre-build version somehow doesn't work on your local machine, you can download the code and build the program from scratch. We provide an `ant` auto-build script (it's like `make` for C), so the compilation should be straight forward. 

1. Check whether JAVA has been installed on your machine. 
```
   java -version
```
or
```
   javac -version
```
 

2. Install the build-tool `ant` (java version of `cmake`). To install it, on Ubuntu, you can use the command:             
```
   sudo apt-get install ant
```
(other distribution should be similar). Then launch the build-tool by:
```
   ant build
```
It will compiles add the source codes into binaries (`.class`) and they will sit in folder `./bin`. 

3. Wrap the binaries into an executable:
```
   ./install.sh
```
Now, you will find an executable jar `./GridGenerator.jar`.


## Usage

To use the executable, you need four things: the executable jar **GridGenerator.jar**, the database **minDistanceCollections**, the script **getKPoints**,  and an input file **PRECALC**.

1. Setup **`getKPoints`**

Open the **getKPoints** script with your favorite text editor and assign to the variable **JAR_PATH** the path of the folder containing the executable jar. For example, if the bundle is untarred at a user's home directory, the value will be:
```
   JAR_PATH="/home/<user_name>/k-pointGridGenerator"
```

An optional variable to set is `CALL_SERVER_IF_LOCAL_MISSING`, and it has a default value of `FALSE`. If users want the script to call our server when the local jar file is not found at `JAR_PATH` or `Java` is not installed on local machine, you should change it to `TRUE`. (Note, if the local program runs and failed to generate a k-point grid for either user input errors or hidden bugs, the server will not be called.)

2. Set input parameters in **PRECALC**. 

The minimum setting is 
```
   MINDISTANCE=<VALUE>
```
It specifies the size of the requested grid. For other flags, check our [group website](http://muellergroup.jhu.edu/K-Points.html) for a detailed explanation.


Now you are all set! Simply type:
```
~/bin/getKPoints 
```
in the folder where you put the VASP input files. A `KPOINTS` file will be generated and it's ready to be used in calculation.
